<?php 
class Product_option_photo_model extends CI_Model {

	var $API_END_POINT = 'admin/product_option_photos';
	var $API_PRODUCT_OPTIONS_END_POINT = 'admin/product_options';

	function __construct()
	{
		parent::__construct();
	}

	function getAllByProductOptionID($product_option_id)
	{
		$params = null;
		
		return json_decode($this->restclient->get($params, $this->API_PRODUCT_OPTIONS_END_POINT . '/' . $product_option_id . '/photos'), true);
	}

	function upload($array)
	{
		return json_decode($this->restclient->post($array, $this->API_END_POINT), true);
	}
}
?>