<?php 
class Option_model extends CI_Model {

	var $API_END_POINT = 'admin/options';
	var $API_GROUPS_END_POINT = 'admin/option_groups';

	function __construct()
	{
		parent::__construct();
	}

	function getAllByGroupContentID($group_content_id)
	{	
		$params = null;

		if (!empty($this->lang_id)) 
		{
			$params = array('lang_id' => $this->lang_id);
		}

		return json_decode($this->restclient->get($params, $this->API_GROUPS_END_POINT . '/' . $group_content_id . '/options'), true);
	}

	function getByContentID($content_id)
	{
		$params = null;
		
		if (!empty($this->lang_id)) 
		{
			$params = array('lang_id' => $this->lang_id);
		}

		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/' . $content_id), true);
	}

	function save($array)
	{
		return json_decode($this->restclient->post($array, $this->API_END_POINT), true);
	}

	function update($array)
	{
		return json_decode($this->restclient->put($array, $this->API_END_POINT), true);
	}

	function delete($content_id)
	{
		return json_decode($this->restclient->delete(null, $this->API_END_POINT . '/' . $content_id), true);
	}
}
?>