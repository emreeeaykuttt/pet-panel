<?php 
class Product_option_model extends CI_Model {

	var $API_END_POINT = 'admin/product_options';
	var $API_PRODUCTS_END_POINT = 'admin/products';

	function __construct()
	{
		parent::__construct();
	}

	function getAllByProductContentID($product_content_id)
	{
		$params = null;
		
		if (!empty($this->lang_id)) 
		{
			$params = array('lang_id' => $this->lang_id);
		}

		return json_decode($this->restclient->get($params, $this->API_PRODUCTS_END_POINT . '/' . $product_content_id . '/options'), true);
	}

	function getByContentID($content_id)
	{
		$params = null;
		
		if (!empty($this->lang_id)) 
		{
			$params = array('lang_id' => $this->lang_id);
		}

		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/' . $content_id), true);
	}

	function save($array)
	{
		return json_decode($this->restclient->post($array, $this->API_END_POINT), true);
	}

	function update($array)
	{
		return json_decode($this->restclient->put($array, $this->API_END_POINT), true);
	}

	function delete($content_id)
	{
		return json_decode($this->restclient->delete(null, $this->API_END_POINT . '/' . $content_id), true);
	}
}
?>