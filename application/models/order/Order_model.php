<?php 
class Order_model extends CI_Model {

	var $API_END_POINT = 'admin/orders';

	function __construct()
	{
		parent::__construct();
	}

	function getDatatables()
	{
		$params = array('data' => $this->post);
		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/datatables'), true);
	}

	function getCountAll()
	{
		$params = array('data' => $this->post, 'count' => 'all');
		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/datatables'), true);
	}

	function getCountFiltered()
	{
		$params = array('data' => $this->post, 'count' => 'filtered');
		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/datatables'), true);
	}

	function getByID($id)
	{
		return json_decode($this->restclient->get(null, $this->API_END_POINT . '/' . $id), true);
	}

	function update($array, $id)
	{
		return json_decode($this->restclient->put($array, $this->API_END_POINT . '/' . $id), true);
	}
}
?>