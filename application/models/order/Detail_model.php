<?php 
class Detail_model extends CI_Model {

	var $API_ORDERS_END_POINT = 'admin/orders';

	function __construct()
	{
		parent::__construct();
	}

	function getAllByOrderID($order_id)
	{
		$params = null;

		return json_decode($this->restclient->get($params, $this->API_ORDERS_END_POINT . '/' . $order_id . '/details'), true);
	}
}
?>