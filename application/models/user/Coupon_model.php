<?php 
class Coupon_model extends CI_Model {

	var $API_END_POINT = 'admin/coupons';
	var $API_USERS_END_POINT = 'users';

	function __construct()
	{
		parent::__construct();
	}

	function getAllByCouponContentID($coupon_content_id)
	{	
		$params = null;

		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/' . $coupon_content_id . '/' . $this->API_USERS_END_POINT), true);
	}

	function save($array, $coupon_content_id)
	{
		return json_decode($this->restclient->post($array, $this->API_END_POINT . '/' . $coupon_content_id . '/' . $this->API_USERS_END_POINT), true);
	}

	function delete($id, $coupon_content_id)
	{
		return json_decode($this->restclient->delete(null, $this->API_END_POINT . '/' . $coupon_content_id . '/' . $this->API_USERS_END_POINT . '/' . $id), true);
	}
}
?>