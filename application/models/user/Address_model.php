<?php 
class Address_model extends CI_Model {

	var $API_USERS_END_POINT = 'admin/users';

	function __construct()
	{
		parent::__construct();
	}

	function getAllByUserID($user_id)
	{
		$params = null;

		return json_decode($this->restclient->get($params, $this->API_USERS_END_POINT . '/' . $user_id . '/addresses'), true);
	}
}
?>