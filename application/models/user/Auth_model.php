<?php 
class Auth_model extends CI_Model {

	var $API_END_POINT = 'admin/authentication';

	function __construct()
	{
		parent::__construct();
	}

	function validate($array)
	{
		return json_decode($this->restclient->post($array, $this->API_END_POINT . '/token'), true);
	}

	function getTotalRecords()
	{
		$params = null;

		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/total_records'), true);
	}
}
?>