<?php 
class User_model extends CI_Model {

	var $API_END_POINT = 'admin/users';

	function __construct()
	{
		parent::__construct();
	}

	function getDatatables()
	{
		$params = array('data' => $this->post);
		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/datatables'), true);
	}

	function getCountAll()
	{
		$params = array('data' => $this->post, 'count' => 'all');
		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/datatables'), true);
	}

	function getCountFiltered()
	{
		$params = array('data' => $this->post, 'count' => 'filtered');
		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/datatables'), true);
	}

	function getByID($id)
	{
		return json_decode($this->restclient->get(null, $this->API_END_POINT . '/' . $id), true);
	}

	function getAllByNameAndSurname($name_and_surname)
	{
		$params = array('name_and_surname' => $name_and_surname);

		return json_decode($this->restclient->get($params, $this->API_END_POINT), true);
	}
}
?>