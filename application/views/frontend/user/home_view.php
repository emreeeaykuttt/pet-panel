<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Admin Panel">
    <meta name="keywords" content="Admin Panel">

    <title>Admin Panel</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>

	<div class="main-content">
		
		<?php include(dirname(__DIR__) . '/inc/left_area.php'); ?>
	
		<div class="right-content">

			<?php include(dirname(__DIR__) . '/inc/top_area.php'); ?>

			<div class="content-title">
				KULLANICILAR
			</div>
			
			<div class="content">
				
				<div class="table-responsive">
					<table id="table" class="display" style="width:100%">
				        <thead>
				            <tr>
				                <th class="font-weight-light">Ad Soyad</th>
				                <th class="font-weight-light">Email</th>
				                <th class="font-weight-light">Telefon</th>
				                <th class="font-weight-light">Cinsiyet</th>
				                <th class="font-weight-light" width="97px">İşlemler</th>
				            </tr>
				        </thead>
				    </table>
			    </div>

			</div>
			
		</div>
				
	</div>
  
	<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
	<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

	<script type="text/javascript">

		var save_method;
		var table;
	    var export_name = module_export_name('kullanicilar');
		
		$(document).ready(function(){

		    table = $('#table').DataTable({

		    	'language': {
		            'url': base_url + 'assets/plugins/datatables/language_tr.json'
		        },

		        'processing': true,
		        'serverSide': true,
		        'order': [],
		        'lengthMenu': [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'Tüm']],
		        'ajax': {
		            'url': base_url + 'user/main/list_all',
		            'type': 'POST'
		        },

		        'dom': '<"w-50 float-left"l><"w-50 float-left"fB><"table-content" tr><"w-50 float-left"i><"w-50 float-left"p>',

				'buttons': [
					{	
						extend: 'colvis', 
						text: 'Sütünlar'
					},
					{
						extend: 'csv', 
						text: 'CSV', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
					{
						extend: 'pdf', 
						text: 'PDF', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
				],

		  		'columns': [
		            { 'data': 'UserFirstName' },
		            { 'data': 'UserEmail' },
		            { 'data': 'UserPhone' },
		            { 'data': 'UserGender' },
		            { 'data': 'Transactions', 'orderable': false}
		        ]

		    });

		});
		
		function view(id)
		{
			$('#user-view').html('');

			$.ajax({
		        url : base_url + 'user/main/view/' + id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	let addres_type = '';
		        	let bill_type = '';
		        	let phone = data.UserPhone ? data.UserPhone : '';
		        	let gender = data.UserGender ? data.UserGender : '';
		        	let birht = data.UserDateBirth ? data.UserDateBirth : '';

		        	$('#user-view').append(
	                	
	                	'<div class="container">' +
			      			'<div class="row" id="user-detail">' +

			      				'<div class="col-md-4">' +
			      					'<div class="view-item"><b>Kullanıcı Bilgileri</b></div>' +
				         			'<div class="view-item"><b>İsim: </b>' + data.UserFirstName + '</div>' +
				         			'<div class="view-item"><b>Soyisim: </b>' + data.UserLastName + '</div>' +
				         			'<div class="view-item"><b>Email: </b>' + data.UserEmail + '</div>' +
				         			'<div class="view-item"><b>Telefon: </b>' + phone + '</div>' +
				         			'<div class="view-item"><b>Cinsiyet: </b>' + gender + '</div>' +
				         			'<div class="view-item"><b>Doğum Tarihi: </b>' + birht + '</div>' +
				         			'<div class="view-item"><b>Oluşturma Tarihi: </b>' + data.UserCreatedAt + '</div>' +
		      					'</div>' +

			      			'</div>' +
		      			'</div>'
                	);
					
					$.each(data.Addresses, function(i, item) {

						if (item.AddressType == 'delivery')
						{
							addres_type = 'Teslimat Adresi';
						}
						else if (item.AddressType == 'billing')
						{
							addres_type = 'Fatura Adresi';
						}

						if (item.AddressBillType == 'individual')
						{
							bill_type = '<div class="view-item"><b>Fatura Tipi: Bireysel</b></div>';
						}
						else if (item.AddressBillType == 'corporate')
						{
							bill_type = 
								'<div class="view-item"><b>Fatura Tipi: Kurumsal</b></div>' +
								'<div class="view-item"><b>Firma Adı: </b>' + item.AddressCompanyName + '</div>' +
								'<div class="view-item"><b>Vergi Numarası: </b>' + item.AddressCompanyTaxNumber + '</div>' +
								'<div class="view-item"><b>Vergi Dairesi: </b>' + item.AddressCompanyTaxAdministration + '</div>';
						}
						else
						{
							bill_type = '';
						}

						$('#user-detail').append(
							'<div class="col-md-4">' +
		      					'<div class="view-item"><b>Adres Adı: </b>' + item.AddressTitle + '</div>' +
		      					'<div class="view-item"><b>Adres Tipi: </b>' + addres_type + '</div>' +
			         			'<div class="view-item"><b>İsim Soyisim: </b>' + item.AddressFirstName + ' ' + item.AddressLastName + '</div>' +
			         			'<div class="view-item"><b>TC Kimlik No: </b>' + item.AddressIdentityNumber + '</div>' +
			         			'<div class="view-item"><b>Email: </b>' + item.AddressEmail + '</div>' +
			         			'<div class="view-item"><b>Telefon: </b>' + item.AddressPhone + '</div>' +
			         			'<div class="view-item"><b>Ülke: </b>' + item.CountryName + ' / ' + item.CityName + ' / ' + item.DistrictName + '</div>' +
			         			'<div class="view-item"><b>Açık Adres: </b>' + item.AddressOpenAddress + '</div>' +
			         			'<div class="view-item"><b>Posta Kodu: </b>' + item.AddressZipCode + '</div>' +
			         			bill_type +
	      					'</div>'
	                	);

                	});

		            $('#view-modal').modal('show'); 
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		}

		function reload_table()
		{
		    table.ajax.reload(null,false);
		}
	</script>

	<div class="modal fade" id="view-modal" role="dialog">
	    <div class="modal-dialog modal-xl">
	        <div class="modal-content popup-content">

	            <div class="modal-header">
			        <h5 class="modal-title">Görüntüleme</h5>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>

	            <div class="modal-body">

	                <div id="user-view" class="view-list">

					</div>

	            </div>

	            <div class="modal-footer">
	                <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>
	            </div>

	        </div>
	    </div>
	</div>

</body>
	
</html>