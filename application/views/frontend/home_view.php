<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Admin Panel">
    <meta name="keywords" content="Admin Panel">

    <title>Admin Panel</title>

    <?php include('inc/head.php'); ?>

</head>

<body>

	<div class="main-content">
		
		<?php include('inc/left_area.php'); ?>
	
		<div class="right-content">

			<?php include('inc/top_area.php'); ?>

			<div class="content-title">
				DASHBOARD
			</div>

			<div class="content">
				
				<div class="row">
					
					<div class="col-md-4">
						<div class="card border-success mb-4">
							<a href="<?=base_url()?>product/main" class="card-header text-success bg-transparent border-success">
								<b>Ürünler</b>
							</a>
							<div class="card-body text-success">
								<div class="row">
			                        <div class="col-3">
			                            <i class="la la-chart-bar la-3x"></i>
			                        </div>
			                        <div class="col-9 text-right">
			                            <h5><?=$product_total?></h5>
			                            <h6 class="card-text">Kayıt var</h6>
			                        </div>
			                    </div>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="card border-primary mb-4">
							<a href="<?=base_url()?>product/category" class="card-header text-primary bg-transparent border-primary">
								<b>Kategoriler</b>
							</a>
							<div class="card-body text-primary">
								<div class="row">
			                        <div class="col-3">
			                            <i class="la la-chart-bar la-3x"></i>
			                        </div>
			                        <div class="col-9 text-right">
			                            <h5><?=$category_total?></h5>
			                            <h6 class="card-text">Kayıt var</h6>
			                        </div>
			                    </div>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="card border-danger mb-4">
							<a href="<?=base_url()?>product/option_group" class="card-header text-danger bg-transparent border-danger">
								<b>Opsiyon Grupları</b>
							</a>
							<div class="card-body text-danger">
								<div class="row">
			                        <div class="col-3">
			                            <i class="la la-chart-bar la-3x"></i>
			                        </div>
			                        <div class="col-9 text-right">
			                            <h5><?=$group_total?></h5>
			                            <h6 class="card-text">Kayıt var</h6>
			                        </div>
			                    </div>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="card border-info mb-4">
							<a href="<?=base_url()?>product/tag" class="card-header text-info bg-transparent border-info">
								<b>Etiketler</b>
							</a>
							<div class="card-body text-info">
								<div class="row">
			                        <div class="col-3">
			                            <i class="la la-chart-bar la-3x"></i>
			                        </div>
			                        <div class="col-9 text-right">
			                            <h5><?=$tag_total?></h5>
			                            <h6 class="card-text">Kayıt var</h6>
			                        </div>
			                    </div>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="card border-dark mb-4">
							<a href="<?=base_url()?>product/coupon" class="card-header text-dark bg-transparent border-dark">
								<b>Kuponlar</b>
							</a>
							<div class="card-body text-dark">
								<div class="row">
			                        <div class="col-3">
			                            <i class="la la-chart-bar la-3x"></i>
			                        </div>
			                        <div class="col-9 text-right">
			                            <h5><?=$coupon_total?></h5>
			                            <h6 class="card-text">Kayıt var</h6>
			                        </div>
			                    </div>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="card border-warning mb-4">
							<a href="<?=base_url()?>order/main" class="card-header text-warning bg-transparent border-warning">
								<b>Siparişler</b>
							</a>
							<div class="card-body text-warning">
								<div class="row">
			                        <div class="col-3">
			                            <i class="la la-chart-bar la-3x"></i>
			                        </div>
			                        <div class="col-9 text-right">
			                            <h5><?=$order_total?></h5>
			                            <h6 class="card-text">Kayıt var</h6>
			                        </div>
			                    </div>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="card border-secondary mb-4">
							<a href="<?=base_url()?>user/main" class="card-header text-secondary bg-transparent border-secondary">
								<b>Kullanıcılar</b>
							</a>
							<div class="card-body text-secondary">
								<div class="row">
			                        <div class="col-3">
			                            <i class="la la-chart-bar la-3x"></i>
			                        </div>
			                        <div class="col-9 text-right">
			                            <h5><?=$user_total?></h5>
			                            <h6 class="card-text">Kayıt var</h6>
			                        </div>
			                    </div>
							</div>
						</div>
					</div>

				</div>

			</div>
			
		</div>
				
	</div>

  
	<?php include('inc/footer.php'); ?>
	<?php include('inc/script.php'); ?>

</body>
	
</html>