<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Admin Panel Giriş">
    <meta name="keywords" content="Admin Panel Giriş">

    <title>Admin Panel Giriş</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>

	<?php  
		$login_status = $this->session->flashdata('login_status');
		$flashdata = null;
		if (isset($login_status)) {
			$flashdata = $login_status;
		}
   	?>

	<div class="main-content">
	
		<div class="right-content w-100">

			<div class="content-title">
				Giriş Yap
			</div>

			<div class="content" style="height: 100vh;">
				
				<form action="<?=base_url()?>auth/login" method="post" autocomplete="off">

					<div class="row">
						<div class="col-md-6 offset-md-3">
							
							<div class="row">

								<div class="col-md-12">
	                                <h3>Giriş Yap</h3>
	                            </div>

								<div class="col-md-12">
			                        <?php if ($flashdata): ?>
								    <div class="alert special-alert <?=$flashdata['status'] ? 'alert-success' : 'alert-danger'?> mb-2">
								       <span><?=$flashdata['message']?></span>
								    </div>
								    <?php endif ?>
								</div>

								<div class="col-md-12">
									<div class="form-group <?=isset($errors['UserEmail']) ? 'has-error' : ''?>">
										<label for="email">E-Posta:</label>
										<input type="text" name="UserEmail" class="form-control" value="<?= set_value('UserEmail') ? set_value('UserEmail') : $this->session->tempdata('RememberUserEmail') ?>"  />
										<div class="help-feedback"><?=isset($errors['UserEmail']) ? $errors['UserEmail'] : ''?></div>
									</div>
									<div class="form-group <?=isset($errors['UserPassword']) ? 'has-error' : ''?>">
										<label for="pwd">Şifre:</label>
										<input type="password" name="UserPassword" class="form-control" value="<?= set_value('UserPassword') ? '' : $this->session->tempdata('RememberUserPassword') ?>"/>
										<div class="help-feedback"><?=isset($errors['UserPassword']) ? $errors['UserPassword'] : ''?></div>
									</div>
									<div class="form-group form-check">
										<label class="form-check-label">
											<input type="checkbox" name="UserRemember" class="form-check-input" <?= $this->session->tempdata('RememberUserEmail') ? 'checked="checked"' : ''?> /> Beni hatirla
										</label>
									</div>
								  	<button type="submit" class="btn btn-primary w-100 mt-2">GİRİŞ</button>
								</div>

							</div>

						</div>
					</div>

				</form>

			</div>
			
		</div>
				
	</div>

  
	<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
	<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

</body>
	
</html>