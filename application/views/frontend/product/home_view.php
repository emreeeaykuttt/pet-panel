<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Admin Panel">
    <meta name="keywords" content="Admin Panel">

    <title>Admin Panel</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>
    <link href="<?=base_url()?>assets/plugins/slim-select/css/slimselect.min.css" rel="stylesheet"></link>

</head>

<body>

	<div class="main-content">
		
		<?php include(dirname(__DIR__) . '/inc/left_area.php'); ?>
	
		<div class="right-content">

			<?php include(dirname(__DIR__) . '/inc/top_area.php'); ?>

			<div class="content-title">
				Ürünler
			</div>
			
			<div class="content">

				<div class="new-add">
					<a href="javascript:void(0)" onclick="add()">
						<i class="la la-plus"></i> Yeni Ekle
					</a>
				</div>

				<div class="languages">
					<select id="lang">
						<!-- language content -->
					</select>
				</div>
				
				<div class="table-responsive">
					<table id="table" class="display" style="width:100%">
				        <thead>
				            <tr>
				                <th class="font-weight-light">Ürün Adı</th>
				                <th class="font-weight-light">Kod</th>
				                <th class="font-weight-light">Kategori</th>
				                <th class="font-weight-light" width="413px">İşlemler</th>
				            </tr>
				        </thead>
				    </table>
			    </div>

			</div>
			
		</div>
				
	</div>


	<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
	<?php include(dirname(__DIR__) . '/inc/script.php'); ?>
	<script src="<?=base_url()?>assets/plugins/slim-select/js/slimselect.min.js"></script>

	<script type="text/javascript">

		var save_method;
		var save_method_option;
		var table;
		var language_count = 0;
		var default_language;
		var languages = [];
	    var price;
		var vat_rate;
		var vat_price;
		var vat_inclusive_price;
		var discount_rate;
		var discount_price;
		var latest_price;
		var price_second;
		var vat_rate_second;
		var vat_price_second;
		var vat_inclusive_price_second;
		var discount_rate_second;
		var discount_price_second;
		var latest_price_second;
		var main_categories;
		var sub_categories;
		var option_groups;
		var optipn_list;
	    var export_name = module_export_name('urunler');
	    var product_option_name = '';
		var product_group_total;
		var product_group_detail;
		var slim_select_group = [];
		var tags;
		var slim_select_tag;

		ajax_languages();
		ajax_main_categories(1);
		ajax_option_groups();
		
		$(document).ready(function(){

		    table = $('#table').DataTable({

		    	'language': {
		            'url': base_url + 'assets/plugins/datatables/language_tr.json'
		        },

		        'processing': true,
		        'serverSide': true,
		        'order': [],
		        'lengthMenu': [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'Tüm']],
		        'ajax': {
		            'url': base_url + 'product/main/list_all',
		            'type': 'POST'
		        },

		        'dom': '<"w-50 float-left"l><"w-50 float-left"fB><"table-content" tr><"w-50 float-left"i><"w-50 float-left"p>',

				'buttons': [
					{	
						extend: 'colvis', 
						text: 'Sütünlar'
					},
					{
						extend: 'csv', 
						text: 'CSV', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
					{
						extend: 'pdf', 
						text: 'PDF', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
				],

		  		'columns': [
		            { 'data': 'ProductName' },
		            { 'data': 'ProductCode' },
		            { 'data': 'CategoryName' },
		            { 'data': 'Transactions', 'orderable': false}
		        ]

		    });

		    for (var i = 1; i <= languages.length; i++)
		    {
		        var lang_selected = '';
		        var lang_active = '';
		        var lang_active_id = 'passive-id';
		        var lang_in_active_default_active = 'fade';
		        var common_img_status = 0;
		        var img_display = 'block';
		        
		        if (default_language == languages[language_count].LanguageID) 
		        {
		            lang_selected = 'selected';
		            lang_active = ' active show';
		            lang_active_id = 'active-id';
		            lang_in_active_default_active = 'show active default-active';
		        }

		        if (i > 1) 
		        {
		            common_img_status = 1;
		            img_display = 'none';
		        }

		        $('#lang').append('<option value="'+languages[language_count].LanguageID+'" '+lang_selected+'>'+languages[language_count].LanguageName+'</option>');

		        $('#language-nav').append(
		            '<li class="nav-item">' +
		                '<a class="nav-link'+lang_active+'" id="'+lang_active_id+'" data-toggle="tab" href="#tab-'+languages[language_count].LanguageCode+'">' +
	                		languages[language_count].LanguageName +
                		'</a>' +
		            '</li>'
		        );

		        $('#form-content-lang').append(
		            '<div id="tab-'+languages[language_count].LanguageCode+'" class="container tab-pane fade '+lang_in_active_default_active+' ">' +
		                '<div class="panel-body">' +
    
	                        '<input type="hidden" value="" name="ProductID['+i+']"/>' +
	                        '<input type="hidden" name="ProductLangID['+i+']" value="'+languages[language_count].LanguageID+'">' +

	                        '<div class="row">' +

		                        '<div class="col-md-8">' +
		                        	'<div class="form-group">' +
			                            '<label class="control-label">Ürün Adı</label>' +
			                            '<div class="input-area">' +
			                                '<input name="ProductName['+i+']" class="form-control" type="text">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +	
		                        '</div>' +

		                        '<div class="col-md-4">' +
		                        	'<div class="form-group">' +
			                            '<label class="control-label">Ürün Kodu</label>' +
			                            '<div class="input-area">' +
			                                '<input name="ProductCode['+i+']" class="form-control product-code" type="text">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +	
		                        '</div>' +

	                        '</div>' +					                        

	                        '<div class="row">' +

		                        '<div class="col-md-6">' +
			                        '<div class="form-group">' +
			                            '<label class="control-label">Kategori</label>' +
			                            '<div class="input-area">' +
			                                '<select class="form-control main-category" id="main-category-'+i+'" name="ProductMainCategoryID['+i+']">' +
			                                    '<option value="0">Seçiniz...</option>' +
			                                '</select>' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
		                        '</div>' +

		                        '<div class="col-md-6">' +
			                        '<div class="form-group">' +
			                            '<label class="control-label">Alt Kategori</label>' +
			                            '<div class="input-area">' +
			                                '<select class="form-control sub-category" id="sub-category-'+i+'" name="ProductCategoryContentID['+i+']">' +
			                                    '<option value="0" data-category-id="0">Seçiniz...</option>' +
			                                '</select>' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
		                        '</div>' +

		                        '<input type="hidden" value="0" class="sub-category-hidden" name="ProductCategoryID['+i+']"/>' +

	                        '</div>' +

	                        '<div class="form-group">' +
	                            '<label class="control-label">Açıklama</label>' +
	                            '<div class="input-area">' +
	                                '<textarea name="ProductDescription['+i+']" id="detail_'+i+'" class="form-control"></textarea>' +
	                                '<span class="help-block"></span>' +
	                            '</div>' +
	                        '</div>' +

	                        '<div class="form-group" style="display:'+ img_display +'">' +
	                            '<label class="control-label">Opsiyon Grupları</label>' +
	                            '<div class="selectslim-area">' +
	                                '<select id="product-groups-'+i+'" name="ProductOptionGroups['+i+'][]" multiple>' +
									'</select>' +
	                                '<span class="option-groups-help-block help-block"></span>' +
	                            '</div>' +
	                        '</div>' +

	                        '<div class="form-group" style="display:'+ img_display +'">' +
	                            '<label class="control-label">Etiket</label>' +
	                            '<div class="selectslim-area">' +
	                                '<select id="tags-'+i+'" name="ProductTags['+i+'][]" multiple>' +
									'</select>' +
	                                '<span class="tags-help-block help-block"></span>' +
	                            '</div>' +
	                        '</div>' +

	                        '<div class="form-area-border common-img-'+common_img_status+'">' +
	                            '<div class="form-group photo-class margin-bottom-0" id="photo-preview-'+i+'">' +
	                                '<label class="control-label"></label>' +
	                                '<div>' +
	                                    '(Fotoğraf Yok)' +
	                                    '<span class="help-block"></span>' +
	                                '</div>' +
	                            '</div>' +
	                            '<div class="form-group">' +
	                                '<label class="control-label" id="label-photo">Öne Çıkan Görsel </label>' +
	                                '<div>' +
	                                    '<input name="ProductPhoto['+i+']" type="file">' +
	                                    '<span class="help-block"></span>' +
	                                '</div>' +
	                            '</div>' +
	                        '</div>' +

	                        '<div class="form-group">' +
	                            '<label class="control-label">Görselin Alt Etiketinde Gözükecek Yazı</label>' +
	                            '<div class="input-area">' +
	                                '<input name="ProductPhotoAlt['+i+']" class="form-control" type="text">' +
	                                '<span class="help-block"></span>' +
	                            '</div>' +
	                        '</div>' +	

	                        '<div class="form-group" style="display:'+ img_display +'">' +
                                '<label class="font-bold common-img">' +
                                	'<input type="checkbox" name="ProductCommonPhoto" value="1">' +
                                    ' Tüm dillerde ortak görsel kullanılsın' +
                                '</label>' +
	                        '</div>' +

		                '</div>' +
		            '</div>'
		        );

				for (var m = 0; m < main_categories.length; m++)
		        {
		        	if (main_categories[m].CategoryContentID != 7) {
			            if (main_categories[m].CategoryLangID == languages[language_count].LanguageID) {
			                $('#main-category-'+i).append('<option value="'+main_categories[m].CategoryContentID+'">'+main_categories[m].CategoryName+'</option>');
			            }
		            }
		        }

		        $.ajax({
			        url : base_url + 'product/category/list_by_parent_id/7',
			        type: 'GET',
			        dataType: 'JSON',
			        success: function(val)
			        {
			            for (var m = 0; m < val.categories.length; m++)
				        {
				            $('#main-category-1').append('<option value="'+val.categories[m].CategoryContentID+'">'+val.categories[m].CategoryName+'</option>');
				        }
			        }
			    });

		        for (var m = 0; m < option_groups.length; m++)
		        {
		            if (option_groups[m].GroupLangID == languages[language_count].LanguageID) {
		                $('#product-groups-'+i).append('<option value="'+option_groups[m].GroupContentID+'">'+option_groups[m].GroupName+'</option>');
		            }
		        }

		        let slim_product_group = new SlimSelect({
				  	select: '#product-groups-' + i,
				  	placeholder: 'Opsiyon Grubu seçiniz', 
			  		closeOnSelect: false
				})

				slim_select_group.push(slim_product_group);

		        $('#language-nav-option').append(
		            '<li class="nav-item">' +
		                '<a class="nav-link'+lang_active+'" id="'+lang_active_id+'" data-toggle="tab" href="#tab-option-'+languages[language_count].LanguageCode+'">' +
	                		languages[language_count].LanguageName +
                		'</a>' +
		            '</li>'
		        );

		        $('#form-content-option').append(
		            '<div id="tab-option-'+languages[language_count].LanguageCode+'" class="container tab-pane fade '+lang_in_active_default_active+' ">' +
		                '<div class="panel-body">' +
    
	                        '<input type="hidden" value="" class="product-option-id" name="ProductOptionID['+i+']"/>' +
	                        '<input type="hidden" name="ProductOptionLangID['+i+']" value="'+languages[language_count].LanguageID+'">' +

	                        '<div class="row option-group-view" id="option-group-view-'+i+'">' +
								
							'</div>' +		

	                        '<div class="row">' +
	                        	
	                        	'<div class="col-md-6">' +
			                        '<div class="form-group">' +
			                            '<label class="control-label">Ürün SKU</label>' +
			                            '<div class="input-area">' +
			                                '<input name="ProductOptionSKU['+i+']" class="form-control product-sku" type="text">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
		                        '</div>' +

		                        '<div class="col-md-6">' +
		                        	'<div class="form-group">' +
			                            '<label class="control-label">Ürün Adet</label>' +
			                            '<div class="input-area">' +
			                                '<input name="ProductOptionQuantity['+i+']" class="form-control product-quantity" placeholder="Limitsiz ise boş bırakınız" type="text">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
		                        '</div>' +

	                        '</div>' +

							'<div class="row">' +
	                        	
	                        	'<div class="col-md-3">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">Fiyat</label>' +
			                            '<div class="input-area">' +
			                                '<input readonly name="ProductOptionPrice['+i+']" class="form-control disabled-input product-price" type="text" onkeyup="vat_inclusive_price_calc('+i+')">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
                        	    '</div>' +

                        	    '<div class="col-md-3">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">Kdv Oranı(%)</label>' +
			                            '<div class="input-area">' +
			                                '<input name="ProductOptionVatRate['+i+']" value="18" class="form-control product-vat-rate" type="text" onkeyup="vat_inclusive_price_calc('+i+')">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
                        	    '</div>' +

                        	    '<div class="col-md-3">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">Kdv Miktarı</label>' +
			                            '<div class="input-area">' +
			                                '<input name="ProductOptionVatPrice['+i+']" class="form-control product-vat-price" type="text" onkeyup="vat_inclusive_price_calc('+i+')">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
                        	    '</div>' +

                        	    '<div class="col-md-3">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">Kdvli Fiyat</label>' +
			                            '<div class="input-area">' +
			                                '<input name="ProductOptionVatInclusivePrice['+i+']" class="form-control active-input product-vat-inclusive-price" type="text" onkeyup="price_calc('+i+')">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
                        	    '</div>' +

	                        '</div>' +

	                        '<div class="row">' +

                        	    '<div class="col-md-4">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">İndirim Oranı(%)</label>' +
			                            '<div class="input-area">' +
			                                '<input name="ProductOptionDiscountRate['+i+']" class="form-control product-discount-rate" type="text" onkeyup="discount_rate_calc('+i+')">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
                        	    '</div>' +

                        	    '<div class="col-md-4">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">İndirim Miktarı</label>' +
			                            '<div class="input-area">' +
			                                '<input name="ProductOptionDiscountPrice['+i+']" class="form-control product-discount-price" type="text" onkeyup="discount_price_calc('+i+')">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
                        	    '</div>' +

                        	    '<div class="col-md-4">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">İndirimli Fiyat(Son Fiyat)</label>' +
			                            '<div class="input-area">' +
			                                '<input name="ProductOptionLatestPrice['+i+']" class="form-control product-latest-price" type="text" onkeyup="latest_price_calc('+i+')">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
                        	    '</div>' +

	                        '</div>' +

	                        '<div class="second-title second-currency">' +
	                        	'İkinci fiyat girme alanı(para birimi olarak ayarladığınız ikinci para birimi)' +
                        	'</div>' +

	                        '<div class="row second-currency">' +
	                        	
	                        	'<div class="col-md-3">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">İkinci Fiyat</label>' +
			                            '<div class="input-area">' +
			                                '<input readonly name="ProductOptionSecondPrice['+i+']" class="form-control product-second-price" type="text" onkeyup="vat_inclusive_price_second_calc('+i+')">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
                        	    '</div>' +

                        	    '<div class="col-md-3">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">İkinci Kdv Oranı(%)</label>' +
			                            '<div class="input-area">' +
			                                '<input name="ProductOptionSecondVatRate['+i+']" value="18" class="form-control product-second-vat-rate" type="text" onkeyup="vat_inclusive_price_second_calc('+i+')">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
                        	    '</div>' +

                        	    '<div class="col-md-3">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">İkinci Kdv Miktarı</label>' +
			                            '<div class="input-area">' +
			                                '<input name="ProductOptionSecondVatPrice['+i+']" class="form-control product-second-vat-price" type="text" onkeyup="vat_inclusive_price_second_calc('+i+')">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
                        	    '</div>' +

                        	    '<div class="col-md-3">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">İkinci Kdvli Fiyat</label>' +
			                            '<div class="input-area">' +
			                                '<input name="ProductOptionSecondVatInclusivePrice['+i+']" class="form-control product-vat-inclusive-second-price" type="text" onkeyup="price_second_calc('+i+')">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
                        	    '</div>' +

	                        '</div>' +

	                        '<div class="row second-currency">' +

                        	    '<div class="col-md-4">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">İkinci İndirim Oranı(%)</label>' +
			                            '<div class="input-area">' +
			                                '<input name="ProductOptionSecondDiscountRate['+i+']" class="form-control product-second-discount-rate" type="text" onkeyup="discount_rate_second_calc('+i+')">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
                        	    '</div>' +

                        	    '<div class="col-md-4">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">İkinci İndirim Miktarı</label>' +
			                            '<div class="input-area">' +
			                                '<input name="ProductOptionSecondDiscountPrice['+i+']" class="form-control product-second-discount-price" type="text" onkeyup="discount_price_second_calc('+i+')">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
                        	    '</div>' +

                        	    '<div class="col-md-4">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">İkinci İndirimli Fiyat(Son Fiyat)</label>' +
			                            '<div class="input-area">' +
			                                '<input name="ProductOptionSecondLatestPrice['+i+']" class="form-control product-second-latest-price" type="text" onkeyup="latest_price_second_calc('+i+')">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
                        	    '</div>' +

	                        '</div>' +

	                        '<div class="form-group" style="display:'+ img_display +'">' +
                                '<label class="font-bold common-first-option">' +
                                	'<input type="checkbox" name="ProductOptionFirstOption" value="1">' +
                                    ' İlk opsiyon yap' +
                                '</label>' +
	                        '</div>' +

	                        '<div class="form-area-border common-gallery-'+common_img_status+'">' +
	                            '<div class="form-group">' +
	                                '<label class="control-label">Galeri</label>' +
	                                '<div>' +
	                                    '<input name="ProductOptionGallery['+i+'][]" data-gallery-id="'+i+'" type="file" class="gallery-img" multiple>' +
	                                    '<span class="help-block"></span>' +
	                                '</div>' +
	                                '<div style="display:'+ img_display +'">' +
		                                '<label class="font-bold common-gallery mb-0">' +
		                                	'<input type="checkbox" name="ProductOptionCommonGallery" value="1">' +
		                                    ' Tüm dillerde ortak görsel kullanılsın' +
		                                '</label>' +
			                        '</div>' +
	                                '<div class="gallery-area">' +
	                                    '<div id="images-area'+i+'" class="row">' +
	                                    '</div>' +
	                                    '<span class="help-block image-help-block"></span>' +
	                                '</div>' +
	                            '</div>' +
	                        '</div>' +

	                        '<div class="form-group mt-1">' +
	                        	'<button type="button" onclick="option_save()" class="option-save btn btn-primary w-100">Kaydet</button>'+
	                        '</div>' +

	                        '<div class="mt-4 form-group">' +
	                        	'<table class="table table-hover">' +
					      			'<thead>' +
							            '<tr>' +
							                '<th class="font-weight-normal">Varyasyon</th>' +
							                '<th class="font-weight-normal">Özellikler</th>' +
							                '<th class="font-weight-normal" width="90">Düzenle</th>' +
							                '<th class="font-weight-normal" width="90">Sil</th>' +
							            '</tr>' +
							        '</thead>' +
							        '<tbody class="option-list" id="options-'+languages[language_count].LanguageID+'">' +
							         
							        '</tbody>' +
				      			'</table>' +
			      			'</div>' +

		                '</div>' +
		            '</div>'
		        );

				CKEDITOR.replace('ProductDescription['+i+']',{
		            filebrowserWindowWidth: '900',
		            filebrowserWindowHeight: '700',
		            filebrowserBrowseUrl: base_url + 'assets/plugins/ckfinder/ckfinder_.php',
		            filebrowserImageBrowseUrl: base_url + 'assets/plugins/ckfinder/ckfinder_.php?Type=Images',
		            filebrowserUploadUrl: base_url + 'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		            filebrowserImageUploadUrl: base_url + 'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images'
		        }); 

		        language_count++;
		    }

		    $(document).on('change','#lang',function(){
		        var lang_id = $(this).val();
		        table.ajax.url(base_url + 'product/main/list_all/' + lang_id).load();
		    });

		    $('.main-category').change(function() {
		        let new_value = $(this).val();
		        $('.main-category').val(new_value);

		        $('.main-category').parent().parent().removeClass('has-error');
		        $('.main-category').next().empty();

		        ajax_sub_categories(new_value);
		        $('.sub-category').html('<option value="0">Seçiniz...</option>');

		        for (var i = 1; i <= languages.length; i++)
    			{
			        for (var m = 0; m < sub_categories.length; m++)
			        {
			            if (sub_categories[m].CategoryLangID == languages[i-1].LanguageID) {
			                $('#sub-category-'+i).append('<option value="'+sub_categories[m].CategoryContentID+'" data-category-id="'+sub_categories[m].CategoryID+'">'+sub_categories[m].CategoryName+'</option>');
			            }
			        }
			    }
		    });

		    $('.sub-category').change(function() {
		        let new_value = $(this).val();
		        let new_category_id = $(this).find(':selected').data('category-id');
		        $('.sub-category').val(new_value);
		        $('.sub-category-hidden').val(new_category_id);

		        $('.sub-category').parent().parent().removeClass('has-error');
		        $('.sub-category').next().empty();
		    });

		    $('.product-code').change(function() {
		    	let new_value = $(this).val();
		        $('.product-code').val(new_value);
		    });

		    $('.product-sku').change(function() {
		    	let new_value = $(this).val();
		        $('.product-sku').val(new_value);
		    });

		    $('.product-quantity').change(function() {
		    	let new_value = $(this).val();
		        $('.product-quantity').val(new_value);
		    });

	     	$('.common-img input[type="checkbox"]').change(function() {
		        if($(this).is(":checked")) 
		        {
		            $('.common-img-1').css('display','none');
		            $('.common-img input[type="checkbox"]').click();
		        }
		        else
		        {
		            $('.common-img-1').css('display','block');
		            $('.common-img input[type="checkbox"]').click();
		        }      
		    });

		    $('.common-gallery input[type="checkbox"]').change(function() {
		        if($(this).is(":checked")) 
		        {
		            $('.common-gallery-1').css('display','none');
		            $('.common-gallery input[type="checkbox"]').click();
		        }
		        else
		        {
		            $('.common-gallery-1').css('display','block');
		            $('.common-gallery input[type="checkbox"]').click();
		        }      
		    });

		    $('.common-first-option input[type="checkbox"]').change(function() {
		        if($(this).is(":checked")) 
		        {
		            $('.common-first-option input[type="checkbox"]').click();
		        }
		        else
		        {
		            $('.common-first-option input[type="checkbox"]').click();
		        }      
		    });

		    $('.gallery-img').change(function(){

		        var gallery_id = $(this).attr('data-gallery-id');

		        var formData = new FormData($('#option-form')[0]);
		    
		        $.ajax({
		            url : base_url + 'product/product_option_photo/gallery_upload/' + gallery_id,
		            type: "POST",
		            data: formData,
		            contentType: false,
		            processData: false,
		            dataType: "JSON",
		            success: function(data)
		            {
		                for(var x = 0; x < data.length; x++)
		                {
		                    let image_path = data[x].split('.');
		                    let img_class = image_path[0].split('/');
		                    $('#images-area'+gallery_id).append(
		                        '<div class="col-lg-3 col-md-12 image-area-class '+gallery_id+img_class[3]+'">' +
		                            '<div class="img-area">' +
		                                '<div class="img-close close'+x+'" id="'+gallery_id+img_class[3]+'" >X</div>' +
		                                '<div class="img-class">' +
		                                    '<img src="'+server_url+data[x]+'" class="mx-auto d-block" width="160" />' +
		                                    '<input type="hidden" name="PhotoSrc['+gallery_id+'][]" value="'+data[x]+'" />' +
		                                '</div>' +
		                                '<div class="image-text">' +
		                                    '<input type="text" name="PhotoTitle['+gallery_id+'][]" placeholder="Görsel içeriği" class="form-control" />' +
		                                '</div>' +
		                                '<div class="image-sort">' +
		                                    '<input type="text" name="PhotoSort['+gallery_id+'][]" placeholder="Sıra" class="form-control" />' +
		                                '</div>' +
		                            '</div>' +
		                        '</div>'
		                    );

		                    $('.close'+x).click(function(){
			                    var imgId = $(this).attr('id');
			                    $('.'+imgId).remove();
			                });
		                }
		            },
		            error: function (jqXHR, textStatus, errorThrown)
		            {
		                alert('Error get data from ajax');
		            }
		        });
		    });

		    $('.product-price, .product-vat-rate, .product-vat-price, .product-vat-inclusive-price, .product-discount-rate, .product-discount-price, .product-latest-price').change(function() {
		        $('.product-price').val($('.product-price').val());
		        $('.product-vat-rate').val($('.product-vat-rate').val());
		        $('.product-vat-price').val($('.product-vat-price').val());
		        $('.product-vat-inclusive-price').val($('.product-vat-inclusive-price').val());
		        $('.product-discount-rate').val($('.product-discount-rate').val());
		        $('.product-discount-price').val($('.product-discount-price').val());
		        $('.product-latest-price').val($('.product-latest-price').val());
		    });

		    $('.product-second-price, .product-second-vat-rate, .product-second-vat-price, .product_second_vat_inclusive_price, .product-second-discount-rate, .product-second-discount-price, .product-second-latest-price').change(function() {
		        $('.product-second-price').val($('.product-second-price').val());
		        $('.product-second-vat-rate').val($('.product-second-vat-rate').val());
		        $('.product-second-vat-price').val($('.product-second-vat-price').val());
		        $('.product_second_vat_inclusive_price').val($('.product_second_vat_inclusive_price').val());
		        $('.product-second-discount-rate').val($('.product-second-discount-rate').val());
		        $('.product-second-discount-price').val($('.product-second-discount-price').val());
		        $('.product-second-latest-price').val($('.product-second-latest-price').val());
		    });

		    $('.input-area input, .input-area textarea, .input-area select').change(function(){
		        $(this).parent().parent().removeClass('has-error');
		        $(this).next().empty();
		    });

		    slim_select_tag_create();

		});

		function ajax_main_categories(parent_id)
		{
		    $.ajax({
		        url : base_url + 'product/category/list_by_parent_id/' + parent_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            main_categories = data.categories;
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        },
		        async: false,
		    });
		}

		function ajax_sub_categories(parent_id)
		{
		    $.ajax({
		        url : base_url + 'product/category/list_by_parent_id/' + parent_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            sub_categories = data.categories;
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        },
		        async: false,
		    });
		}

		function ajax_option_groups()
		{
		    $.ajax({
		        url : base_url + 'product/option_group/groups',
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            option_groups = data.option_groups;
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        },
		        async: false,
		    });
		}

		function ajax_options(group_content_id)
		{
		    $.ajax({
		        url : base_url + 'product/option/list_by_group_content_id/' + group_content_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            option_list = data.options;
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        },
		        async: false,
		    });
		}

		function slim_select_tag_create()
		{
			slim_select_tag = new SlimSelect({
			  	select: '#tags-1',
			  	searchingText: 'Etiket ara...',
			  	placeholder: 'Seçiniz',
			  	addable: function (value) 
		  		{
		  			let tag_status = false;
		  			let tag_content_id = '';
		  			
		  			$.ajax({
			            url : base_url + 'product/tag/add',
			            async: false,
			            type: "POST",
			            data: { 'TagLangID[1]': 1, 'TagLangID[2]': 2, 'TagTitle[1]' : value , 'TagTitle[2]' : value},
			            dataType: "JSON",
			            success: function(data)
			            {
			            	if(data.status) 
			            	{
			            		tag_status = true;
			            		tag_content_id = data.TagContentID
			            	}
			            	else
			            	{
			            		tag_status = false;
			            	}
			            },
			            error: function (jqXHR, textStatus, errorThrown)
			            {
			                alert('Error get data from ajax');
			            }
			        });

			        if (tag_status)
			        {
	            		return {
	            			text: value,
	            			value: tag_content_id
	            		};
			        }
			        else
			        {
			        	return false;
			        }
			  	},
			  	ajax: function (search, callback) {

				    if (search.length < 3) {
				     	callback('3 karakter gerekiyor')
				      	return
				    }

				    $.ajax({
				        url : base_url + 'product/tag/search?title=' + search,
				        type: "GET",
				        dataType: "JSON",
				        success: function(tags)
				        {
				        	let data = []

				      		for (let i = 0; i < tags.length; i++) {
				        		data.push({value: tags[i].TagContentID, text: tags[i].TagTitle})
				      		}
				      		callback(data)
				        },
				        error: function (jqXHR, textStatus, errorThrown)
				        {
				            callback(false)
				        }
				    });
			  	}
			});
		}

		function vat_inclusive_price_calc(i)
		{
			price = $('[name="ProductOptionPrice['+i+']"]').val();
			vat_rate = $('[name="ProductOptionVatRate['+i+']"]').val();
			vat_price = $('[name="ProductOptionVatPrice['+i+']"]').val();
			vat_inclusive_price = $('[name="ProductOptionVatInclusivePrice['+i+']"]').val();

			vat_inclusive_price = price * (1 + (vat_rate / 100));
			vat_price = vat_inclusive_price - price;

			$('[name="ProductOptionVatInclusivePrice['+i+']"]').val(round_number(vat_inclusive_price));
			$('[name="ProductOptionVatPrice['+i+']"]').val(round_number(vat_price));

			discount_reset(i);
		}

		function price_calc(i)
		{
			price = $('[name="ProductOptionPrice['+i+']"]').val();
			vat_rate = $('[name="ProductOptionVatRate['+i+']"]').val();
			vat_price = $('[name="ProductOptionVatPrice['+i+']"]').val();
			vat_inclusive_price = $('[name="ProductOptionVatInclusivePrice['+i+']"]').val();

			price = vat_inclusive_price / (1 + (vat_rate / 100));
			vat_price = vat_inclusive_price - price;
			
			$('[name="ProductOptionPrice['+i+']"]').val(round_number(price));
			$('[name="ProductOptionVatPrice['+i+']"]').val(round_number(vat_price));

			discount_reset(i);
		}

		function discount_rate_calc(i)
		{
			vat_inclusive_price = $('[name="ProductOptionVatInclusivePrice['+i+']"]').val();
			discount_rate = $('[name="ProductOptionDiscountRate['+i+']"]').val();
			discount_price = $('[name="ProductOptionDiscountPrice['+i+']"]').val();
			latest_price = $('[name="ProductOptionLatestPrice['+i+']"]').val();

			latest_price = ((100 - discount_rate) / 100) * vat_inclusive_price;
			discount_price = vat_inclusive_price - latest_price;

			$('[name="ProductOptionLatestPrice['+i+']"]').val(round_number(latest_price));
			$('[name="ProductOptionDiscountPrice['+i+']"]').val(round_number(discount_price));
		}

		function discount_price_calc(i)
		{
			vat_inclusive_price = $('[name="ProductOptionVatInclusivePrice['+i+']"]').val();
			discount_rate = $('[name="ProductOptionDiscountRate['+i+']"]').val();
			discount_price = $('[name="ProductOptionDiscountPrice['+i+']"]').val();
			latest_price = $('[name="ProductOptionLatestPrice['+i+']"]').val();

			latest_price = vat_inclusive_price - discount_price;
			discount_rate = (discount_price / vat_inclusive_price) * 100;

			$('[name="ProductOptionLatestPrice['+i+']"]').val(round_number(latest_price));
			$('[name="ProductOptionDiscountRate['+i+']"]').val(round_number(discount_rate));
		}

		function latest_price_calc(i)
		{
			vat_inclusive_price = $('[name="ProductOptionVatInclusivePrice['+i+']"]').val();
			discount_rate = $('[name="ProductOptionDiscountRate['+i+']"]').val();
			discount_price = $('[name="ProductOptionDiscountPrice['+i+']"]').val();
			latest_price = $('[name="ProductOptionLatestPrice['+i+']"]').val();

			discount_price = vat_inclusive_price - latest_price;
			discount_rate = (discount_price / vat_inclusive_price) * 100;

			$('[name="ProductOptionDiscountPrice['+i+']"]').val(round_number(discount_price));
			$('[name="ProductOptionDiscountRate['+i+']"]').val(round_number(discount_rate));
		}

		function discount_reset(i)
		{
			$('[name="ProductOptionDiscountRate['+i+']"]').val(0);
			$('[name="ProductOptionDiscountPrice['+i+']"]').val(0);
			$('[name="ProductOptionLatestPrice['+i+']"]').val(vat_inclusive_price);
		}

		function vat_inclusive_price_second_calc(i)
		{
			price_second = $('[name="ProductOptionSecondPrice['+i+']"]').val();
			vat_rate_second = $('[name="ProductSOptionecondVatRate['+i+']"]').val();
			vat_price_second = $('[name="ProductOptionSecondVatPrice['+i+']"]').val();
			vat_inclusive_price_second = $('[name="ProductOptionSecondVatInclusivePrice['+i+']"]').val();

			vat_inclusive_price_second = price_second * (1 + (vat_rate_second / 100));
			vat_price_second = vat_inclusive_price_second - price_second;

			$('[name="ProductOptionSecondVatInclusivePrice['+i+']"]').val(round_number(vat_inclusive_price_second));
			$('[name="ProductOptionSecondVatPrice['+i+']"]').val(round_number(vat_price_second));

			discount_second_reset(i);
		}

		function price_second_calc(i)
		{
			price_second = $('[name="ProductOptionSecondPrice['+i+']"]').val();
			vat_rate_second = $('[name="ProductOptionSecondVatRate['+i+']"]').val();
			vat_price_second = $('[name="ProductOptionSecondVatPrice['+i+']"]').val();
			vat_inclusive_price_second = $('[name="ProductOptionSecondVatInclusivePrice['+i+']"]').val();

			price_second = vat_inclusive_price_second / (1 + (vat_rate_second / 100));
			vat_price_second = vat_inclusive_price_second - price_second;
			
			$('[name="ProductOptionSecondPrice['+i+']"]').val(round_number(price_second));
			$('[name="ProductOptionSecondVatPrice['+i+']"]').val(round_number(vat_price_second));

			discount_second_reset(i);
		}

		function discount_rate_second_calc(i)
		{
			vat_inclusive_price_second = $('[name="ProductOptionSecondVatInclusivePrice['+i+']"]').val();
			discount_rate_second = $('[name="ProductOptionSecondDiscountRate['+i+']"]').val();
			discount_price_second = $('[name="ProductOptionSecondDiscountPrice['+i+']"]').val();
			latest_price_second = $('[name="ProductOptionSecondLatestPrice['+i+']"]').val();

			latest_price_second = ((100 - discount_rate_second) / 100) * vat_inclusive_price_second;
			discount_price_second = vat_inclusive_price_second - latest_price_second;

			$('[name="ProductOptionSecondLatestPrice['+i+']"]').val(round_number(latest_price_second));
			$('[name="ProductOptionSecondDiscountPrice['+i+']"]').val(round_number(discount_price_second));
		}

		function discount_price_second_calc(i)
		{
			vat_inclusive_price_second = $('[name="ProductOptionSecondVatInclusivePrice['+i+']"]').val();
			discount_rate_second = $('[name="ProductOptionSecondDiscountRate['+i+']"]').val();
			discount_price_second = $('[name="ProductOptionSecondDiscountPrice['+i+']"]').val();
			latest_price_second = $('[name="ProductOptionSecondLatestPrice['+i+']"]').val();

			latest_price_second = vat_inclusive_price_second - discount_price_second;
			discount_rate_second = (discount_price_second / vat_inclusive_price_second) * 100;

			$('[name="ProductOptionSecondLatestPrice['+i+']"]').val(round_number(latest_price_second));
			$('[name="ProductOptionSecondDiscountRate['+i+']"]').val(round_number(discount_rate_second));
		}

		function latest_price_second_calc(i)
		{
			vat_inclusive_price_second = $('[name="ProductOptionSecondVatInclusivePrice['+i+']"]').val();
			discount_rate_second = $('[name="ProductOptionSecondDiscountRate['+i+']"]').val();
			discount_price_second = $('[name="ProductOptionSecondDiscountPrice['+i+']"]').val();
			latest_price_second = $('[name="ProductOptionSecondLatestPrice['+i+']"]').val();

			discount_price_second = vat_inclusive_price_second - latest_price_second;
			discount_rate_second = (discount_price_second / vat_inclusive_price_second) * 100;

			$('[name="ProductOptionSecondDiscountPrice['+i+']"]').val(round_number(discount_price_second));
			$('[name="ProductOptionSecondDiscountRate['+i+']"]').val(round_number(discount_rate_second));
		}

		function discount_second_reset(i)
		{
			$('[name="ProductOptionSecondDiscountRate['+i+']"]').val(0);
			$('[name="ProductOptionSecondDiscountPrice['+i+']"]').val(0);
			$('[name="ProductOptionSecondLatestPrice['+i+']"]').val(vat_inclusive_price_second);
		}

		function add()
		{
			save_method = 'add';
			$('#form')[0].reset();
		    $('.form-group').removeClass('has-error');
		    $('.help-block').empty(); 
		    $('.photo-class').hide();
		    $('.product-title').text('Ürün Ekle');
		    $('#form-modal').modal('show');
		    $('#error-message').text('');
		    $('.nav-tabs .nav-item a, .tab-pane').removeClass('active show'); 
		    $('#active-id, .default-active').addClass('active show');
		    $('.common-img input[type="checkbox"]').removeAttr('checked');
    		$('.common-img-1').css('display','block');

    		for (var i = 1; i <= languages.length; i++)
		    {
			    slim_select_group[(i-1)].set([]);
			    $('[name="ProductDescription['+i+']"]').val(CKEDITOR.instances['detail_'+i].setData(''));
			}

			$('#tags-1').html('');
			slim_select_tag_create();
		}

		function edit(content_id)
		{
			save_method = 'update';
		    $('#form')[0].reset();
		    $('.form-group').removeClass('has-error');
		    $('.help-block').empty();
		    $('#error-message').text('');

		    $.ajax({
		        url : base_url + 'product/main/view/' + content_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('#form-modal').modal('show'); 
	                $('.photo-class').show(); 
	                $('.product-title').text('Ürün Düzenle');
	                $('.nav-tabs .nav-item a, .tab-pane').removeClass('active show'); 
	    			$('#active-id, .default-active').addClass('active show');

		            for (var i = 1; i <= data.total_record; i++) 
		            {   
		                $('[name="ProductID['+i+']"]').val(data[i].ProductID);
		                $('[name="ProductContentID"]').val(data[i].ProductContentID);
		                $('[name="ProductName['+i+']"]').val(data[i].ProductName);
		                $('[name="ProductCode['+i+']"]').val(data[i].ProductCode);
		                $('[name="ProductMainCategoryID['+i+']"]').val(data[i].ProductMainCategoryID);
		                $('[name="ProductMainCategoryID['+i+']"]').change();
		                $('[name="ProductCategoryContentID['+i+']"]').val(data[i].ProductCategoryContentID);
		                $('[name="ProductCategoryContentID['+i+']"]').change();
		                $('[name="ProductCategoryID['+i+']"]').val(data[i].ProductCategoryID);
		                // $('[name="ProductDescription['+i+']"]').val(data[i].ProductDescription);
		                CKEDITOR.instances['detail_'+i].setData(data[i].ProductDescription);
		                $('[name="ProductPhotoAlt['+i+']"]').val(data[i].ProductPhotoAlt);
		                $('[name="ProductLangID['+i+']"]').val(data[i].ProductLangID);

		                slim_select_group[(i-1)].set([]);

		                if (data[i].ProductOptionGroups)
		                {
		                	$.each(JSON.parse(data[i].ProductOptionGroups), function(count, item) {
			                	slim_select_group[(i-1)].set(item)
		                	});
		                }

		                if (data[i].ProductCommonPhoto == 0) 
		                {
		                    $('.common-img-1').css('display','block');
		                    $('.common-img input[type="checkbox"]').removeAttr('checked');
		                }
		                else if(data[i].ProductCommonPhoto == 1)
		                {
		                    $('.common-img-1').css('display','none');
		                    $('.common-img input[type="checkbox"]').attr('checked', 'checked');
		                }

		                if(data[i].ProductPhoto)
		                {
		                    $('#photo-preview-' +i + ' div').html('<img src="'+server_url+data[i].ProductPhoto+'" class="img-responsive" width="100">'); 
		                    $('#photo-preview-' +i + ' div').append('<input type="hidden" name="RemovePhoto['+i+']" value="'+data[i].ProductPhoto+'"/>'); 
		                }
		                else
		                {
		                    $('#photo-preview-' +i + ' div').text('(Fotoğraf Yok)');
		                }
		            }

		            if (data[1].ProductTags)
	                {
	                	$.ajax({
					        url : base_url + 'product/tag/search?content_ids=' + data[1].ProductTags,
					        type: "GET",
					        dataType: "JSON",
					        success: function(tags)
					        {
					        	$('#tags-1').html('');

					      		for (let i = 0; i < tags.length; i++) {
					        		$('#tags-1').append('<option value="'+tags[i].TagContentID+'">'+tags[i].TagTitle+'</option>');
					      		}

					      		slim_select_tag_create();
					      		
					      		
				      			$.each(JSON.parse(data[1].ProductTags), function(count, item) {
				                	slim_select_tag.set(item);
			                	});
					        },
					    });
	                }
	                else
	                {
	                	$('#tags-1').html('');
	                	slim_select_tag_create();
	                }
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		}

		function save()
		{
			for (var i = 1; i <= languages.length; i++)
		    {
			    $('[name="ProductDescription['+i+']"]').val(CKEDITOR.instances['detail_'+i].getData());
			}

			let url;
			let notify_message;
			let notify_type;
			let formData = new FormData($('#form')[0]);

		    $('#btn-save').text('Kaydediyor...');
		    $('#btn-save').attr('disabled',true);

		    if(save_method == 'add') 
		    {
		        url = base_url + 'product/main/add';
		        notify_message = 'Kayıt başarılı bir şekilde eklendi.';
		        notify_type = 'success';
		    } 
		    else
		    {
		        url = base_url + 'product/main/update';
		        notify_message = 'Kayıt başarılı bir şekilde düzenlendi.';
		        notify_type = 'info';
		    }

		    $.ajax({
		        url : url,
		        type: 'POST',
		        data: formData,
		        contentType: false,
		        processData: false,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) 
		            {
	                    reload_table();
	                    $('#form-modal').modal('hide');
	                    $('.nav-tabs .nav-item a').removeClass('active show');
	                    $('.tab-pane').removeClass('active show'); 
	                    $('#active-id').addClass('active show');
	                    $('.default-active').addClass('active show');

	                    $.notify({
	                        icon: 'glyphicon glyphicon-ok',
	                        message: notify_message
	                    },{
	                        type: notify_type,
	                        offset: {
	                            x: 23,
	                            y: 53
	                        },
	                        animate: {
	                            enter: 'animated fadeInRight',
	                            exit: 'animated fadeOutRight'
	                        }
	                    });

		                $('#error-message').text('');
		            }
		            else
		            {
				        $('.input-area input, .input-area select').parent().parent().removeClass('has-error');
				        $('.input-area input, .input-area select').next().empty();
				        $('.option-groups-help-block').text('');

		                for (var i = 0; i < data.error.inputerror.length; i++) 
		                {   
		                	if (data.error.inputerror[i][data.error.lang[i]] == 'ProductOptionGroups') 
		                	{
		                		$('.option-groups-help-block').text(data.error.error_string[i][data.error.lang[i]]); 
		                	}
		                	else if (data.error.inputerror[i][data.error.lang[i]] == 'ProductDescription')
		                	{
		                		console.log(data.error.error_string[i][data.error.lang[i]]);
		                	}
		                	else
		                	{
		                		$('[name="'+data.error.inputerror[i][data.error.lang[i]]+'['+data.error.lang[i]+']"]').parent().parent().addClass('has-error'); 
		                    	$('[name="'+data.error.inputerror[i][data.error.lang[i]]+'['+data.error.lang[i]+']"]').next().text(data.error.error_string[i][data.error.lang[i]]); 
		                	}
		                }
		                
		                $('#error-message').text('*Gerekli alanları doldurmadan işleme devam edemezsiniz.');
		                $('#form-modal').stop().animate({
		                    scrollTop:0
		                });
		            }
		           
		            $('#btn-save').text('Kaydet'); 
		            $('#btn-save').attr('disabled',false);
		            
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error adding / update data');
		            $('#btn-save').text('Kaydet'); 
		            $('#btn-save').attr('disabled',false);
		        }
		    });
		}

		function destroy(content_id)
		{
			let notify_icon;
			let notify_type;

			Swal.fire({
				title: 'Emin misiniz?',
				text: "Sil derseniz kayıt silinecektir.",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonText: 'Vazgeç',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sil'
			}).then((result) => {
				if (result.value) 
				{
					$.ajax({
				        url : base_url + 'product/main/destroy/' + content_id,
				        type: 'DELETE',
				        success: function(data)
				        {
				        	data = JSON.parse(data);

				        	if(data.status)
				        	{
				        		notify_icon = 'glyphicon glyphicon-remove';
				        		notify_type = 'danger';

				        		reload_table();
				        	}
				        	else
				        	{
				        		notify_icon = 'glyphicon glyphicon-info-sign';
				        		notify_type = 'warning';
				        	}

				            $.notify({
				                icon: notify_icon,
				                message: data.message, 
				            },{
				                type: notify_type,
				                offset: {
				                    x: 23,
		                            y: 53
				                },
				                animate: {
				                    enter: 'animated fadeInRight',
				                    exit: 'animated fadeOutRight'
				                }
				            });
				        },
				        error: function (jqXHR, textStatus, errorThrown)
				        {
				            alert('Error deleting data');
				        }
				    });
			  	}
			});
		}

		function view(content_id)
		{
			$('#accordion').html('');
			$('.options-content').html('');
			$('#view-modal').modal('show'); 

			$.ajax({
		        url : base_url + 'product/main/view_all/' + content_id,
		        type: "GET",
		        dataType: "JSON",
		        success: function(data)
		        {
		        	let lang_name = '';
		        	let quantity = '';
		        	let sku = '';

		            for (var i = 1; i <= data.total_record; i++) 
		            {   
		            	if (languages[i-1].LanguageID == data[i].ProductLangID) 
		            	{
		            		lang_name = languages[i-1].LanguageName;
		            	}

		            	accordion_show = (i == 1 ? 'show' : '');

		                $('#accordion').append(
		                	'<div class="card">' +
						    	'<div class="card-header" id="heading'+i+'">' +
						      		'<h5 class="mb-0">' +
						        		'<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse'+i+'" aria-expanded="false" aria-controls="collapse'+i+'"><i class="la la-angle-double-down"></i>' +
						        			lang_name +
						        		'</button>' +
						      		'</h5>' +
						    	'</div>' +
						    	'<div id="collapse'+i+'" class="collapse '+accordion_show+'" aria-labelledby="heading'+i+'" data-parent="#accordion">' +
						      		'<div class="card-body">' +
						      			'<div class="view-item"><b>Ürün Adı: </b>' + data[i].ProductName + '</div>' +
						      			'<div class="view-item"><b>Ürün Kodu: </b>' + data[i].ProductCode + '</div>' +
						      			'<div class="view-item"><b>Kategori: </b>' + data[i].ProductCategoryName + '</div>' +
						      			'<div class="view-item"><b>Açıklama: </b>' + (data[i].ProductDescription ? data[i].ProductDescription : '') + '</div>' +
						      			'<div class="view-item"><b>Ürün Görseli: </b></div>' +
						      			'<div class="view-item"><img src=' + server_url + data[i].ProductPhoto + ' width="200" /></div>' +
						      			'<div class="options-content mt-3" id="options-content-'+data[i].ProductLangID+'">' +
						      				'<div class="row">' +
						      				'</div>' +
						      			'</div>' +
						      		'</div>'+
						    	'</div>' +
						  	'</div>'
	                	);

       //          	 	$.each(data[i].ProductOptions, function(i, item)
			    //     	{
			    //     		quantity = item.ProductOptionQuantity == null ? 'Limitsiz' : item.ProductOptionQuantity;
							// sku = item.ProductOptionSKU == null ? '' : item.ProductOptionSKU;

							// $('#options-content-' + item.ProductOptionLangID + ' .row').append(
							// 	'<div class="col-md-4 mt-2">' +
					  //               '<div class="view-item"><b>Opsiyon Grubu: </b>' + item.ProductOptionGroupName + '</div>' +
					  //               '<div class="view-item"><b>Opsiyon: </b>' + item.ProductOptionOptionName + '</div>' +
				   //              	'<div class="view-item"><b>Ürün SKU: </b>' + sku + '</div>' +
							// 		'<div class="view-item"><b>Adet: </b>' + quantity + '</div>' +
							// 		'<div class="view-item"><b>Fiyat: </b>' + item.ProductOptionPrice + '</div>' +
							// 		'<div class="view-item"><b>Kdvli Fiyat: </b>' + item.ProductOptionVatInclusivePrice + '</div>' +
							// 		'<div class="view-item"><b>İndirimli Fiyat(Son Fiyat): </b>' + item.ProductOptionLatestPrice + '</div>' +
							// 	'</div>'
		     //            	);
	      //           	});
		            }
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		}

		function option_group_create(group_total, group_detail)
		{
			let group_arr = group_detail.split('-');

			$('.option-group-view').html('');
		    for (var i = 1; i <= languages.length; i++)
		    {
		        for (var group = 1; group <= group_total; group++) 
			    {
			    	$('#option-group-view-' + i).append(
			    		'<div class="col-md-6">' +
	                        '<div class="form-group">' +
	                            '<label class="control-label">Opsiyon Grubu</label>' +
	                            '<div class="input-area">' +
	                                '<select class="form-control group-select group-select-'+group+'" id="group-select-'+i+'-'+group+'" data-group="'+group+'" name="ProductOptionGroupContentIDDisabled['+i+']['+group+']">' +
	                                    '<option value="0">Seçiniz...</option>' +
	                                '</select>' +
	                                '<input type="hidden" class="group-hidden-'+group+'" name="ProductOptionGroupContentIDs['+i+']['+group+']" />' +
	                                '<span class="help-block"></span>' +
	                            '</div>' +
	                        '</div>' +
	                    '</div>' +

	                    '<div class="col-md-6">' +
	                        '<div class="form-group">' +
	                            '<label class="control-label">Opsiyon</label>' +
	                            '<div class="input-area">' +
	                                '<select class="form-control option-select option-select-'+group+'" id="option-select-'+i+'-'+group+'" data-option="'+group+'" name="ProductOptionOptionContentIDs['+i+']['+group+']">' +
	                                    '<option value="0">Seçiniz...</option>' +
	                                '</select>' +
	                                '<span class="help-block"></span>' +
	                            '</div>' +
	                        '</div>' +
	                    '</div>'
		    		);

			    	for (var m = 0; m < option_groups.length; m++)
			        {
			            if (option_groups[m].GroupLangID == languages[i-1].LanguageID) 
			            {
			                $('#group-select-'+i+'-'+group).append('<option value="'+option_groups[m].GroupContentID+'">'+option_groups[m].GroupName+'</option>');
			            }
			        }
			    }
		    }

		    $('.group-select').change(function() {
		        let new_value = $(this).val();
		        let data_group = $(this).attr('data-group');

		        $('.group-select-'+data_group).val(new_value);
		        $('.group-hidden-'+data_group).val(new_value);
		        $('.group-select-'+data_group).parent().parent().removeClass('has-error');
		        $('.group-select-'+data_group).next().empty();

		        ajax_options(new_value);

		        $('.option-select-'+data_group).html('<option value="0">Seçiniz...</option>');
		        for (var z = 1; z <= languages.length; z++)
    			{
			        for (var m = 0; m < option_list.length; m++)
			        {
			            if (option_list[m].OptionLangID == z) 
			            {
		                	$('#option-select-'+z+'-'+data_group).append('<option value="'+option_list[m].OptionContentID+'">'+option_list[m].OptionName+'</option>');
			            }
			        }
			    }
		    });

		    $('.option-select').change(function() {
		        let new_value = $(this).val();
		        let data_option = $(this).attr('data-option');

		        $('.option-select-'+data_option).val(new_value);
		        $('.option-select-'+data_option).parent().parent().removeClass('has-error');
		        $('.option-select-'+data_option).next().empty();
		    });

		    $('.group-select').attr('disabled', 'disabled');
		    
		    setTimeout(function(){ 
		    	$.each(group_arr, function(count, item) {
	            	$('.group-select-'+(count+1)).val(item);
	            	$('.group-select-'+(count+1)).change();
	        	});
		    }, 400);
		}

		function option_add(product_id, product_content_id, product_name, group_total, group_detail)
		{
			save_method_option = 'add';
			product_option_name = product_name;
			product_group_total = group_total;
			product_group_detail = group_detail

			console.log('option_add1: ' + group_detail);
			console.log('option_add2: ' + product_group_detail);
			$('#option-form')[0].reset();
			$('.product-option-id, .product-option-content-id').val('');
			$('[name="ProductOptionProductID"]').val(product_id);
			$('[name="ProductOptionProductContentID"]').val(product_content_id);
		    $('.form-group').removeClass('has-error');
		    $('.help-block').empty(); 
		    $('.modal-title .option-title').text('Opsiyon Ekle');
		    $('.modal-title .product-title').text('(Ürün Adı: ' + product_option_name + ')');
		    $('.option-save').text('Yeni Ekle');
		    $('#option-modal').modal('show');
		    $('#error-message-option').text('');
		    $('.nav-tabs .nav-item a, .tab-pane').removeClass('active show');
		    $('#active-id, .default-active').addClass('active show');
		    $('.option-list').html('');
		    $('.image-area-class').remove();
		    $('.common-gallery input[type="checkbox"]').removeAttr('checked');
    		$('.common-gallery-1').css('display','block');
    		$('.common-first-option input[type="checkbox"]').removeAttr('checked');

		    $.ajax({
		        url : base_url + 'product/product_option/list_by_product_content_id/' + product_content_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	let quantity = '';
					let sku = '';
					let first_option = '';

		        	$.each(data.options, function(i, item)
		        	{
		        		quantity = item.ProductOptionQuantity == null ? 'Limitsiz' : item.ProductOptionQuantity;
						sku = item.ProductOptionSKU == null ? '' : item.ProductOptionSKU;
						first_option = item.ProductOptionFirstOption == 1 ? ' (*İlk Opsiyon)' : '';

						$('#options-' + item.ProductOptionLangID).append(
				            '<tr>' +
				                '<td>' + item.ProductOptionOptionNames + first_option + '</td>' +
				                '<td>' + 
				                	'<div class="view-item"><b>Ürün SKU: </b>' + sku + '</div>' +
									'<div class="view-item"><b>Adet: </b>' + quantity + '</div>' +
									'<div class="view-item"><b>Fiyat: </b>' + item.ProductOptionPrice + '</div>' +
									'<div class="view-item"><b>Kdvli Fiyat: </b>' + item.ProductOptionVatInclusivePrice + '</div>' +
									'<div class="view-item"><b>İndirimli Fiyat(Son Fiyat): </b>' + item.ProductOptionLatestPrice + '</div>' +
			                	'</td>' +
				                '<td>' +
				                	'<a class="btn btn-warning text-white w-100 p-1" href="javascript:void(0)" onclick="option_edit('+item.ProductOptionContentID+')">' +
										'<i class="la la-pen-nib"></i>' +
									'</a>' +
				                '</td>' +
				                '<td>' +
				                	'<a class="btn btn-danger w-100 p-1" href="javascript:void(0)" onclick="option_destroy('+item.ProductOptionContentID+', '+product_id+', '+ product_content_id+', \''+ product_option_name+'\')">' +
										'<i class="la la-trash-alt"></i>' +
									'</a>' +
				                '</td>' +
				            '</tr>'
	                	);
                	});
	        	}
		    });

		    option_group_create(product_group_total, product_group_detail);
		}

		function option_edit(product_option_content_id)
		{
			save_method_option = 'update';
		    $('#option-form')[0].reset();
		    $('.form-group').removeClass('has-error');
		    $('.help-block').empty();
		    $('#error-message-option').text('');
		    $('.option-save').text('Güncelle');
		    $('.image-area-class').remove();

		    $.ajax({
		        url : base_url + 'product/product_option/view/' + product_option_content_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	let group_arr = product_group_detail.split('-');
                	$.each(group_arr, function(count, item) {
		            	$('.group-select-'+(count+1)).val(item);
		            	$('.group-select-'+(count+1)).change();
		        	});

		            for (var i = 1; i <= data.total_record; i++) 
		            {   
		            	$('[name="ProductOptionID['+i+']"]').val(data[i].ProductOptionID);
		                $('[name="ProductOptionContentID"]').val(data[i].ProductOptionContentID);
		                $('[name="ProductOptionProductID"]').val(data[i].ProductOptionProductID);
		                $('[name="ProductOptionProductContentID"]').val(data[i].ProductOptionProductContentID);
		                $('[name="ProductOptionSKU['+i+']"]').val(data[i].ProductOptionSKU);
		                $('[name="ProductOptionQuantity['+i+']"]').val(data[i].ProductOptionQuantity);
		                $('[name="ProductOptionPrice['+i+']"]').val(data[i].ProductOptionPrice);
		                $('[name="ProductOptionVatRate['+i+']"]').val(data[i].ProductOptionVatRate);
		                $('[name="ProductOptionVatPrice['+i+']"]').val(data[i].ProductOptionVatPrice);
		                $('[name="ProductOptionVatInclusivePrice['+i+']"]').val(data[i].ProductOptionVatInclusivePrice);
		                $('[name="ProductOptionDiscountRate['+i+']"]').val(data[i].ProductOptionDiscountRate);
		                $('[name="ProductOptionDiscountPrice['+i+']"]').val(data[i].ProductOptionDiscountPrice);
		                $('[name="ProductOptionLatestPrice['+i+']"]').val(data[i].ProductOptionLatestPrice);
		                $('[name="ProductOptionSecondPrice['+i+']"]').val(data[i].ProductOptionSecondPrice);
		                $('[name="ProductOptionSecondVatRate['+i+']"]').val(data[i].ProductOptionSecondVatRate);
		                $('[name="ProductOptionSecondVatPrice['+i+']"]').val(data[i].ProductOptionSecondVatPrice);
		                $('[name="ProductOptionSecondVatInclusivePrice['+i+']"]').val(data[i].ProductOptionSecondVatInclusivePrice);
		                $('[name="ProductOptionSecondDiscountRate['+i+']"]').val(data[i].ProductOptionSecondDiscountRate);
		                $('[name="ProductOptionSecondDiscountPrice['+i+']"]').val(data[i].ProductOptionSecondDiscountPrice);
		                $('[name="ProductOptionSecondLatestPrice['+i+']"]').val(data[i].ProductOptionSecondLatestPrice);
		                $('[name="ProductOptionLangID['+i+']"]').val(data[i].ProductOptionLangID);

				    	$.each(JSON.parse(data[i].ProductOptionOptionContentIDs), function(count, item) {
		                	$('[name="ProductOptionOptionContentIDs['+i+']['+count+']"]').val(item);
	                	});

		                if (data[i].ProductOptionCommonGallery == 0) 
		                {
		                    $('.common-gallery-1').css('display','block');
		                    $('.common-gallery input[type="checkbox"]').removeAttr('checked');
		                }
		                else if(data[i].ProductOptionCommonGallery == 1)
		                {
		                    $('.common-gallery-1').css('display','none');
		                    $('.common-gallery input[type="checkbox"]').attr('checked', 'checked');
		                }

		                if (data[i].ProductOptionFirstOption == 0) 
		                {
		                    $('.common-first-option input[type="checkbox"]').removeAttr('checked');
		                }
		                else if(data[i].ProductOptionFirstOption == 1)
		                {
		                    $('.common-first-option input[type="checkbox"]').attr('checked', 'checked');
		                }

		                $.ajax({
		                    url : base_url + 'product/product_option_photo/list_by_product_option_id/' + data[i].ProductOptionID,
		                    type: "GET",
		                    dataType: "JSON",
		                    success: function(dataImg)
		                    {
		                        if (dataImg != null) 
		                        {
		                            for(var x = 0; x < dataImg.length; x++)
		                            {
		                                let langID = dataImg[x].PhotoLangID;
		                                let image_path = dataImg[x].PhotoName.split('.');
		                                let img_class = image_path[0].split('/');
		                                $('#images-area'+langID).append(
		                                    '<div class="col-lg-3 col-md-12 image-area-class '+langID+img_class[3]+'">' +
		                                        '<div class="img-area">' +
		                                            '<div class="img-close close'+x+'" id="'+langID+img_class[3]+'" >X</div>' +
		                                            '<div class="img-class">' +
		                                                '<img src="'+server_url+dataImg[x].PhotoName+'" class="mx-auto d-block" width="160" />' +
		                                                '<input type="hidden" name="PhotoSrc['+langID+'][]" value="'+dataImg[x].PhotoName+'" />' +
		                                            '</div>' +
		                                            '<div class="image-text">' +
		                                                '<input type="text" name="PhotoTitle['+langID+'][]" value="'+dataImg[x].PhotoTitle+'" placeholder="Görsel içeriği" class="form-control" />' +
		                                            '</div>' +
		                                            '<div class="image-sort">' +
		                                                '<input type="text" name="PhotoSort['+langID+'][]" value="'+dataImg[x].PhotoSort+'" placeholder="Sıra" class="form-control" />' +
		                                            '</div>' +
		                                        '</div>' +
		                                    '</div>'
		                                );

		                                $('.close' + x).click(function(){
		                                    var imgId = $(this).attr('id');
		                                    $('.' + imgId).remove();
		                                });
		                            }
		                        }
		                     
		                    },
		                    error: function (jqXHR, textStatus, errorThrown)
		                    {
		                        alert('Error get data from ajax');
		                    }
		                });
		            }

		            $('#option-modal').stop().animate({
	                    scrollTop:0
	                });

		            $('.modal-title .option-title').text('Opsiyon Düzenle');
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		}

		function option_save()
		{
			let url;
			let notify_message;
			let notify_type;
			let formData = new FormData($('#option-form')[0]);

		    $('#option-save').text('Kaydediyor...');
		    $('#option-save').attr('disabled',true);

		    if(save_method_option == 'add') 
		    {
		        url = base_url + 'product/product_option/add';
		        notify_message = 'Opsiyon başarılı bir şekilde eklendi.';
		        notify_type = 'success';
		    } 
		    else
		    {
		        url = base_url + 'product/product_option/update';
		        notify_message = 'Opsiyon başarılı bir şekilde düzenlendi.';
		        notify_type = 'success';
		    }

		    $.ajax({
		        url : url,
		        type: 'POST',
		        data: formData,
		        contentType: false,
		        processData: false,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) 
		            {
		            	Swal.fire({
							position: 'top-end',
							icon: notify_type,
							text: notify_message,
							showConfirmButton: false,
							timer: 2000
						});

						$('#error-message-option').text('');
						
						reload_table();

		                return option_add(data.ProductOptionProductID, data.ProductOptionProductContentID, product_option_name, product_group_total, product_group_detail)
		            }
		            else
		            {
				        $('.input-area input, .input-area textarea, .input-area select').parent().parent().removeClass('has-error');
				        $('.input-area input, .input-area textarea, .input-area select').next().empty();
				        $('.image-help-block').text('');

				        let is_popup = 0;
				        
		                for (var i = 0; i < data.error.inputerror.length; i++) 
		                {
		                	if (data.error.inputerror[i][data.error.lang[i]] == 'PhotoSrc')
		                	{
		                		$('.image-help-block').text(data.error.error_string[i][data.error.lang[i]]);
		                	}
		                	else if(data.error.inputerror[i][data.error.lang[i]] == 'ProductOptionOptionContentIDs')
		                	{
		                		$('[name="'+data.error.inputerror[i][data.error.lang[i]]+'['+data.error.lang[i]+']['+data.error.inputerror_counter[i][data.error.lang[i]]+']"]').parent().parent().addClass('has-error'); 
		                		$('[name="'+data.error.inputerror[i][data.error.lang[i]]+'['+data.error.lang[i]+']['+data.error.inputerror_counter[i][data.error.lang[i]]+']"]').next().text(data.error.error_string[i][data.error.lang[i]]); 
		                	}
		                	else if(data.error.inputerror[i][data.error.lang[i]] == 'ProductOptionOptionContentIDs-popup')
		                	{
		                		is_popup = 1;
		                		Swal.fire({
									icon: 'warning',
									text: data.error.error_string[i][data.error.lang[i]],
								})
		                	}
		                	else
		                	{
		                		$('[name="'+data.error.inputerror[i][data.error.lang[i]]+'['+data.error.lang[i]+']"]').parent().parent().addClass('has-error'); 
		            			$('[name="'+data.error.inputerror[i][data.error.lang[i]]+'['+data.error.lang[i]+']"]').next().text(data.error.error_string[i][data.error.lang[i]]); 
		                	}
		                }

		                if (is_popup == 0)
		                {
		                	$('#option-modal').stop().animate({
			                    scrollTop:0
			                });
		                }

		                $('#error-message-option').text('*Gerekli alanları doldurmadan işleme devam edemezsiniz.');
		            }
		           
		            $('#option-save').text('Kaydet'); 
		            $('#option-save').attr('disabled',false);
		            
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error adding / update data');
		            $('#option-save').text('Kaydet'); 
		            $('#option-save').attr('disabled',false);
		        }
		    });
		}

		function option_destroy(content_id, product_id, product_content_id, product_name)
		{
			let notify_message;
			let notify_type;

			Swal.fire({
				title: 'Emin misiniz?',
				text: "Sil derseniz opsiyon silinecektir.",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonText: 'Vazgeç',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sil'
			}).then((result) => {
				if (result.value) 
				{
					$.ajax({
				        url : base_url + 'product/product_option/destroy/' + content_id,
				        type: 'DELETE',
				        success: function(data)
				        {
				        	data = JSON.parse(data);

				        	if(data.status)
				        	{
				        		notify_type = 'success';
				        		notify_message = data.message
				        	}
				        	else
				        	{
				        		notify_type = 'warning';
				        		notify_message = data.message
				        	}

				        	Swal.fire({
								position: 'top-end',
								icon: notify_type,
								text: notify_message,
								showConfirmButton: false,
								timer: 2000
							})

							reload_table();

							return option_add(product_id, product_content_id, product_name, product_group_total, product_group_detail)
				        },
				        error: function (jqXHR, textStatus, errorThrown)
				        {
				            alert('Error deleting data');
				        }
				    });
			  	}
			});
		}

		function reload_table()
		{
		    table.ajax.reload(null,false);
		}
	</script>

	<div class="modal fade" id="form-modal" role="dialog">
	    <div class="modal-dialog modal-lg">
	        <div class="modal-content popup-content">

	            <div class="modal-header">
			        <h5 class="product-title">Ürün Formu</h5>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>

	            <div class="modal-body form">

	                <div id="error-message" class="error-message"></div>

	                <ul class="nav nav-tabs" role="tablist" id="language-nav">
	                    
	                </ul>

	                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">

	                    <input type="hidden" value="" name="ProductContentID"/> 

	                    <div class="tab-content" id="form-content-lang">

	                    </div>

	                </form>

	            </div>

	            <div class="modal-footer">
	                <button type="button" id="btn-save" onclick="save()" class="btn btn-primary">Kaydet</button>
	                <button type="button" class="btn btn-danger" data-dismiss="modal">İptal</button>
	            </div>

	        </div>
	    </div>
	</div>

	<div class="modal fade" id="option-modal" role="dialog">
	    <div class="modal-dialog modal-xl">
	        <div class="modal-content popup-content">

	            <div class="modal-header">
			        <h5 class="modal-title">
			        	<span class="option-title">Opsiyon Formu</span>
			        	<span class="product-title"></span>
			        </h5>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>

	            <div class="modal-body form">

	                <div id="error-message-option" class="error-message"></div>

	                <ul class="nav nav-tabs" role="tablist" id="language-nav-option">
	                    
	                </ul>

	                <form action="#" id="option-form" class="form-horizontal">

	                    <input type="hidden" value="" class="product-option-content-id" name="ProductOptionContentID"/> 
	                    <input type="hidden" value="" name="ProductOptionProductID"/> 
	                    <input type="hidden" value="" name="ProductOptionProductContentID"/> 

	                    <div class="tab-content" id="form-content-option">

	                    </div>

	                </form>

	            </div>

	        </div>
	    </div>
	</div>

	<div class="modal fade" id="view-modal" role="dialog">
	    <div class="modal-dialog modal-xl">
	        <div class="modal-content popup-content">

	            <div class="modal-header">
			        <h5 class="modal-title">Görüntüleme</h5>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>

	            <div class="modal-body">

	                <div id="accordion" class="view-list">

					</div>

	            </div>

	            <div class="modal-footer">
	                <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>
	            </div>

	        </div>
	    </div>
	</div>

</body>
	
</html>