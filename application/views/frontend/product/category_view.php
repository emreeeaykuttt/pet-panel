<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Admin Panel">
    <meta name="keywords" content="Admin Panel">

    <title>Admin Panel</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>

	<div class="main-content">
		
		<?php include(dirname(__DIR__) . '/inc/left_area.php'); ?>
	
		<div class="right-content">

			<?php include(dirname(__DIR__) . '/inc/top_area.php'); ?>

			<div class="content-title">
				Kategoriler
			</div>
			
			<div class="content">

				<div class="new-add">
					<a href="javascript:void(0)" onclick="add()">
						<i class="la la-plus"></i> Yeni Ekle
					</a>
				</div>

				<div class="languages">
					<select id="lang">
						<!-- language content -->
					</select>
				</div>
				
				<div class="table-responsive">
					<table id="table" class="display" style="width:100%">
				        <thead>
				            <tr>
				            	<th class="font-weight-light">Kategori</th>
				                <th class="font-weight-light">Bağlı Olduğu Üst Kategori</th>
				                <th class="font-weight-light" width="263px">İşlemler</th>
				            </tr>
				        </thead>
				    </table>
			    </div>

			</div>
			
		</div>
				
	</div>
  
	<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
	<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

	<script type="text/javascript">

		var save_method;
		var table;
		var language_count = 0;
		var default_language;
		var languages = [];
		var main_categories;
	    var export_name = module_export_name('kategoriler');
	    var TreeHtml = '';

		ajax_languages();

		var extra_list = {
		    tree_category_list: function(active_parent_id, lang_id)
		    {
		        if(typeof active_parent_id == 'undefined'){
		            active_parent_id = -1;
		        }

		        $.getJSON(base_url + 'product/category/tree_list/' + lang_id ,function(data){
		        	TreeHtml = '';
		            tree_radio(data, 0, active_parent_id, lang_id);

		            $('.category-item').change(function(){
		                var parent_id = $(this).val();
		                $('.parent-'+parent_id).click();
		            });
		        });
		    }
		}
		
		$(document).ready(function(){

		    table = $('#table').DataTable({

		    	'language': {
		            'url': base_url + 'assets/plugins/datatables/language_tr.json'
		        },

		        'processing': true,
		        'serverSide': true,
		        'order': [],
		        'lengthMenu': [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'Tüm']],
		        'ajax': {
		            'url': base_url + 'product/category/list_all',
		            'type': 'POST'
		        },

		        'dom': '<"w-50 float-left"l><"w-50 float-left"fB><"table-content" tr><"w-50 float-left"i><"w-50 float-left"p>',

				'buttons': [
					{	
						extend: 'colvis', 
						text: 'Sütünlar'
					},
					{
						extend: 'csv', 
						text: 'CSV', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
					{
						extend: 'pdf', 
						text: 'PDF', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
				],

		  		'columns': [
		            { 'data': 'CategoryName' },
		            { 'data': 'CategoryHeadName' },
		            { 'data': 'Transactions', 'orderable': false}
		        ]

		    });

		    for (var i = 1; i <= languages.length; i++)
		    {
		        var lang_selected = '';
		        var lang_active = '';
		        var lang_active_id = 'passive-id';
		        var lang_in_active_default_active = 'fade';
		        
		        if (default_language == languages[language_count].LanguageID) 
		        {
		            lang_selected = 'selected';
		            lang_active = ' active show';
		            lang_active_id = 'active-id';
		            lang_in_active_default_active = 'show active default-active';
		        }

		        $('#lang').append('<option value="'+languages[language_count].LanguageID+'" '+lang_selected+'>'+languages[language_count].LanguageName+'</option>');

		        $('#language-nav').append(
		            '<li class="nav-item">' +
		                '<a class="nav-link'+lang_active+'" id="'+lang_active_id+'" data-toggle="tab" href="#tab-'+languages[language_count].LanguageCode+'">' +
	                		languages[language_count].LanguageName +
                		'</a>' +
		            '</li>'
		        );

		        $('#form-content-lang').append(
		            '<div id="tab-'+languages[language_count].LanguageCode+'" class="container tab-pane fade '+lang_in_active_default_active+' ">' +
		                '<div class="panel-body">' +
    
	                        '<input type="hidden" value="" name="CategoryID['+i+']" class="category-id" />' +
	                        '<input type="hidden" name="CategoryLangID['+i+']" value="'+languages[language_count].LanguageID+'">' +

	                        '<div class="form-group">' +
	                            '<label class="control-label radio-label">Bağlı Olduğu Kategori</label>' +
                                '<ul class="radio-select">' +
                                    '<li class="tree-radio" id="tree-radio'+i+'"></li>' +
                                '</ul>' +
                                '<span class="help-block radio-help-block"></span>' +
	                        '</div>' +

	                        '<div class="form-group">' +
	                            '<label class="control-label">Kategori Adı</label>' +
	                            '<div class="input-area">' +
	                                '<input name="CategoryName['+i+']" class="form-control" type="text">' +
	                                '<span class="help-block"></span>' +
	                            '</div>' +
	                        '</div>' +

		                '</div>' +
		            '</div>'
		        );

		        language_count++;
		    }

		    $(document).on('change','#lang',function(){
		        var lang_id = $(this).val();
		        table.ajax.url(base_url + 'product/category/list_all/' + lang_id).load();
		    });

		    $('.input-area input, .input-area textarea, .input-area select').change(function(){
		        $(this).parent().parent().removeClass('has-error');
		        $(this).next().empty();
		    });

		});

		function tree_radio(data, parent_id, active_parent_id, lang_id)
		{
		    var is_checked = '';

		    if(typeof active_parent_id == "undefined"){
		        active_parent_id = -1;
		    }

		    if(data[parent_id]){
		        TreeHtml += "<ul>";

		        $.each(data[parent_id],function(index,value){
		            var parent_id = index;
		         	
		         	is_checked = (active_parent_id == parent_id) ? 'checked="checked"' : '';

		            if (lang_id == value['CategoryLangID']) {

		                TreeHtml += '<li>' +
		                                '<label>' +    
		                                    '<input type="radio" ' +
		                                    	'name="CategoryParentID' + '['+lang_id+']" ' +
		                                    	'class="category-item parent-' + parent_id + '" ' +
		                                    	'parent_id="' + parent_id + '" ' +
		                                    	'value="' + parent_id + '" '+ is_checked +' />'  + 
		                                    '<span>' + value['CategoryName'] + '</span>' +
		                                '</label>';
		                
		                tree_radio(data, parent_id, active_parent_id, lang_id);

		                TreeHtml += '</li>';

		            }

		        });
		        TreeHtml += '</ul>';    
		    }

		    $('#tree-radio' + lang_id).html(TreeHtml);
		}

		function add()
		{
			save_method = 'add';
			$('#form')[0].reset();
			$('.category-id, [name="CategoryContentID"]').val('');
		    $('.form-group').removeClass('has-error');
		    $('.help-block').empty(); 
		    $('.modal-title').text('Kategori Ekle');
		    $('#form-modal').modal('show');
		    $('#error-message').text('');
		    $('.nav-tabs .nav-item a').removeClass('active show'); 
		    $('#active-id').addClass('active show');
		    $('.tab-pane').removeClass('active show'); 
		    $('.default-active').addClass('active show');

		    for (var i = 1; i <= languages.length; i++)
		    {
		        extra_list.tree_category_list('-1', languages[i-1].LanguageID);
		    }
		}

		function edit(content_id)
		{
			save_method = 'update';
		    $('#form')[0].reset();
		    $('.form-group').removeClass('has-error');
		    $('.help-block').empty();
		    $('#error-message').text('');

		    $.ajax({
		        url : base_url + 'product/category/view/' + content_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            for (var i = 1; i <= data.total_record; i++) 
		            {   
		            	extra_list.tree_category_list(data[i].CategoryParentID, data[i].CategoryLangID);

		                $('[name="CategoryID['+i+']"]').val(data[i].CategoryID);
		                $('[name="CategoryContentID"]').val(data[i].CategoryContentID);
		                $('[name="CategoryName['+i+']"]').val(data[i].CategoryName);
		                $('[name="CategoryLangID['+i+']"]').val(data[i].CategoryLangID);

		                $('#form-modal').modal('show'); 
		                $('.modal-title').text('Kategori Düzenle');
		                $('.nav-tabs .nav-item a').removeClass('active show');
	                    $('.tab-pane').removeClass('active show'); 
		                $('#active-id').addClass('active show');
		                $('.default-active').addClass('active show');
		            }
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		}

		function save()
		{
			let url;
			let notify_message;
			let notify_type;
			let formData = new FormData($('#form')[0]);

		    $('#btn-save').text('Kaydediyor...');
		    $('#btn-save').attr('disabled',true);

		    if(save_method == 'add') 
		    {
		        url = base_url + 'product/category/add';
		        notify_message = 'Kayıt başarılı bir şekilde eklendi.';
		        notify_type = 'success';
		    } 
		    else
		    {
		        url = base_url + 'product/category/update';
		        notify_message = 'Kayıt başarılı bir şekilde düzenlendi.';
		        notify_type = 'info';
		    }

		    $.ajax({
		        url : url,
		        type: 'POST',
		        data: formData,
		        contentType: false,
		        processData: false,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) 
		            {
	                    reload_table();
	                    $('#form-modal').modal('hide');
	                    $('.nav-tabs .nav-item a').removeClass('active show');
	                    $('.tab-pane').removeClass('active show'); 
	                    $('#active-id').addClass('active show');
	                    $('.default-active').addClass('active show');

	                    $.notify({
	                        icon: 'glyphicon glyphicon-ok',
	                        message: notify_message
	                    },{
	                        type: notify_type,
	                        offset: {
	                            x: 23,
	                            y: 53
	                        },
	                        animate: {
	                            enter: 'animated fadeInRight',
	                            exit: 'animated fadeOutRight'
	                        }
	                    });

		                $('#error-message').text('');
		            }
		            else
		            {
				        $('.input-area input, .input-area textarea, .input-area select').parent().parent().removeClass('has-error');
				        $('.input-area input, .input-area textarea, .input-area select').next().empty();
				        $('.radio-label').parent().removeClass('has-error');
				        $('.radio-help-block').empty();

		                for (var i = 0; i < data.error.inputerror.length; i++) 
		                {
		                	if (data.error.inputerror[i][data.error.lang[i]] == 'CategoryParentID')
		                	{
		                		$('.radio-label').parent().addClass('has-error'); 
		                    	$('.radio-help-block').text(data.error.error_string[i][data.error.lang[i]]); 
		                	}
		                	else
		                	{
		                		$('[name="'+data.error.inputerror[i][data.error.lang[i]]+'['+data.error.lang[i]+']"]').parent().parent().addClass('has-error'); 
		                    	$('[name="'+data.error.inputerror[i][data.error.lang[i]]+'['+data.error.lang[i]+']"]').next().text(data.error.error_string[i][data.error.lang[i]]); 
		                	}
		                }
		                
		                $('#error-message').text('*Gerekli alanları doldurmadan işleme devam edemezsiniz.');
		                $('#form-modal').stop().animate({
		                    scrollTop:0
		                });
		            }
		           
		            $('#btn-save').text('Kaydet'); 
		            $('#btn-save').attr('disabled',false);
		            
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error adding / update data');
		            $('#btn-save').text('Kaydet'); 
		            $('#btn-save').attr('disabled',false);
		        }
		    });
		}

		function destroy(content_id)
		{
			let notify_icon;
			let notify_type;
			
			Swal.fire({
				title: 'Emin misiniz?',
				text: "Sil derseniz kayıt silinecektir.",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonText: 'Vazgeç',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sil'
			}).then((result) => {
				if (result.value) 
				{
					$.ajax({
				        url : base_url + 'product/category/destroy/' + content_id,
				        type: 'DELETE',
				        success: function(data)
				        {
				        	data = JSON.parse(data);

				        	if(data.status)
				        	{
				        		notify_icon = 'glyphicon glyphicon-remove';
				        		notify_type = 'danger';

				        		reload_table();
				        	}
				        	else
				        	{
				        		notify_icon = 'glyphicon glyphicon-info-sign';
				        		notify_type = 'warning';
				        	}

				            $.notify({
				                icon: notify_icon,
				                message: data.message, 
				            },{
				                type: notify_type,
				                offset: {
				                    x: 23,
		                            y: 53
				                },
				                animate: {
				                    enter: 'animated fadeInRight',
				                    exit: 'animated fadeOutRight'
				                }
				            });
				        },
				        error: function (jqXHR, textStatus, errorThrown)
				        {
				            alert('Error deleting data');
				        }
				    });
			  	}
			});
		}

		function view(content_id)
		{
			$('#accordion').html('');

			$.ajax({
		        url : base_url + 'product/category/view/' + content_id,
		        type: "GET",
		        dataType: "JSON",
		        success: function(data)
		        {
		        	let lang_name = '';
		        	let accordion_show = '';
		        	
		            for (var i = 1; i <= data.total_record; i++) 
		            {   
		            	if (languages[i-1].LanguageID == data[i].CategoryLangID) 
		            	{
		            		lang_name = languages[i-1].LanguageName;
		            	}

		            	accordion_show = (i == 1 ? 'show' : '');

		                $('#accordion').append(
		                	'<div class="card">' +
						    	'<div class="card-header" id="heading'+i+'">' +
						      		'<h5 class="mb-0">' +
						        		'<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse'+i+'" aria-expanded="false" aria-controls="collapse'+i+'"><i class="la la-angle-double-down"></i>' +
						        			lang_name +
						        		'</button>' +
						      		'</h5>' +
						    	'</div>' +
						    	'<div id="collapse'+i+'" class="collapse '+accordion_show+'" aria-labelledby="heading'+i+'" data-parent="#accordion">' +
						      		'<div class="card-body">' +
						      			'<div class="view-item"><b>Kategori Adı: </b>' + data[i].CategoryName + '</div>' +
						      			'<div class="view-item"><b>Bağlı Olduğu Üst Kategori: </b>' + data[i].CategoryHeadName + '</div>' +
						      		'</div>'+
						    	'</div>' +
						  	'</div>'
	                	);
		            }

		            $('#view-modal').modal('show'); 
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		}

		function reload_table()
		{
		    table.ajax.reload(null,false);
		}
	</script>

	<div class="modal fade" id="form-modal" role="dialog">
	    <div class="modal-dialog modal-lg">
	        <div class="modal-content popup-content">

	            <div class="modal-header">
			        <h5 class="modal-title">Kategori Formu</h5>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>

	            <div class="modal-body form">

	                <div id="error-message" class="error-message"></div>

	                <ul class="nav nav-tabs" role="tablist" id="language-nav">
	                    
	                </ul>

	                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">

	                    <input type="hidden" value="" name="CategoryContentID"/> 

	                    <div class="tab-content" id="form-content-lang">

	                    </div>

	                </form>

	            </div>

	            <div class="modal-footer">
	                <button type="button" id="btn-save" onclick="save()" class="btn btn-primary">Kaydet</button>
	                <button type="button" class="btn btn-danger" data-dismiss="modal">İptal</button>
	            </div>

	        </div>
	    </div>
	</div>

	<div class="modal fade" id="view-modal" role="dialog">
	    <div class="modal-dialog modal-lg">
	        <div class="modal-content popup-content">

	            <div class="modal-header">
			        <h5 class="modal-title">Görüntüleme</h5>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>

	            <div class="modal-body">

	                <div id="accordion" class="view-list">

					</div>

	            </div>

	            <div class="modal-footer">
	                <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>
	            </div>

	        </div>
	    </div>
	</div>

</body>
	
</html>