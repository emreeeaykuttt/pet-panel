<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Admin Panel">
    <meta name="keywords" content="Admin Panel">

    <title>Admin Panel</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>
    <link href="<?=base_url()?>assets/plugins/slim-select/css/slimselect.min.css" rel="stylesheet"></link>

</head>

<body>

	<div class="main-content">
		
		<?php include(dirname(__DIR__) . '/inc/left_area.php'); ?>
	
		<div class="right-content">

			<?php include(dirname(__DIR__) . '/inc/top_area.php'); ?>

			<div class="content-title">
				Kuponlar
			</div>
			
			<div class="content">

				<div class="new-add">
					<a href="javascript:void(0)" onclick="add()">
						<i class="la la-plus"></i> Yeni Ekle
					</a>
				</div>

				<div class="languages">
					<select id="lang">
						<!-- language content -->
					</select>
				</div>
				
				<div class="table-responsive">
					<table id="table" class="display" style="width:100%">
				        <thead>
				            <tr>
				            	<th class="font-weight-light">Kupon Adı</th>
				                <th class="font-weight-light" width="411px">İşlemler</th>
				            </tr>
				        </thead>
				    </table>
			    </div>

			</div>
			
		</div>
				
	</div>
  
	<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
	<?php include(dirname(__DIR__) . '/inc/script.php'); ?>
	<script src="<?=base_url()?>assets/plugins/slim-select/js/slimselect.min.js"></script>

	<script type="text/javascript">

		var save_method;
		var save_method_user;
		var table;
		var language_count = 0;
		var default_language;
		var languages = [];
	    var export_name = module_export_name('kuponlar');
	    var coupon_name = ''
	    var slim_select_user;

		ajax_languages();
		
		$(document).ready(function(){

		    table = $('#table').DataTable({

		    	'language': {
		            'url': base_url + 'assets/plugins/datatables/language_tr.json'
		        },

		        'processing': true,
		        'serverSide': true,
		        'order': [],
		        'lengthMenu': [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'Tüm']],
		        'ajax': {
		            'url': base_url + 'product/coupon/list_all',
		            'type': 'POST'
		        },

		        'dom': '<"w-50 float-left"l><"w-50 float-left"fB><"table-content" tr><"w-50 float-left"i><"w-50 float-left"p>',

				'buttons': [
					{	
						extend: 'colvis', 
						text: 'Sütünlar'
					},
					{
						extend: 'csv', 
						text: 'CSV', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
					{
						extend: 'pdf', 
						text: 'PDF', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
				],

		  		'columns': [
		            { 'data': 'CouponName' },
		            { 'data': 'Transactions', 'orderable': false}
		        ]

		    });

		    for (var i = 1; i <= languages.length; i++)
		    {
		        var lang_selected = '';
		        var lang_active = '';
		        var lang_active_id = 'passive-id';
		        var lang_in_active_default_active = 'fade';
		        var checkbox_display = 'block';
		        
		        if (default_language == languages[language_count].LanguageID) 
		        {
		            lang_selected = 'selected';
		            lang_active = ' active show';
		            lang_active_id = 'active-id';
		            lang_in_active_default_active = 'show active default-active';
		        }

		        if (i > 1) 
		        {
		            checkbox_display = 'none';
		        }

		        $('#lang').append('<option value="'+languages[language_count].LanguageID+'" '+lang_selected+'>'+languages[language_count].LanguageName+'</option>');

		        $('#language-nav-coupon').append(
		            '<li class="nav-item">' +
		                '<a class="nav-link'+lang_active+'" id="'+lang_active_id+'" data-toggle="tab" href="#tab-'+languages[language_count].LanguageCode+'">' +
	                		languages[language_count].LanguageName +
                		'</a>' +
		            '</li>'
		        );

		        $('#form-content-coupon').append(
		            '<div id="tab-'+languages[language_count].LanguageCode+'" class="container tab-pane fade '+lang_in_active_default_active+' ">' +
		                '<div class="panel-body">' +
    
	                        '<input type="hidden" value="" name="CouponID['+i+']"/>' +
	                        '<input type="hidden" name="CouponLangID['+i+']" value="'+languages[language_count].LanguageID+'">' +

	                        '<div class="row">' +
	                        	
	                        	'<div class="col-md-12">' +
	                        		'<div class="form-group" style="display:'+ checkbox_display +'">' +
		                                '<label class="font-bold common-active">' +
		                                	'<input type="checkbox" name="CouponIsActive['+i+']" value="1">' +
		                                    ' Kupon Aktif' +
		                                '</label>' +
			                        '</div>' +
	                        	'</div>' +

	                        	'<div class="col-md-12">' +
	                        		'<div class="form-group" style="display:'+ checkbox_display +'">' +
		                                '<label class="font-bold common-public">' +
		                                	'<input type="checkbox" name="CouponPublic['+i+']" value="1">' +
		                                    ' Herkese Açık' +
		                                '</label>' +
			                        '</div>' +
	                        	'</div>' +

	                        	'<div class="col-md-6">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">Kupon Adı</label>' +
			                            '<div class="input-area">' +
			                                '<input name="CouponName['+i+']" class="form-control" type="text">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
	                        	'</div>' +

	                        	'<div class="col-md-6">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">Kupon Kodu</label>' +
			                            '<div class="input-area">' +
			                                '<input name="CouponCode['+i+']" class="form-control common-code" type="text">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
	                        	'</div>' +

	                        	'<div class="col-md-12">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">Açıklama</label>' +
			                            '<div class="input-area">' +
			                                '<textarea name="CouponDescription['+i+']" class="form-control"></textarea>' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
	                        	'</div>' +

	                        	'<div class="col-md-6">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">İndirim Miktarı(Fiyat)</label>' +
			                            '<div class="input-area">' +
			                                '<input name="CouponAmount['+i+']" class="form-control common-amount" type="text">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
	                        	'</div>' +

	                        	'<div class="col-md-6">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">İndirim Oranı(%)</label>' +
			                            '<div class="input-area">' +
			                                '<input name="CouponRate['+i+']" class="form-control common-rate" type="text" placeholder="miktar girdiyseniz oran alanını boş bırakınız">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
	                        	'</div>' +

	                        	'<div class="col-md-6">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">Minumum Satış Miktarı</label>' +
			                            '<div class="input-area">' +
			                                '<input name="CouponMinAmount['+i+']" class="form-control common-min-amount" type="text">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
	                        	'</div>' +

	                        	'<div class="col-md-6">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">Adet</label>' +
			                            '<div class="input-area">' +
			                                '<input name="CuoponQuantity['+i+']" class="form-control common-quantity" type="text" placeholder="boş bırakırsanız sınırsız anlamına gelir">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
	                        	'</div>' +

	                        	'<div class="col-md-6">' +
	                        		'<div class="form-group">' +
			                            '<label class="control-label">Başlangıç Tarihi</label>' +
			                            '<div class="input-area">' +
			                                '<input name="CouponStartDate['+i+']" class="form-control common-start-date" type="datetime-local">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
			                        '<div class="form-group" style="display:'+ checkbox_display +'">' +
		                                '<label class="font-bold common-is-end-date">' +
		                                	'<input type="checkbox" name="CouponIsEndDate['+i+']" value="1">' +
		                                    ' Sınırsız süre' +
		                                '</label>' +
			                        '</div>' +
	                        	'</div>' +

	                        	'<div class="col-md-6">' +
	                        		'<div class="form-group end-date">' +
			                            '<label class="control-label">Bitiş Tarihi</label>' +
			                            '<div class="input-area">' +
			                                '<input name="CouponEndDate['+i+']" class="form-control common-end-date" type="datetime-local">' +
			                                '<span class="help-block"></span>' +
			                            '</div>' +
			                        '</div>' +
	                        	'</div>' +

	                        '</div>' +

		                '</div>' +
		            '</div>'
		        );

		        language_count++;
		    }

		    $(document).on('change','#lang',function(){
		        var lang_id = $(this).val();
		        table.ajax.url(base_url + 'product/coupon/list_all/' + lang_id).load();
		    });

		    $('.input-area input, .input-area textarea, .input-area select').change(function(){
		        $(this).parent().parent().removeClass('has-error');
		        $(this).next().empty();
		    });

		    $('.common-is-end-date input[type="checkbox"]').change(function() {
		        if($(this).is(":checked")) 
		        {
		            $('.common-is-end-date input[type="checkbox"]').click();
		            $('.end-date').css('display', 'none');
		        }
		        else
		        {
		            $('.common-is-end-date input[type="checkbox"]').click();
		            $('.end-date').css('display', 'block');
		        }      
		    });

		    $('.common-active input[type="checkbox"]').change(function() {
		        if($(this).is(":checked")) 
		        {
		            $('.common-active input[type="checkbox"]').click();
		        }
		        else
		        {
		            $('.common-active input[type="checkbox"]').click();
		        }      
		    });

		    $('.common-public input[type="checkbox"]').change(function() {
		        if($(this).is(":checked")) 
		        {
		            $('.common-public input[type="checkbox"]').click();
		        }
		        else
		        {
		            $('.common-public input[type="checkbox"]').click();
		        }      
		    });

		    $('.common-code').change(function(){
		    	let new_value = $(this).val();
		        $('.common-code').val(new_value);
		    });

		    $('.common-amount').change(function(){
		    	let new_value = $(this).val();
		        $('.common-amount').val(new_value);
		    });

		    $('.common-rate').change(function(){
		    	let new_value = $(this).val();
		        $('.common-rate').val(new_value);
		    });

		    $('.common-min-amount').change(function(){
		    	let new_value = $(this).val();
		        $('.common-min-amount').val(new_value);
		    });

		    $('.common-quantity').change(function(){
		    	let new_value = $(this).val();
		        $('.common-quantity').val(new_value);
		    });

		    $('.common-start-date').change(function(){
		    	let new_value = $(this).val();
		        $('.common-start-date').val(new_value);
		    });

		    $('.common-end-date').change(function(){
		    	let new_value = $(this).val();
		        $('.common-end-date').val(new_value);
		    });

		   	slim_select_user_create();

		});

		function add()
		{
			save_method = 'add';
			$('#coupon-form')[0].reset();
		    $('.form-group').removeClass('has-error');
		    $('.help-block').empty(); 
		    $('.modal-title .coupon').text('Kupon Ekle');
		    $('#coupon-modal').modal('show');
		    $('#error-message').text('');
		    $('.nav-tabs .nav-item a, .tab-pane').removeClass('active show'); 
		    $('#active-id, .default-active').addClass('active show');
		    $('.common-is-end-date input[type="checkbox"]').removeAttr('checked');
		    $('.common-active input[type="checkbox"]').removeAttr('checked');
		    $('.common-public input[type="checkbox"]').removeAttr('checked');
    		$('.end-date').css('display','block');
		}

		function date_convert(val)
		{
			let now = new Date(val);
		    let day = ("0" + now.getDate()).slice(-2);
		    let month = ("0" + (now.getMonth() + 1)).slice(-2);
		    let hour = now.getHours() < 10 ? '0' + now.getHours() : now.getHours();
		    let minute = now.getMinutes() < 10 ? '0' + now.getMinutes() : now.getMinutes();
		    let new_date = now.getFullYear() + '-' + (month) + '-' + (day) + 'T' + (hour) + ":" + (minute);
		    
		    return new_date;
		}

		function edit(content_id)
		{
			save_method = 'update';
		    $('#coupon-form')[0].reset();
		    $('.form-group').removeClass('has-error');
		    $('.help-block').empty();
		    $('#error-message').text('');

		    $.ajax({
		        url : base_url + 'product/coupon/view/' + content_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            for (var i = 1; i <= data.total_record; i++) 
		            {   
		                $('[name="CouponID['+i+']"]').val(data[i].CouponID);
		                $('[name="CouponName['+i+']"]').val(data[i].CouponName);
		                $('[name="CouponCode['+i+']"]').val(data[i].CouponCode);
		                $('[name="CouponDescription['+i+']"]').val(data[i].CouponDescription);
		                $('[name="CouponAmount['+i+']"]').val(data[i].CouponAmount);
		                $('[name="CouponRate['+i+']"]').val(data[i].CouponRate);
		                $('[name="CouponMinAmount['+i+']"]').val(data[i].CouponMinAmount);
		                $('[name="CuoponQuantity['+i+']"]').val(data[i].CuoponQuantity);
		                $('[name="CouponStartDate['+i+']"]').val(date_convert(data[i].CouponStartDate));
					    $('[name="CouponEndDate['+i+']"]').val(date_convert(data[i].CouponEndDate));
		                $('[name="CouponLangID['+i+']"]').val(data[i].CouponLangID);
		                $('[name="CouponContentID"]').val(data[i].CouponContentID);

		                if (data[i].CouponIsActive == 0) 
		                {
		                    $('.common-active input[type="checkbox"]').removeAttr('checked');
		                }
		                else if(data[i].CouponIsActive == 1)
		                {
		                    $('.common-active input[type="checkbox"]').attr('checked', 'checked');
		                }

		                if (data[i].CouponPublic == 0) 
		                {
		                    $('.common-public input[type="checkbox"]').removeAttr('checked');
		                }
		                else if(data[i].CouponPublic == 1)
		                {
		                    $('.common-public input[type="checkbox"]').attr('checked', 'checked');
		                }

		                if (data[i].CouponIsEndDate == 0) 
		                {
		                    $('.common-is-end-date input[type="checkbox"]').removeAttr('checked');
		                    $('.end-date').css('display', 'block');
		                }
		                else if(data[i].CouponIsEndDate == 1)
		                {
		                    $('.common-is-end-date input[type="checkbox"]').attr('checked', 'checked');
		                    $('.end-date').css('display', 'none');
		                }

		                $('#coupon-modal').modal('show'); 
		                $('.modal-title .coupon').text('Kuponu Düzenle');
		                $('.nav-tabs .nav-item a, .tab-pane').removeClass('active show'); 
		    			$('#active-id, .default-active').addClass('active show');
		            }
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		}

		function save()
		{
			let url;
			let notify_message;
			let notify_type;
			let formData = new FormData($('#coupon-form')[0]);

		    $('#btn-save').text('Kaydediyor...');
		    $('#btn-save').attr('disabled',true);

		    if(save_method == 'add') 
		    {
		        url = base_url + 'product/coupon/add';
		        notify_message = 'Kayıt başarılı bir şekilde eklendi.';
		        notify_type = 'success';
		    } 
		    else
		    {
		        url = base_url + 'product/coupon/update';
		        notify_message = 'Kayıt başarılı bir şekilde düzenlendi.';
		        notify_type = 'info';
		    }

		    $.ajax({
		        url : url,
		        type: 'POST',
		        data: formData,
		        contentType: false,
		        processData: false,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) 
		            {
	                    reload_table();
	                    $('#coupon-modal').modal('hide');
	                    $('.nav-tabs .nav-item a, .tab-pane').removeClass('active show'); 
		    			$('#active-id, .default-active').addClass('active show');

	                    $.notify({
	                        icon: 'glyphicon glyphicon-ok',
	                        message: notify_message
	                    },{
	                        type: notify_type,
	                        offset: {
	                            x: 23,
	                            y: 53
	                        },
	                        animate: {
	                            enter: 'animated fadeInRight',
	                            exit: 'animated fadeOutRight'
	                        }
	                    });

		                $('#error-message').text('');
		            }
		            else
		            {
				        $('.input-area input, .input-area textarea, .input-area select').parent().parent().removeClass('has-error');
				        $('.input-area input, .input-area textarea, .input-area select').next().empty();

		                for (var i = 0; i < data.error.inputerror.length; i++) 
		                {
	                		$('[name="'+data.error.inputerror[i][data.error.lang[i]]+'['+data.error.lang[i]+']"]').parent().parent().addClass('has-error'); 
                    		$('[name="'+data.error.inputerror[i][data.error.lang[i]]+'['+data.error.lang[i]+']"]').next().text(data.error.error_string[i][data.error.lang[i]]); 
		                }
		                
		                $('#error-message').text('*Gerekli alanları doldurmadan işleme devam edemezsiniz.');
		                $('#coupon-modal').stop().animate({
		                    scrollTop:0
		                });
		            }
		           
		            $('#btn-save').text('Kaydet'); 
		            $('#btn-save').attr('disabled',false);
		            
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error adding / update data');
		            $('#btn-save').text('Kaydet'); 
		            $('#btn-save').attr('disabled',false);
		        }
		    });
		}

		function destroy(content_id)
		{
			let notify_icon;
			let notify_type;

			Swal.fire({
				title: 'Emin misiniz?',
				text: "Sil derseniz kupon silinecektir.",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonText: 'Vazgeç',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sil'
			}).then((result) => {
				if (result.value) 
				{
					$.ajax({
				        url : base_url + 'product/coupon/destroy/' + content_id,
				        type: 'DELETE',
				        success: function(data)
				        {
				        	data = JSON.parse(data);

				        	if(data.status)
				        	{
				        		notify_icon = 'glyphicon glyphicon-remove';
				        		notify_type = 'danger';

				        		reload_table();
				        	}
				        	else
				        	{
				        		notify_icon = 'glyphicon glyphicon-info-sign';
				        		notify_type = 'warning';
				        	}

				            $.notify({
				                icon: notify_icon,
				                message: data.message, 
				            },{
				                type: notify_type,
				                offset: {
				                    x: 23,
		                            y: 53
				                },
				                animate: {
				                    enter: 'animated fadeInRight',
				                    exit: 'animated fadeOutRight'
				                }
				            });
				        },
				        error: function (jqXHR, textStatus, errorThrown)
				        {
				            alert('Error deleting data');
				        }
				    });
			  	}
			});
		}

		function view(content_id)
		{
			$('#accordion').html('');

			$.ajax({
		        url : base_url + 'product/coupon/view/' + content_id,
		        type: "GET",
		        dataType: "JSON",
		        success: function(data)
		        {
		        	let lang_name = '';
		        	let discount_row = '';
		        	let quantity = '';
		        	let end_date = '';
		        	
		            for (var i = 1; i <= data.total_record; i++) 
		            {   
		            	if (languages[i-1].LanguageID == data[i].CouponLangID) 
		            	{
		            		lang_name = languages[i-1].LanguageName;
		            	}

		            	if (data[i].CouponAmount)
		            	{
		            		discount_row = '<div class="view-item"><b>İndirim Miktarı: </b>' + data[i].CouponAmount + '</div>';
		            	}
		            	else
		            	{
		            		discount_row = '<div class="view-item"><b>İndirim Oranı: </b>' + data[i].CouponRate + '</div>';
		            	}

		            	if (data[i].CuoponQuantity)
		            	{
		            		quantity = data[i].CuoponQuantity
		            	}
		            	else
		            	{
		            		quantity = 'Sınırsız';
		            	}

		            	if (data[i].CouponEndDate)
		            	{
		            		end_date = data[i].CouponEndDate
		            	}
		            	else
		            	{
		            		end_date = 'Bitiş Süresi Yok';
		            	}

		            	accordion_show = (i == 1 ? 'show' : '');
		            	
		                $('#accordion').append(
		                	'<div class="card">' +
						    	'<div class="card-header" id="heading'+i+'">' +
						      		'<h5 class="mb-0">' +
						        		'<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse'+i+'" aria-expanded="false" aria-controls="collapse'+i+'"><i class="la la-angle-double-down"></i>' +
						        			lang_name +
						        		'</button>' +
						      		'</h5>' +
						    	'</div>' +
						    	'<div id="collapse'+i+'" class="collapse '+accordion_show+'" aria-labelledby="heading'+i+'" data-parent="#accordion">' +
						      		'<div class="card-body">' +
						      			'<div class="view-item"><b>Kupon Adı: </b>' + data[i].CouponName + '</div>' +
						      			'<div class="view-item"><b>Kupon Kodu: </b>' + data[i].CouponCode + '</div>' +
						      			'<div class="view-item"><b>Açıklama: </b>' + data[i].CouponDescription + '</div>' +
						      			discount_row +
						      			'<div class="view-item"><b>Minumum Satış Miktarı: </b>' + data[i].CouponMinAmount + '</div>' +
						      			'<div class="view-item"><b>Adet: </b>' + quantity + '</div>' +
						      			'<div class="view-item"><b>Başlangıç Tarihi: </b>' + data[i].CouponStartDate + '</div>' +
						      			'<div class="view-item"><b>Bitiş Tarihi: </b>' + end_date + '</div>' +
						      		'</div>'+
						    	'</div>' +
						  	'</div>'
	                	);
		            }

		            $('#view-modal').modal('show'); 
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		}

		function slim_select_user_create()
		{
			slim_select_user = new SlimSelect({
			  	select: '#user-id',
			  	searchingText: 'Kişi ara...',
			  	placeholder: 'Seçiniz',
			  	ajax: function (search, callback) {

				    if (search.length < 3) {
				     	callback('3 karakter gerekiyor')
				      	return
				    }

				    $.ajax({
				        url : base_url + 'user/main/search/' + search,
				        type: "GET",
				        dataType: "JSON",
				        success: function(users)
				        {
				        	let data = []

				      		for (let i = 0; i < users.length; i++) {
				        		data.push({value: users[i].UserID, text: users[i].UserFirstName + ' ' + users[i].UserLastName})
				      		}
				      		callback(data)
				        },
				        error: function (jqXHR, textStatus, errorThrown)
				        {
				            callback(false)
				        }
				    });
			  	}
			});
		}

		function user_add(coupon_id, coupon_content_id, user_coupon_name)
		{
			save_method_option = 'add';
			coupon_name = user_coupon_name;
			$('#user-form')[0].reset();
			$('[name="UserCouponCouponID"]').val(coupon_id);
			$('[name="UserCouponCouponContentID"]').val(coupon_content_id);
		    $('.form-group').removeClass('has-error');
		    $('.help-block').empty(); 
		    $('.modal-title .user-title').text('Kullanıcı Ekle');
		    $('.modal-title .coupon-title').text('(Kupon Adı: ' + coupon_name + ')');
		    $('.user-save').text('Yeni Ekle');
		    $('#user-modal').modal('show');
		    $('#error-message-user').text('');
		    $('.user-list').html('');
		    slim_select_user_create();

		    $.ajax({
		        url : base_url + 'user/coupon/list_by_coupon_content_id/' + coupon_content_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	let status;
		        	let destroy_btn;

		        	if (data.users.status == null)
		        	{
		        		$.each(data.users, function(i, item) {

		        			if (item.UserCouponIsUsed == 1)
		        			{
		        				status = '<span class="badge badge-success">Kullanılmış</span>';
		        				destroy_btn = 'none';
		        			}
		        			else
		        			{
		        				status = '<span class="badge badge-secondary">Kullanılmamış</span>';
		        				destroy_btn = 'block';
		        			}

							$('#users').append(
					            '<tr>' +
					                '<td>' + item.UserFirstName + ' ' + item.UserLastName + '</td>' +
					                '<td>' + status + '</td>' +
					                '<td>' +
					                	'<a class="btn btn-danger w-100 p-1" style="display:'+destroy_btn+'" href="javascript:void(0)" onclick="user_destroy('+item.UserCouponID+', '+coupon_id+', '+coupon_content_id+', \''+coupon_name+'\')">' +
											'<i class="la la-trash-alt"></i>' +
										'</a>' +
					                '</td>' +
					            '</tr>'
		                	);
	                	});
		        	}
	        	}
		    });
		}

		function user_save()
		{
			let url;
			let notify_message;
			let notify_type;
			let formData = new FormData($('#user-form')[0]);

		    if(save_method_option == 'add') 
		    {
		        url = base_url + 'user/coupon/add';
		        notify_message = 'Kullanıcı başarılı bir şekilde eklendi.';
		        notify_type = 'success';
		    }

		    $.ajax({
		        url : url,
		        type: 'POST',
		        data: formData,
		        contentType: false,
		        processData: false,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) 
		            {
		            	Swal.fire({
							position: 'top-end',
							icon: notify_type,
							text: notify_message,
							showConfirmButton: false,
							timer: 2000
						})

						$('#error-message-user').text('');

						reload_table();

		                return user_add(data.CouponID, data.CouponContentID, coupon_name)
		            }
		            else
		            {
				        $('.input-area input, .input-area textarea, .input-area select').parent().parent().removeClass('has-error');
				        $('.input-area input, .input-area textarea, .input-area select').next().empty();

		                for (var i = 0; i < data.error.inputerror.length; i++) 
		                {
		                	if (data.error.inputerror[i] == 'UserCouponUserID') 
		                	{
		                		$('.user-help-block').text(data.error.error_string[i]); 
		                	}
		                	else
		                	{
		                		$('[name="'+data.error.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
		            			$('[name="'+data.error.inputerror[i]+'"]').next().text(data.error.error_string[i]); 
		                	}
		                }

		                $('#error-message-user').text('*Gerekli alanları doldurmadan işleme devam edemezsiniz.');
		            }
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error adding / update data');
		        }
		    });
		}

		function user_destroy(user_coupon_id, coupon_id, coupon_content_id, coupon_name)
		{
			let notify_message;
			let notify_type;
			let notify_timer;

			Swal.fire({
				title: 'Emin misiniz?',
				text: "Sil derseniz kullanıcı silinecektir.",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonText: 'Vazgeç',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sil'
			}).then((result) => {
				if (result.value) 
				{
					$.ajax({
				        url : base_url + 'user/coupon/destroy/' + user_coupon_id + '/' + coupon_content_id,
				        type: 'DELETE',
				        success: function(data)
				        {
				        	data = JSON.parse(data);

				        	if(data.status)
				        	{
				        		notify_type = 'success';
				        		notify_message = data.message;
				        		notify_timer = 2000;
				        	}
				        	else
				        	{
				        		notify_type = 'warning';
				        		notify_message = data.message;
				        		notify_timer = 3000;
				        	}

				        	Swal.fire({
								position: 'top-end',
								icon: notify_type,
								text: notify_message,
								showConfirmButton: false,
								timer: notify_timer,
							})

							reload_table();

							return user_add(coupon_id, coupon_content_id, coupon_name)
				        },
				        error: function (jqXHR, textStatus, errorThrown)
				        {
				            alert('Error deleting data');
				        }
				    });
			  	}
			});
		}

		function reload_table()
		{
		    table.ajax.reload(null,false);
		}
	</script>

	<div class="modal fade" id="coupon-modal" role="dialog">
	    <div class="modal-dialog modal-lg">
	        <div class="modal-content popup-content">

	            <div class="modal-header">
			        <h5 class="modal-title">
			        	<span class="coupon">Kupon Formu</span>
		        	</h5>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>

	            <div class="modal-body form">

	                <div id="error-message" class="error-message"></div>

	                <ul class="nav nav-tabs" role="tablist" id="language-nav-coupon">
	                    
	                </ul>

	                <form action="#" id="coupon-form" class="form-horizontal">

	                    <input type="hidden" value="" name="CouponContentID"/> 

	                    <div class="tab-content" id="form-content-coupon">

	                    </div>

	                </form>

	            </div>

	            <div class="modal-footer">
	                <button type="button" id="btn-save" onclick="save()" class="btn btn-primary">Kaydet</button>
	                <button type="button" class="btn btn-danger" data-dismiss="modal">İptal</button>
	            </div>

	        </div>
	    </div>
	</div>

	<div class="modal fade" id="user-modal" role="dialog">
	    <div class="modal-dialog modal-md">
	        <div class="modal-content popup-content">

	            <div class="modal-header">
			        <h5 class="modal-title">
			        	<span class="user-title">Kullanıcı Formu</span><br />
			        	<span class="coupon-title"></span>
			        </h5>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>

	            <div class="modal-body form">

	                <div id="error-message-user" class="error-message"></div>

	                <form action="#" id="user-form" class="form-horizontal panel-body">

	                    <input type="hidden" value="" name="UserCouponCouponID"/> 
	                    <input type="hidden" value="" name="UserCouponCouponContentID"/> 

	                    <div class="form-group">
                            <label class="control-label">Kullanıcı</label>
                            <div class="selectslim-area">
                            	<select name="UserCouponUserID" id="user-id">
                            	</select>
                                <span class="user-help-block help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
	                    	<button type="button" onclick="user_save()" class="user-save btn btn-primary w-100">Kaydet</button>
	                    </div>

	                    <div class="mt-4 form-group">
                        	<table class="table table-hover">
				      			<thead>
						            <tr>
						                <th class="font-weight-normal">Kullanıcı Adı</th>
						                <th class="font-weight-normal">Durumu</th>
						                <th class="font-weight-normal" width="90">Sil</th>
						            </tr>
						        </thead>
						        <tbody class="user-list" id="users">
						         
						        </tbody>
			      			</table>
		      			</div>

	                </form>

	            </div>

	        </div>
	    </div>
	</div>

	<div class="modal fade" id="view-modal" role="dialog">
	    <div class="modal-dialog modal-lg">
	        <div class="modal-content popup-content">

	            <div class="modal-header">
			        <h5 class="modal-title">Görüntüleme</h5>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>

	            <div class="modal-body">

	                <div id="accordion" class="view-list">

					</div>

	            </div>

	            <div class="modal-footer">
	                <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>
	            </div>

	        </div>
	    </div>
	</div>

</body>
	
</html>