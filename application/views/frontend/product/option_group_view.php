<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Admin Panel">
    <meta name="keywords" content="Admin Panel">

    <title>Admin Panel</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>

	<div class="main-content">
		
		<?php include(dirname(__DIR__) . '/inc/left_area.php'); ?>
	
		<div class="right-content">

			<?php include(dirname(__DIR__) . '/inc/top_area.php'); ?>

			<div class="content-title">
				Opsiyon Grupları
			</div>
			
			<div class="content">

				<div class="new-add">
					<a href="javascript:void(0)" onclick="add()">
						<i class="la la-plus"></i> Yeni Ekle
					</a>
				</div>

				<div class="languages">
					<select id="lang">
						<!-- language content -->
					</select>
				</div>
				
				<div class="table-responsive">
					<table id="table" class="display" style="width:100%">
				        <thead>
				            <tr>
				            	<th class="font-weight-light">Opsiyon Grup Adı</th>
				            	<th class="font-weight-light">Opsiyon Grup Sıralaması</th>
				                <th class="font-weight-light" width="411px">İşlemler</th>
				            </tr>
				        </thead>
				    </table>
			    </div>

			</div>
			
		</div>
				
	</div>
  
	<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
	<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

	<script type="text/javascript">

		var save_method;
		var save_method_option;
		var table;
		var language_count = 0;
		var default_language;
		var languages = [];
	    var export_name = module_export_name('urun_opsiyon_gruplari');
	    var option_group_name = '';

		ajax_languages();
		
		$(document).ready(function(){

		    table = $('#table').DataTable({

		    	'language': {
		            'url': base_url + 'assets/plugins/datatables/language_tr.json'
		        },

		        'processing': true,
		        'serverSide': true,
		        'order': [],
		        'lengthMenu': [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'Tüm']],
		        'ajax': {
		            'url': base_url + 'product/option_group/list_all',
		            'type': 'POST'
		        },

		        'dom': '<"w-50 float-left"l><"w-50 float-left"fB><"table-content" tr><"w-50 float-left"i><"w-50 float-left"p>',

				'buttons': [
					{	
						extend: 'colvis', 
						text: 'Sütünlar'
					},
					{
						extend: 'csv', 
						text: 'CSV', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
					{
						extend: 'pdf', 
						text: 'PDF', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
				],

		  		'columns': [
		            { 'data': 'GroupName' },
		            { 'data': 'GroupSort' },
		            { 'data': 'Transactions', 'orderable': false}
		        ]

		    });

		    for (var i = 1; i <= languages.length; i++)
		    {
		        var lang_selected = '';
		        var lang_active = '';
		        var lang_active_id = 'passive-id';
		        var lang_in_active_default_active = 'fade';
		        
		        if (default_language == languages[language_count].LanguageID) 
		        {
		            lang_selected = 'selected';
		            lang_active = ' active show';
		            lang_active_id = 'active-id';
		            lang_in_active_default_active = 'show active default-active';
		        }

		        $('#lang').append('<option value="'+languages[language_count].LanguageID+'" '+lang_selected+'>'+languages[language_count].LanguageName+'</option>');

		        $('#language-nav-group').append(
		            '<li class="nav-item">' +
		                '<a class="nav-link'+lang_active+'" id="'+lang_active_id+'" data-toggle="tab" href="#tab-'+languages[language_count].LanguageCode+'">' +
	                		languages[language_count].LanguageName +
                		'</a>' +
		            '</li>'
		        );

		        $('#form-content-group').append(
		            '<div id="tab-'+languages[language_count].LanguageCode+'" class="container tab-pane fade '+lang_in_active_default_active+' ">' +
		                '<div class="panel-body">' +
    
	                        '<input type="hidden" value="" name="GroupID['+i+']"/>' +
	                        '<input type="hidden" name="GroupLangID['+i+']" value="'+languages[language_count].LanguageID+'">' +

	                        '<div class="form-group">' +
	                            '<label class="control-label">Opsiyon Grup Adı</label>' +
	                            '<div class="input-area">' +
	                                '<input name="GroupName['+i+']" class="form-control" type="text">' +
	                                '<span class="help-block"></span>' +
	                            '</div>' +
	                        '</div>' +

	                        '<div class="form-group">' +
	                            '<label class="control-label">Opsiyon Grup Sıralaması</label>' +
	                            '<div class="input-area">' +
	                                '<input name="GroupSort['+i+']" class="form-control common-sort" type="text">' +
	                                '<span class="help-block"></span>' +
	                            '</div>' +
	                        '</div>' +

		                '</div>' +
		            '</div>'
		        );

		        $('#language-nav-option').append(
		            '<li class="nav-item">' +
		                '<a class="nav-link'+lang_active+'" id="'+lang_active_id+'" data-toggle="tab" href="#tab-option-'+languages[language_count].LanguageCode+'">' +
	                		languages[language_count].LanguageName +
                		'</a>' +
		            '</li>'
		        );

		        $('#form-content-option').append(
		            '<div id="tab-option-'+languages[language_count].LanguageCode+'" class="container tab-pane fade '+lang_in_active_default_active+' ">' +
		                '<div class="panel-body">' +
    
	                        '<input type="hidden" value="" name="OptionID['+i+']"/>' +
	                        '<input type="hidden" name="OptionLangID['+i+']" value="'+languages[language_count].LanguageID+'">' +

	                        '<div class="form-group">' +
	                            '<label class="control-label">Opsiyon Adı</label>' +
	                            '<div class="input-area">' +
	                                '<input name="OptionName['+i+']" class="form-control" type="text">' +
	                                '<span class="help-block"></span>' +
	                            '</div>' +
	                        '</div>' +

	                        '<div class="form-group">' +
	                            '<label class="control-label">Opsiyon Renk</label>' +
	                            '<div class="input-area">' +
	                                '<input name="OptionColor['+i+']" placeholder="isteğe bağlı" class="form-control" type="text">' +
	                                '<span class="help-block"></span>' +
	                            '</div>' +
	                        '</div>' +

	                        '<div class="form-group">' +
	                            '<label class="control-label">Opsiyon Sıralaması</label>' +
	                            '<div class="input-area">' +
	                                '<input name="OptionSort['+i+']" class="form-control common-option-sort" type="text">' +
	                                '<span class="help-block"></span>' +
	                            '</div>' +
	                        '</div>' +

	                        '<div class="form-group">' +
	                        	'<button type="button" onclick="option_save()" class="option-save btn btn-primary w-100">Kaydet</button>'+
	                        '</div>' +

	                        '<div class="mt-4 form-group">' +
	                        	'<table class="table table-hover">' +
					      			'<thead>' +
							            '<tr>' +
							                '<th class="font-weight-normal">Opsiyon Adı</th>' +
							                '<th class="font-weight-normal">Sıralaması</th>' +
							                '<th class="font-weight-normal" width="90">Düzenle</th>' +
							                '<th class="font-weight-normal" width="90">Sil</th>' +
							            '</tr>' +
							        '</thead>' +
							        '<tbody class="option-list" id="options-'+languages[language_count].LanguageID+'">' +
							         
							        '</tbody>' +
				      			'</table>' +
			      			'</div>' +

		                '</div>' +
		            '</div>'
		        );

		        language_count++;
		    }

		    $(document).on('change','#lang',function(){
		        var lang_id = $(this).val();
		        table.ajax.url(base_url + 'product/option_group/list_all/' + lang_id).load();
		    });

		    $('.input-area input, .input-area textarea, .input-area select').change(function(){
		        $(this).parent().parent().removeClass('has-error');
		        $(this).next().empty();
		    });

		    $('.common-sort').change(function(){
		    	let new_value = $(this).val();
		        $('.common-sort').val(new_value);
		    });

		    $('.common-option-sort').change(function(){
		    	let new_value = $(this).val();
		        $('.common-option-sort').val(new_value);
		    });

		});

		function add()
		{
			save_method = 'add';
			$('#group-form')[0].reset();
		    $('.form-group').removeClass('has-error');
		    $('.help-block').empty(); 
		    $('.modal-title').text('Opsiyon Grup Başlığı Ekle');
		    $('#group-modal').modal('show');
		    $('#error-message').text('');
		    $('.nav-tabs .nav-item a, .tab-pane').removeClass('active show'); 
		    $('#active-id, .default-active').addClass('active show');
		}

		function edit(content_id)
		{
			save_method = 'update';
		    $('#group-form')[0].reset();
		    $('.form-group').removeClass('has-error');
		    $('.help-block').empty();
		    $('#error-message').text('');

		    $.ajax({
		        url : base_url + 'product/option_group/view/' + content_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            for (var i = 1; i <= data.total_record; i++) 
		            {   
		                $('[name="GroupID['+i+']"]').val(data[i].GroupID);
		                $('[name="GroupContentID"]').val(data[i].GroupContentID);
		                $('[name="GroupName['+i+']"]').val(data[i].GroupName);
		                $('[name="GroupSort['+i+']"]').val(data[i].GroupSort);
		                $('[name="GroupLangID['+i+']"]').val(data[i].GroupLangID);

		                $('#group-modal').modal('show'); 
		                $('.modal-title').text('Opsiyon Grup Başlığını Düzenle');
		                $('.nav-tabs .nav-item a, .tab-pane').removeClass('active show'); 
		    			$('#active-id, .default-active').addClass('active show');
		            }
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		}

		function save()
		{
			let url;
			let notify_message;
			let notify_type;
			let formData = new FormData($('#group-form')[0]);

		    $('#btn-save').text('Kaydediyor...');
		    $('#btn-save').attr('disabled',true);

		    if(save_method == 'add') 
		    {
		        url = base_url + 'product/option_group/add';
		        notify_message = 'Kayıt başarılı bir şekilde eklendi.';
		        notify_type = 'success';
		    } 
		    else
		    {
		        url = base_url + 'product/option_group/update';
		        notify_message = 'Kayıt başarılı bir şekilde düzenlendi.';
		        notify_type = 'info';
		    }

		    $.ajax({
		        url : url,
		        type: 'POST',
		        data: formData,
		        contentType: false,
		        processData: false,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) 
		            {
	                    reload_table();
	                    $('#group-modal').modal('hide');
	                    $('.nav-tabs .nav-item a, .tab-pane').removeClass('active show'); 
		    			$('#active-id, .default-active').addClass('active show');

	                    $.notify({
	                        icon: 'glyphicon glyphicon-ok',
	                        message: notify_message
	                    },{
	                        type: notify_type,
	                        offset: {
	                            x: 23,
	                            y: 53
	                        },
	                        animate: {
	                            enter: 'animated fadeInRight',
	                            exit: 'animated fadeOutRight'
	                        }
	                    });

		                $('#error-message').text('');
		            }
		            else
		            {
				        $('.input-area input, .input-area textarea, .input-area select').parent().parent().removeClass('has-error');
				        $('.input-area input, .input-area textarea, .input-area select').next().empty();

		                for (var i = 0; i < data.error.inputerror.length; i++) 
		                {
	                		$('[name="'+data.error.inputerror[i][data.error.lang[i]]+'['+data.error.lang[i]+']"]').parent().parent().addClass('has-error'); 
                    		$('[name="'+data.error.inputerror[i][data.error.lang[i]]+'['+data.error.lang[i]+']"]').next().text(data.error.error_string[i][data.error.lang[i]]); 
		                }
		                
		                $('#error-message').text('*Gerekli alanları doldurmadan işleme devam edemezsiniz.');
		                $('#group-modal').stop().animate({
		                    scrollTop:0
		                });
		            }
		           
		            $('#btn-save').text('Kaydet'); 
		            $('#btn-save').attr('disabled',false);
		            
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error adding / update data');
		            $('#btn-save').text('Kaydet'); 
		            $('#btn-save').attr('disabled',false);
		        }
		    });
		}

		function destroy(content_id)
		{
			let notify_icon;
			let notify_type;

			Swal.fire({
				title: 'Emin misiniz?',
				text: "Sil derseniz opsiyon grubu ve altındaki opsiyonlar silinecektir.",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonText: 'Vazgeç',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sil'
			}).then((result) => {
				if (result.value) 
				{
					$.ajax({
				        url : base_url + 'product/option_group/destroy/' + content_id,
				        type: 'DELETE',
				        success: function(data)
				        {
				        	data = JSON.parse(data);

				        	if(data.status)
				        	{
				        		notify_icon = 'glyphicon glyphicon-remove';
				        		notify_type = 'danger';

				        		reload_table();
				        	}
				        	else
				        	{
				        		notify_icon = 'glyphicon glyphicon-info-sign';
				        		notify_type = 'warning';
				        	}

				            $.notify({
				                icon: notify_icon,
				                message: data.message, 
				            },{
				                type: notify_type,
				                offset: {
				                    x: 23,
		                            y: 53
				                },
				                animate: {
				                    enter: 'animated fadeInRight',
				                    exit: 'animated fadeOutRight'
				                }
				            });
				        },
				        error: function (jqXHR, textStatus, errorThrown)
				        {
				            alert('Error deleting data');
				        }
				    });
			  	}
			});
		}

		function view(content_id)
		{
			$('#accordion').html('');
			$('.option-list').html('');

			$.ajax({
		        url : base_url + 'product/option_group/view/' + content_id,
		        type: "GET",
		        dataType: "JSON",
		        success: function(data)
		        {
		        	let lang_name = '';
		        	let accordion_show = '';
		        	
		            for (var i = 1; i <= data.total_record; i++) 
		            {   
		            	if (languages[i-1].LanguageID == data[i].GroupLangID) 
		            	{
		            		lang_name = languages[i-1].LanguageName;
		            	}

		            	accordion_show = (i == 1 ? 'show' : '');
		            	
		                $('#accordion').append(
		                	'<div class="card">' +
						    	'<div class="card-header" id="heading'+i+'">' +
						      		'<h5 class="mb-0">' +
						        		'<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse'+i+'" aria-expanded="false" aria-controls="collapse'+i+'"><i class="la la-angle-double-down"></i>' +
						        			lang_name +
						        		'</button>' +
						      		'</h5>' +
						    	'</div>' +
						    	'<div id="collapse'+i+'" class="collapse '+accordion_show+'" aria-labelledby="heading'+i+'" data-parent="#accordion">' +
						      		'<div class="card-body">' +
						      			'<div class="view-item"><b>Opsiyon Grup Adı: </b>' + data[i].GroupName + '</div>' +
						      			'<div class="view-item"><b>Seçenekler: </b></div>' +
						      			'<div class="option-list" id="option-list'+i+'"></div>' +
						      		'</div>'+
						    	'</div>' +
						  	'</div>'
	                	);

		                $.each(data[i].Options, function(count, item) {
						    $('#option-list' + i).append(
						    	'<div class="view-item"> -'+ item.OptionName +'</div>'
	                		);
						});
		            }

		            $('#view-modal').modal('show'); 
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		}

		function option_add(group_id, group_content_id, group_name)
		{
			save_method_option = 'add';
			option_group_name = group_name;
			$('#option-form')[0].reset();
			$('[name="OptionGroupID"]').val(group_id);
			$('[name="OptionGroupContentID"]').val(group_content_id);
		    $('.form-group').removeClass('has-error');
		    $('.help-block').empty(); 
		    $('.modal-title .option-title').text('Opsiyon Ekle');
		    $('.modal-title .group-title').text('(Grup Adı: ' + option_group_name + ')');
		    $('.option-save').text('Yeni Ekle');
		    $('#option-modal').modal('show');
		    $('#error-message-option').text('');
		    $('.nav-tabs .nav-item a, .tab-pane').removeClass('active show');
		    $('#active-id, .default-active').addClass('active show');
		    $('.option-list').html('');

		    $.ajax({
		        url : base_url + 'product/option/list_by_group_content_id/' + group_content_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$.each(data.options, function(i, item) {
						$('#options-' + item.OptionLangID).append(
				            '<tr>' +
				                '<td>' + item.OptionName + '</td>' +
				                '<td>' + (item.OptionSort ? item.OptionSort : '') + '</td>' +
				                '<td>' +
				                	'<a class="btn btn-warning text-white w-100 p-1" href="javascript:void(0)" onclick="option_edit('+item.OptionContentID+')">' +
										'<i class="la la-pen-nib"></i>' +
									'</a>' +
				                '</td>' +
				                '<td>' +
				                	'<a class="btn btn-danger w-100 p-1" href="javascript:void(0)" onclick="option_destroy('+item.OptionContentID+', '+group_id+', '+group_content_id+', \''+option_group_name+'\')">' +
										'<i class="la la-trash-alt"></i>' +
									'</a>' +
				                '</td>' +
				            '</tr>'
	                	);
                	});
	        	}
		    });
		}

		function option_edit(option_content_id)
		{
			save_method_option = 'update';
		    $('#option-form')[0].reset();
		    $('.form-group').removeClass('has-error');
		    $('.help-block').empty();
		    $('#error-message-option').text('');
		    $('.option-save').text('Güncelle');

		    $.ajax({
		        url : base_url + 'product/option/view/' + option_content_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            for (var i = 1; i <= data.total_record; i++) 
		            {   
		                $('[name="OptionID['+i+']"]').val(data[i].OptionID);
		                $('[name="OptionID"]').val(data[i].OptionID);
		                $('[name="OptionContentID"]').val(data[i].OptionContentID);
		                $('[name="OptionName['+i+']"]').val(data[i].OptionName);
		                $('[name="OptionColor['+i+']"]').val(data[i].OptionColor);
		                $('[name="OptionSort['+i+']"]').val(data[i].OptionSort);
		                $('[name="OptionGroupContentID"]').val(data[i].OptionGroupContentID);
		                $('[name="OptionLangID['+i+']"]').val(data[i].OptionLangID);
		            }

		            $('#option-modal').stop().animate({
	                    scrollTop:0
	                });

		            $('.modal-title .option-title').text('Opsiyon Düzenle');
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		}

		function option_save()
		{
			let url;
			let notify_message;
			let notify_type;
			let formData = new FormData($('#option-form')[0]);

		    if(save_method_option == 'add') 
		    {
		        url = base_url + 'product/option/add';
		        notify_message = 'Opsiyon başarılı bir şekilde eklendi.';
		        notify_type = 'success';
		    } 
		    else
		    {
		        url = base_url + 'product/option/update';
		        notify_message = 'Opsiyon başarılı bir şekilde düzenlendi.';
		        notify_type = 'success';
		    }

		    $.ajax({
		        url : url,
		        type: 'POST',
		        data: formData,
		        contentType: false,
		        processData: false,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) 
		            {
		            	Swal.fire({
							position: 'top-end',
							icon: notify_type,
							text: notify_message,
							showConfirmButton: false,
							timer: 2000
						})

						$('#error-message-option').text('');

						reload_table();

		                return option_add(data.OptionGroupID, data.OptionGroupContentID, option_group_name)
		            }
		            else
		            {
				        $('.input-area input, .input-area textarea, .input-area select').parent().parent().removeClass('has-error');
				        $('.input-area input, .input-area textarea, .input-area select').next().empty();

		                for (var i = 0; i < data.error.inputerror.length; i++) 
		                {
		            		$('[name="'+data.error.inputerror[i][data.error.lang[i]]+'['+data.error.lang[i]+']"]').parent().parent().addClass('has-error'); 
		            		$('[name="'+data.error.inputerror[i][data.error.lang[i]]+'['+data.error.lang[i]+']"]').next().text(data.error.error_string[i][data.error.lang[i]]); 
		                }

		                $('#error-message-option').text('*Gerekli alanları doldurmadan işleme devam edemezsiniz.');
		            }
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error adding / update data');
		        }
		    });
		}

		function option_destroy(content_id, group_id, group_content_id, group_name)
		{
			let notify_message;
			let notify_type;
			let notify_timer;

			Swal.fire({
				title: 'Emin misiniz?',
				text: "Sil derseniz opsiyon silinecektir.",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonText: 'Vazgeç',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sil'
			}).then((result) => {
				if (result.value) 
				{
					$.ajax({
				        url : base_url + 'product/option/destroy/' + content_id,
				        type: 'DELETE',
				        success: function(data)
				        {
				        	data = JSON.parse(data);

				        	if(data.status)
				        	{
				        		notify_type = 'success';
				        		notify_message = data.message;
				        		notify_timer = 2000;
				        	}
				        	else
				        	{
				        		notify_type = 'warning';
				        		notify_message = data.message;
				        		notify_timer = 3000;
				        	}

				        	Swal.fire({
								position: 'top-end',
								icon: notify_type,
								text: notify_message,
								showConfirmButton: false,
								timer: notify_timer,
							})

							reload_table();

							return option_add(group_id, group_content_id, group_name)
				        },
				        error: function (jqXHR, textStatus, errorThrown)
				        {
				            alert('Error deleting data');
				        }
				    });
			  	}
			});
		}

		function reload_table()
		{
		    table.ajax.reload(null,false);
		}
	</script>

	<div class="modal fade" id="group-modal" role="dialog">
	    <div class="modal-dialog modal-lg">
	        <div class="modal-content popup-content">

	            <div class="modal-header">
			        <h5 class="modal-title">Opsiyon Grup Formu</h5>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>

	            <div class="modal-body form">

	                <div id="error-message" class="error-message"></div>

	                <ul class="nav nav-tabs" role="tablist" id="language-nav-group">
	                    
	                </ul>

	                <form action="#" id="group-form" class="form-horizontal">

	                    <input type="hidden" value="" name="GroupContentID"/> 

	                    <div class="tab-content" id="form-content-group">

	                    </div>

	                </form>

	            </div>

	            <div class="modal-footer">
	                <button type="button" id="btn-save" onclick="save()" class="btn btn-primary">Kaydet</button>
	                <button type="button" class="btn btn-danger" data-dismiss="modal">İptal</button>
	            </div>

	        </div>
	    </div>
	</div>

	<div class="modal fade" id="option-modal" role="dialog">
	    <div class="modal-dialog modal-md">
	        <div class="modal-content popup-content">

	            <div class="modal-header">
			        <h5 class="modal-title">
			        	<span class="option-title">Opsiyon Formu</span>
			        	<span class="group-title"></span>
			        </h5>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>

	            <div class="modal-body form">

	                <div id="error-message-option" class="error-message"></div>

	                <ul class="nav nav-tabs" role="tablist" id="language-nav-option">
	                    
	                </ul>

	                <form action="#" id="option-form" class="form-horizontal">

	                    <input type="hidden" value="" name="OptionContentID"/> 
	                    <input type="hidden" value="" name="OptionGroupID"/> 
	                    <input type="hidden" value="" name="OptionGroupContentID"/> 

	                    <div class="tab-content" id="form-content-option">

	                    </div>

	                </form>

	            </div>

	        </div>
	    </div>
	</div>

	<div class="modal fade" id="view-modal" role="dialog">
	    <div class="modal-dialog modal-lg">
	        <div class="modal-content popup-content">

	            <div class="modal-header">
			        <h5 class="modal-title">Görüntüleme</h5>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>

	            <div class="modal-body">

	                <div id="accordion" class="view-list">

					</div>

	            </div>

	            <div class="modal-footer">
	                <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>
	            </div>

	        </div>
	    </div>
	</div>

</body>
	
</html>