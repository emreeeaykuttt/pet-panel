<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Admin Panel">
    <meta name="keywords" content="Admin Panel">

    <title>Admin Panel</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>

	<div class="main-content">
		
		<?php include(dirname(__DIR__) . '/inc/left_area.php'); ?>
	
		<div class="right-content">

			<?php include(dirname(__DIR__) . '/inc/top_area.php'); ?>

			<div class="content-title">
				Etiketler
			</div>
			
			<div class="content">

				<div class="new-add">
					<a href="javascript:void(0)" onclick="add()">
						<i class="la la-plus"></i> Yeni Ekle
					</a>
				</div>

				<div class="languages">
					<select id="lang">
						<!-- language content -->
					</select>
				</div>
				
				<div class="table-responsive">
					<table id="table" class="display" style="width:100%">
				        <thead>
				            <tr>
				            	<th class="font-weight-light">Etiket Adı</th>
				                <th class="font-weight-light" width="263px">İşlemler</th>
				            </tr>
				        </thead>
				    </table>
			    </div>

			</div>
			
		</div>
				
	</div>
  
	<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
	<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

	<script type="text/javascript">

		var save_method;
		var table;
		var language_count = 0;
		var default_language;
		var languages = [];
	    var export_name = module_export_name('etiketler');

		ajax_languages();
		
		$(document).ready(function(){

		    table = $('#table').DataTable({

		    	'language': {
		            'url': base_url + 'assets/plugins/datatables/language_tr.json'
		        },

		        'processing': true,
		        'serverSide': true,
		        'order': [],
		        'lengthMenu': [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'Tüm']],
		        'ajax': {
		            'url': base_url + 'product/tag/list_all',
		            'type': 'POST'
		        },

		        'dom': '<"w-50 float-left"l><"w-50 float-left"fB><"table-content" tr><"w-50 float-left"i><"w-50 float-left"p>',

				'buttons': [
					{	
						extend: 'colvis', 
						text: 'Sütünlar'
					},
					{
						extend: 'csv', 
						text: 'CSV', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
					{
						extend: 'pdf', 
						text: 'PDF', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
				],

		  		'columns': [
		            { 'data': 'TagTitle' },
		            { 'data': 'Transactions', 'orderable': false}
		        ]

		    });

		    for (var i = 1; i <= languages.length; i++)
		    {
		        var lang_selected = '';
		        var lang_active = '';
		        var lang_active_id = 'passive-id';
		        var lang_in_active_default_active = 'fade';
		        
		        if (default_language == languages[language_count].LanguageID) 
		        {
		            lang_selected = 'selected';
		            lang_active = ' active show';
		            lang_active_id = 'active-id';
		            lang_in_active_default_active = 'show active default-active';
		        }

		        $('#lang').append('<option value="'+languages[language_count].LanguageID+'" '+lang_selected+'>'+languages[language_count].LanguageName+'</option>');

		        $('#language-nav').append(
		            '<li class="nav-item">' +
		                '<a class="nav-link'+lang_active+'" id="'+lang_active_id+'" data-toggle="tab" href="#tab-'+languages[language_count].LanguageCode+'">' +
	                		languages[language_count].LanguageName +
                		'</a>' +
		            '</li>'
		        );

		        $('#form-content').append(
		            '<div id="tab-'+languages[language_count].LanguageCode+'" class="container tab-pane fade '+lang_in_active_default_active+' ">' +
		                '<div class="panel-body">' +
    
	                        '<input type="hidden" value="" name="TagID['+i+']"/>' +
	                        '<input type="hidden" name="TagLangID['+i+']" value="'+languages[language_count].LanguageID+'">' +

	                        '<div class="form-group">' +
	                            '<label class="control-label">Etiket Adı</label>' +
	                            '<div class="input-area">' +
	                                '<input name="TagTitle['+i+']" class="form-control" type="text">' +
	                                '<span class="help-block"></span>' +
	                            '</div>' +
	                        '</div>' +

		                '</div>' +
		            '</div>'
		        );

		        $('#language-nav-option').append(
		            '<li class="nav-item">' +
		                '<a class="nav-link'+lang_active+'" id="'+lang_active_id+'" data-toggle="tab" href="#tab-option-'+languages[language_count].LanguageCode+'">' +
	                		languages[language_count].LanguageName +
                		'</a>' +
		            '</li>'
		        );

		        language_count++;
		    }

		    $(document).on('change','#lang',function(){
		        var lang_id = $(this).val();
		        table.ajax.url(base_url + 'product/tag/list_all/' + lang_id).load();
		    });

		    $('.input-area input, .input-area textarea, .input-area select').change(function(){
		        $(this).parent().parent().removeClass('has-error');
		        $(this).next().empty();
		    });

		});

		function add()
		{
			save_method = 'add';
			$('#tag-form')[0].reset();
		    $('.form-group').removeClass('has-error');
		    $('.help-block').empty(); 
		    $('.modal-title').text('Etiket Ekle');
		    $('#tag-modal').modal('show');
		    $('#error-message').text('');
		    $('.nav-tabs .nav-item a, .tab-pane').removeClass('active show'); 
		    $('#active-id, .default-active').addClass('active show');
		}

		function edit(content_id)
		{
			save_method = 'update';
		    $('#tag-form')[0].reset();
		    $('.form-group').removeClass('has-error');
		    $('.help-block').empty();
		    $('#error-message').text('');

		    $.ajax({
		        url : base_url + 'product/tag/view/' + content_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            for (var i = 1; i <= data.total_record; i++) 
		            {   
		                $('[name="TagID['+i+']"]').val(data[i].TagID);
		                $('[name="TagContentID"]').val(data[i].TagContentID);
		                $('[name="TagTitle['+i+']"]').val(data[i].TagTitle);
		                $('[name="TagLangID['+i+']"]').val(data[i].TagLangID);

		                $('#tag-modal').modal('show'); 
		                $('.modal-title').text('Etiketi Düzenle');
		                $('.nav-tabs .nav-item a, .tab-pane').removeClass('active show'); 
		    			$('#active-id, .default-active').addClass('active show');
		            }
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		}

		function save()
		{
			let url;
			let notify_message;
			let notify_type;
			let formData = new FormData($('#tag-form')[0]);

		    $('#btn-save').text('Kaydediyor...');
		    $('#btn-save').attr('disabled',true);

		    if(save_method == 'add') 
		    {
		        url = base_url + 'product/tag/add';
		        notify_message = 'Kayıt başarılı bir şekilde eklendi.';
		        notify_type = 'success';
		    } 
		    else
		    {
		        url = base_url + 'product/tag/update';
		        notify_message = 'Kayıt başarılı bir şekilde düzenlendi.';
		        notify_type = 'info';
		    }

		    $.ajax({
		        url : url,
		        type: 'POST',
		        data: formData,
		        contentType: false,
		        processData: false,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) 
		            {
	                    reload_table();
	                    $('#tag-modal').modal('hide');
	                    $('.nav-tabs .nav-item a, .tab-pane').removeClass('active show'); 
		    			$('#active-id, .default-active').addClass('active show');

	                    $.notify({
	                        icon: 'glyphicon glyphicon-ok',
	                        message: notify_message
	                    },{
	                        type: notify_type,
	                        offset: {
	                            x: 23,
	                            y: 53
	                        },
	                        animate: {
	                            enter: 'animated fadeInRight',
	                            exit: 'animated fadeOutRight'
	                        }
	                    });

		                $('#error-message').text('');
		            }
		            else
		            {
				        $('.input-area input, .input-area textarea, .input-area select').parent().parent().removeClass('has-error');
				        $('.input-area input, .input-area textarea, .input-area select').next().empty();

		                for (var i = 0; i < data.error.inputerror.length; i++) 
		                {
	                		$('[name="'+data.error.inputerror[i][data.error.lang[i]]+'['+data.error.lang[i]+']"]').parent().parent().addClass('has-error'); 
                    		$('[name="'+data.error.inputerror[i][data.error.lang[i]]+'['+data.error.lang[i]+']"]').next().text(data.error.error_string[i][data.error.lang[i]]); 
		                }
		                
		                $('#error-message').text('*Gerekli alanları doldurmadan işleme devam edemezsiniz.');
		                $('#tag-modal').stop().animate({
		                    scrollTop:0
		                });
		            }
		           
		            $('#btn-save').text('Kaydet'); 
		            $('#btn-save').attr('disabled',false);
		            
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error adding / update data');
		            $('#btn-save').text('Kaydet'); 
		            $('#btn-save').attr('disabled',false);
		        }
		    });
		}

		function destroy(content_id)
		{
			let notify_icon;
			let notify_type;

			Swal.fire({
				title: 'Emin misiniz?',
				text: "Sil derseniz etiket silinecektir.",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonText: 'Vazgeç',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sil'
			}).then((result) => {
				if (result.value) 
				{
					$.ajax({
				        url : base_url + 'product/tag/destroy/' + content_id,
				        type: 'DELETE',
				        success: function(data)
				        {
				        	data = JSON.parse(data);

				        	if(data.status)
				        	{
				        		notify_icon = 'glyphicon glyphicon-remove';
				        		notify_type = 'danger';

				        		reload_table();
				        	}
				        	else
				        	{
				        		notify_icon = 'glyphicon glyphicon-info-sign';
				        		notify_type = 'warning';
				        	}

				            $.notify({
				                icon: notify_icon,
				                message: data.message, 
				            },{
				                type: notify_type,
				                offset: {
				                    x: 23,
		                            y: 53
				                },
				                animate: {
				                    enter: 'animated fadeInRight',
				                    exit: 'animated fadeOutRight'
				                }
				            });
				        },
				        error: function (jqXHR, textStatus, errorThrown)
				        {
				            alert('Error deleting data');
				        }
				    });
			  	}
			});
		}

		function view(content_id)
		{
			$('#accordion').html('');

			$.ajax({
		        url : base_url + 'product/tag/view/' + content_id,
		        type: "GET",
		        dataType: "JSON",
		        success: function(data)
		        {
		        	let lang_name = '';
		        	
		            for (var i = 1; i <= data.total_record; i++) 
		            {   
		            	if (languages[i-1].LanguageID == data[i].TagLangID) 
		            	{
		            		lang_name = languages[i-1].LanguageName;
		            	}

		            	accordion_show = (i == 1 ? 'show' : '');

		                $('#accordion').append(
		                	'<div class="card">' +
						    	'<div class="card-header" id="heading'+i+'">' +
						      		'<h5 class="mb-0">' +
						        		'<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse'+i+'" aria-expanded="false" aria-controls="collapse'+i+'"><i class="la la-angle-double-down"></i>' +
						        			lang_name +
						        		'</button>' +
						      		'</h5>' +
						    	'</div>' +
						    	'<div id="collapse'+i+'" class="collapse '+accordion_show+'" aria-labelledby="heading'+i+'" data-parent="#accordion">' +
						      		'<div class="card-body">' +
						      			'<div class="view-item"><b>Etiket Adı: </b>' + data[i].TagTitle + '</div>' +
						      		'</div>'+
						    	'</div>' +
						  	'</div>'
	                	);
		            }

		            $('#view-modal').modal('show'); 
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		}

		function reload_table()
		{
		    table.ajax.reload(null,false);
		}
	</script>

	<div class="modal fade" id="tag-modal" role="dialog">
	    <div class="modal-dialog modal-lg">
	        <div class="modal-content popup-content">

	            <div class="modal-header">
			        <h5 class="modal-title">Etiket Formu</h5>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>

	            <div class="modal-body form">

	                <div id="error-message" class="error-message"></div>

	                <ul class="nav nav-tabs" role="tablist" id="language-nav">
	                    
	                </ul>

	                <form action="#" id="tag-form" class="form-horizontal">

	                    <input type="hidden" value="" name="TagContentID"/> 

	                    <div class="tab-content" id="form-content">

	                    </div>

	                </form>

	            </div>

	            <div class="modal-footer">
	                <button type="button" id="btn-save" onclick="save()" class="btn btn-primary">Kaydet</button>
	                <button type="button" class="btn btn-danger" data-dismiss="modal">İptal</button>
	            </div>

	        </div>
	    </div>
	</div>

	<div class="modal fade" id="view-modal" role="dialog">
	    <div class="modal-dialog modal-lg">
	        <div class="modal-content popup-content">

	            <div class="modal-header">
			        <h5 class="modal-title">Görüntüleme</h5>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>

	            <div class="modal-body">

	                <div id="accordion" class="view-list">

					</div>

	            </div>

	            <div class="modal-footer">
	                <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>
	            </div>

	        </div>
	    </div>
	</div>

</body>
	
</html>