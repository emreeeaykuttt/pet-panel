<div class="left-content">

	<div class="main-logo">
		<img src="<?=base_url()?>assets/frontend/img/main-logo.png">	
	</div>

	<div class="customer-logo">
		<img src="<?=base_url()?>assets/frontend/img/customer-logo.svg">	
	</div>

	<div class="menu">

		<ul>
			<li>
				<a href="<?=base_url()?>" id="menu-">
					<i class="la la-list-ul"></i>
					<span>Dashboard</span>
				</a>
			</li>
			<li>
				<a href="<?=base_url()?>slider/main" id="menu-slider-main">
					<i class="la la-list-ul"></i>
					<span>Slider</span>
				</a>
			</li>
			<li>
				<a href="<?=base_url()?>product/main" id="menu-product-main">
					<i class="la la-list-ul"></i>
					<span>Ürünler</span>
				</a>
			</li>
			<li>
				<a href="<?=base_url()?>product/category" id="menu-product-category">
					<i class="la la-list-ul"></i>
					<span>Kategoriler</span>
				</a>
			</li>
			<li>
				<a href="<?=base_url()?>product/option_group" id="menu-product-option_group">
					<i class="la la-list-ul"></i>
					<span>Opsiyon Grupları</span>
				</a>
			</li>
			<li>
				<a href="<?=base_url()?>product/tag" id="menu-product-tag">
					<i class="la la-list-ul"></i>
					<span>Etiketler</span>
				</a>
			</li>
			<li>
				<a href="<?=base_url()?>product/coupon" id="menu-product-coupon">
					<i class="la la-list-ul"></i>
					<span>Kuponlar</span>
				</a>
			</li>
			<li>
				<a href="<?=base_url()?>order/main" id="menu-order-main">
					<i class="la la-list-ul"></i>
					<span>Siparişler</span>
				</a>
			</li>
			<li>
				<a href="<?=base_url()?>user/main" id="menu-user-main">
					<i class="la la-list-ul"></i>
					<span>Kullanıcılar</span>
				</a>
			</li>
		</ul>
		
	</div>

</div>