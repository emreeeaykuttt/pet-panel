<link rel="shortcut icon" href="<?=base_url()?>assets/frontend/img/fav-icon.png">

<link href="<?=base_url()?>assets/plugins/line-awesome/css/line-awesome.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/frontend/css/animate.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/datatables/css/datatables.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/slim-select/css/slimselect.min.css" rel="stylesheet"></link>
<link href="<?=base_url()?>assets/frontend/css/style.css" rel="stylesheet">
<link href="<?=base_url()?>assets/frontend/fonts/stylesheet.css" rel="stylesheet">

<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->