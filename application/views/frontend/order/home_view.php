<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Admin Panel">
    <meta name="keywords" content="Admin Panel">

    <title>Admin Panel</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>

	<div class="main-content">
		
		<?php include(dirname(__DIR__) . '/inc/left_area.php'); ?>
	
		<div class="right-content">

			<?php include(dirname(__DIR__) . '/inc/top_area.php'); ?>

			<div class="content-title">
				SİPARİŞLER
			</div>
			
			<div class="content">
				
				<div class="table-responsive">
					<table id="table" class="display" style="width:100%">
				        <thead>
				            <tr>
				                <th class="font-weight-light">Sipariş Numarası</th>
				                <th class="font-weight-light">Ad Soyad</th>
				                <th class="font-weight-light">Tutar</th>
				                <th class="font-weight-light">Ödeme Tipi</th>
				                <th class="font-weight-light">Ödeme Durumu</th>
				                <th class="font-weight-light">Gönderim Durumu</th>
				                <th class="font-weight-light" width="213px">İşlemler</th>
				            </tr>
				        </thead>
				    </table>
			    </div>

			</div>
			
		</div>
				
	</div>
  
	<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
	<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

	<script type="text/javascript">

		var save_method;
		var table;
	    var export_name = module_export_name('siparisler');
	    var tracking_number = getUrlParameter('tracking_number');
	    var prev_shipping_status_val;
		
		$(document).ready(function(){

		    table = $('#table').DataTable({

		    	'language': {
		            'url': base_url + 'assets/plugins/datatables/language_tr.json'
		        },

		        'processing': true,
		        'serverSide': true,
		        'order': [],
		        'lengthMenu': [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'Tüm']],
		        'ajax': {
		            'url': base_url + 'order/main/list_all',
		            'type': 'POST'
		        },

		        'dom': '<"w-50 float-left"l><"w-50 float-left"fB><"table-content" tr><"w-50 float-left"i><"w-50 float-left"p>',

				'buttons': [
					{	
						extend: 'colvis', 
						text: 'Sütünlar'
					},
					{
						extend: 'csv', 
						text: 'CSV', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
					{
						extend: 'pdf', 
						text: 'PDF', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
				],

		  		'columns': [
		            { 'data': 'OrderTrackingNumber' },
		            { 'data': 'OrderDeliveryFirstName' },
		            { 'data': 'OrderLatestAmount' },
		            { 'data': 'OrderPaymentType' },
		            { 'data': 'OrderStatusText' },
		            { 'data': 'OrderShippingStatusText' },
		            { 'data': 'Transactions', 'orderable': false}
		        ]

		    });

		    if (tracking_number)
		    {
		    	setTimeout(function(){
			    	$('.dataTables_filter input').val(tracking_number);
			    	table.search(tracking_number).draw();
			    }, 500);
		    }

		});

		function print_by_id(id) 
		{
		  var divToPrint = document.getElementById(id);

		  var newWin = window.open('','Print-Window');

		  newWin.document.open();

		  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

		  newWin.document.close();

		  setTimeout(function(){newWin.close();},10);
		}
		
		function view(id)
		{
			$('#order-view').html('');

			$.ajax({
		        url : base_url + 'order/main/view/' + id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	let coupon_result = '';
		        	let coupon_amount = data.OrderCouponAmount;
		        	let coupon_rate = data.OrderCouponRate;
		        	let payment_type = data.OrderPaymentType;
		        	let company_info = '';

		        	if (coupon_amount != null)
		        	{
		        		coupon_result = '<div class="view-item"><b>Kupon İndirim Miktarı: </b>' + coupon_amount + data.OrderCurrencyCode + '</div>';
		        	}
		        	else if(coupon_rate != null)
		        	{
		        		coupon_result = '<div class="view-item"><b>Kupon İndirim Oranı: </b>' + coupon_rate + '</div>';
		        	}

		        	if (payment_type == 'card')
		        	{
		        		payment_type = 'Kredi Kartı / Banka Kartı ile ödeme';
		        	}
		        	else if (payment_type == 'transfer')
		        	{
		        		payment_type = 'EFT/havale ile ödeme';	
		        	}

		        	if (data.OrderBillingCompanyName) 
		        	{
		        		company_info = 
		        					'<div class="view-item"><b>Firma Adı: </b>' + data.OrderBillingCompanyName + '</div>' +
			      					'<div class="view-item"><b>Vergi Numarası: </b>' + data.OrderBillingCompanyTaxNumber + '</div>' +
			      					'<div class="view-item"><b>Vergi Dairesi: </b>' + data.OrderBillingCompanyTaxAdministration + '</div>';
		        	}

		        	$('#order-view').append(
	                	
	                	'<div class="container">' +
			      			'<div class="row">' +

			      				'<input type="hidden" name="OrderID" value="'+ data.OrderID +'">' +

			      				'<div class="col-md-12">' +
			      					'<table class="table mb-0">' +
			      						'<tr>' +
							         		'<td>' +
							         			'<div class="form-group">' +
				                            		'<b>Kargo Firması: </b>' +
								      				'<div class="input-area">' +
									      				'<input type="text" name="OrderCargoCompany" id="cargo-company" class="form-group" value="" />' +
					                                '</div>' +
							      				'</div>' +
							         		'</td>' +
							         		'<td>' +
							         			'<div class="form-group">' +
				                            		'<b>Kargo Numarası: </b>' +
								      				'<div class="input-area">' +
									      				'<input type="text" name="OrderCargoNumber" id="cargo-number" class="form-group" value="" />' +
					                                '</div>' +
							      				'</div>' +
							         		'</td>' +
						         		'</tr>' +
							         	'<tr>' +
							         		'<td>' +
							         			'<div class="form-group">' +
				                            		'<b>Ödeme Durumu: </b>' +
								      				'<div class="input-area">' +
									      				'<select class="form-control" name="OrderStatus" id="order-status">' +
						                                    '<option value="pendingPayment">Bekleyen Ödeme</option>' +
						                                    '<option value="processing">İşleniyor</option>' +
						                                    '<option value="onHold">Beklemede</option>' +
						                                    '<option value="completed">Tamamlandı</option>' +
						                                    '<option value="cancelled">İptal Edildi</option>' +
						                                    '<option value="refunded">Geri Ödendi</option>' +
						                                '</select>' +
					                                '</div>' +
							      				'</div>' +
						         			'</td>' +
						         			'<td>' +
						         				'<div class="form-group">' +
				                            		'<b>Gönderim Durumu: </b>' +
								      				'<div class="input-area">' +
									      				'<select class="form-control" name="OrderShippingStatus" id="shipping-status">' +
						                                    '<option value="pending">Bekliyor</option>' +
						                                    '<option value="preparing">Hazırlanıyor</option>' +
						                                    '<option value="shipped">Kargoya Verildi</option>' +
						                                    '<option value="delivered">Teslim Edildi</option>' +
						                                '</select>' +
					                                '</div>' +
							      				'</div>' +
						         			'</td>' +
							         	'</tr>' +
			      					'</table>' +
			      				'</div>' +

			      				'<div id="order-print">' +
				      				'<div class="col-md-12">' +

				      					'<table class="table mb-0">' +
								         	'<tr>' +
								         		'<td>' +
								         			'<div class="view-item"><b>Sipariş Numarası: </b>' + data.OrderTrackingNumber + '</div>' +
								         			'<div class="view-item"><b>Ödeme Yöntemi: </b>' + payment_type + '</div>' +
									      			'<div class="view-item"><b>Kdvsiz Fiyat: </b>' + data.OrderDiscountInclusiveAmountAndWithoutVat + data.OrderCurrencyCode + '</div>' +
									      			'<div class="view-item"><b>Kdv Miktarı: </b>' + data.OrderDiscountInclusiveVatAmount + data.OrderCurrencyCode + '</div>' +
									      			'<div class="view-item"><b>Kdvli Fiyat: </b>' + data.OrderDiscountInclusiveAmount + data.OrderCurrencyCode + '</div>' +
									      			coupon_result + 
									      			'<div class="view-item"><b>Kargo Fiyat: </b>' + data.OrderCargoAmount + data.OrderCurrencyCode + '</div>' +
									      			'<div class="view-item"><b>Son Fiyat: </b>' + data.OrderLatestAmount + data.OrderCurrencyCode + '</div>' +
									      			'<div class="view-item"><b>Oluşturulma Tarihi: </b>' + data.OrderCreatedAt + '</div>' +
								         		'</td>' +
								         		'<td>' +
								         			'<div class="view-item"><b>Teslimat Adresi</b></div>' +
							      					'<div class="view-item"><b>Ad Soyad: </b>' + data.OrderDeliveryFirstName + ' ' + data.OrderDeliveryLastName + '</div>' +
							      					'<div class="view-item"><b>TC Kimlik No: </b>' + data.OrderDeliveryIdentityNumber + '</div>' +
							      					'<div class="view-item"><b>Posta Kodu: </b>' + data.OrderDeliveryZipCode + '</div>' +
							      					'<div class="view-item"><b>Email: </b>' + data.OrderDeliveryEmail + '</div>' +
							      					'<div class="view-item"><b>Telefon: </b>' + data.OrderDeliveryPhone + '</div>' +
							      					'<div class="view-item"><b>Ülke: </b>' + data.OrderDeliveryCountry + '</div>' +
							      					'<div class="view-item"><b>Şehir: </b>' + data.OrderDeliveryCity + '</div>' +
							      					'<div class="view-item"><b>İlçe: </b>' + data.OrderDeliveryDistrict + '</div>' +
									      			'<div class="view-item"><b>Açık Adres: </b><br />' + data.OrderDeliveryOpenAddress + '</div>' +
								         		'</td>' +
								         		'<td>' +
								         			'<div class="view-item"><b>Fatura Adresi</b></div>' +
							      					'<div class="view-item"><b>Ad Soyad: </b>' + data.OrderBillingFirstName + ' ' + data.OrderBillingLastName + '</div>' +
							      					'<div class="view-item"><b>TC Kimlik No: </b>' + data.OrderBillingIdentityNumber + '</div>' +
							      					'<div class="view-item"><b>Posta Kodu: </b>' + data.OrderBillingZipCode + '</div>' +
							      					'<div class="view-item"><b>Email: </b>' + data.OrderBillingEmail + '</div>' +
							      					'<div class="view-item"><b>Telefon: </b>' + data.OrderBillingPhone + '</div>' +
							      					'<div class="view-item"><b>Ülke: </b>' + data.OrderBillingCountry + '</div>' +
							      					'<div class="view-item"><b>Şehir: </b>' + data.OrderBillingCity + '</div>' +
							      					'<div class="view-item"><b>İlçe: </b>' + data.OrderBillingDistrict + '</div>' +
									      			'<div class="view-item"><b>Açık Adres: </b><br />' + data.OrderBillingOpenAddress + '</div>' +
									      			company_info +
								         		'</td>' +
								         	'</tr>' +
								         	'<tr>' +
								         		'<td height="20" style="border:0;" colspan="3"></td>' +
								         	'</tr>' +
				      					'</table>' +
				      					
			      					'</div>' +
				      		
				      				'<div class="col-md-12">' +
						      			'<table class="table table-bordered table-hover" border="1">' +
							      			'<thead>' +
									            '<tr>' +
									            	'<th class="font-weight-normal">Görsel</th>' +
									                '<th class="font-weight-normal">Ürün Adı</th>' +
									                '<th class="font-weight-normal">Ürün Detayı</th>' +
									                '<th class="font-weight-normal">Kdvsiz Fiyat</th>' +
									                '<th class="font-weight-normal">Kdv Miktarı</th>' +
									                '<th class="font-weight-normal">Kdvli Fiyat</th>' +
									                '<th class="font-weight-normal">Adet</th>' +
									                '<th class="font-weight-normal">Son Fiyat</th>' +
									            '</tr>' +
									        '</thead>' +
									        '<tbody id="order-detail">' +
									         
									        '</tbody>' +
						      			'</table>' +
					      			'</div>' +
				      			'</div>' +

			      			'</div>' +
		      			'</div>'
                	);
					
					$('[name="OrderCargoNumber"]').val(data.OrderCargoNumber);
					$('[name="OrderCargoCompany"]').val(data.OrderCargoCompany);
					$('[name="OrderStatus"]').val(data.OrderStatus);
					$('[name="OrderShippingStatus"]').val(data.OrderShippingStatus);

					$('#order-status').change(function(){
						let order_status_val = $(this).val();
						let order_id = $('[name="OrderID"]').val();
						status_change(order_status_val, order_id)
					});

					$('#shipping-status').on('focus', function () {
				        prev_shipping_status_val = $(this).val();
				    }).change(function() {
				        let shipping_status_val = $(this).val();
						let order_id = $('[name="OrderID"]').val();
						let cargo_number = $('[name="OrderCargoNumber"]').val();
						let cargo_company = $('[name="OrderCargoCompany"]').val();
						shipping_status_change(shipping_status_val, prev_shipping_status_val, cargo_number, cargo_company, order_id)
				    });

					$.each(data.OrderDetails, function(i, item) {

						let detail_photo = '';

                        if (item.DetailProductPhoto)
                        {
                            detail_photo = server_url + item.DetailProductPhoto
                        }
                        else
                        {
                            detail_photo = server_url + 'upload/product/null-photo.png';
                        }

	                	$('#order-detail').append(
				            '<tr>' +
				                '<td><img src="'+ detail_photo +'" width="100" />' + '</td>' +
				                '<td>' + item.DetailProductName + '<br />' + item.DetailSKU + '</td>' +
				                '<td><div id="order-p-option'+ i +'"></div></td>' +
				                '<td>' + item.DetailDiscountInclusivePriceAndWithoutVat + item.DetailCurrencyCode + '</td>' +
				                '<td>' + item.DetailDiscountInclusiveVatPrice + item.DetailCurrencyCode + '</td>' +
				                '<td>' + item.DetailDiscountInclusivePrice + item.DetailCurrencyCode + '</td>' +
				                '<td>' + item.DetailQuantity + '</td>' +
				                '<td>' + item.DetailLatestPrice  + item.DetailCurrencyCode + '</td>' +
				            '</tr>'
	                	);

	                	$.each(JSON.parse(item.DetailProductOptionContent), function(key, val){
                            $('#order-p-option' + i).append(
                                '<div>' + val.group + ': ' + val.option + '</div>'
                            );
                        });
                	});

		            $('#view-modal').modal('show'); 
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		}

		function contract_view(id)
		{
			$('#contract-product-detail').html('');

			$.ajax({
		        url : base_url + 'order/main/view/' + id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	let coupon_result = '';
		        	let coupon_amount = data.OrderCouponAmount;
		        	let coupon_rate = data.OrderCouponRate;
		        	let payment_type = data.OrderPaymentType;

		        	if (coupon_amount != null)
		        	{
		        		coupon_result = 
		        		'<tr>' +
                            '<td colspan="3"><b>Kupon İndirim Miktarı:</b></td>' +
                            '<td colspan="1">' + coupon_amount + data.OrderCurrencyCode + '</td>' +
                        '</tr>';
		        	}
		        	else if(coupon_rate != null)
		        	{
		        		coupon_result = 
		        		'<tr>' +
                            '<td colspan="3"><b>Kupon İndirim Oranı:</b></td>' +
                            '<td colspan="1">' + coupon_rate + '</td>' +
                        '</tr>';
		        	}

		        	if (payment_type == 'card')
		        	{
		        		payment_type = 'Kredi Kartı / Banka Kartı ile ödeme';
		        	}
		        	else if (payment_type == 'transfer')
		        	{
		        		payment_type = 'EFT/havale ile ödeme';	
		        	}

		        	$('.delivery-fullname').text(data.OrderDeliveryFirstName + ' ' + data.OrderDeliveryLastName);
                    $('.delivery-identity-number').text(data.OrderDeliveryIdentityNumber);
                    $('.delivery-address').text(data.OrderDeliveryOpenAddress + ' ' + data.OrderDeliveryDistrict + '/' + data.OrderDeliveryCity);
                    $('.delivery-email').text(data.OrderDeliveryEmail);
                    $('.delivery-phone').text(data.OrderDeliveryPhone);
                    $('.billing-fullname').text(data.OrderBillingFirstName + ' ' + data.OrderBillingLastName);
                    $('.billing-identity-number').text(data.OrderBillingIdentityNumber);
                    $('.billing-address').text(data.OrderDeliveryOpenAddress + ' ' + data.OrderBillingDistrict + '/' + data.OrderBillingCity);
                    $('.billing-email').text(data.OrderBillingEmail);
                    $('.billing-phone').text(data.OrderBillingPhone);
                    $('.payment-type').text(payment_type);

                    $('#contract-product-detail').append(
                    	'<tr>' +
                            '<td><b>Ürün Açıklaması</b></td>' +
                            '<td><b>Adet</b></td>' +
                            '<td><b>Birim Fiyatı</b></td>' +
                            '<td><b>Ara Toplam (KDV Dahil)</b></td>' +
                        '</tr>'
                    );

					$.each(data.OrderDetails, function(i, item) {

	                	$('#contract-product-detail').append(
				            '<tr>' +
				                '<td>' + item.DetailProductName + '</td>' +
				                '<td>' + item.DetailQuantity + '</td>' +
				                '<td>' + item.DetailDiscountInclusivePriceAndWithoutVat + item.DetailCurrencyCode + '</td>' +
				                '<td>' + item.DetailLatestPrice  + item.DetailCurrencyCode + '</td>' +
				            '</tr>'
	                	);

                	});

                	$('#contract-product-detail').append(
                		coupon_result +
                    	'<tr>' +
                            '<td colspan="3"><b>Kargo Tutarı:</b></td>' +
                            '<td colspan="1">' + data.OrderCargoAmount + data.OrderCurrencyCode + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '<td colspan="3"><b>Toplam:</b></td>' +
                            '<td colspan="1">' + data.OrderLatestAmount + data.OrderCurrencyCode + '</td>' +
                        '</tr>'
                    );

		            $('#distance-sales-contract-modal').modal('show'); 
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		}

		function status_change(val, order_id)
		{
			let icon = '';
            let message = '';
			let formData = {OrderStatus: val};

			$.ajax({
                type: "POST",
                url: base_url + 'order/main/status_update/' + order_id,
                data: formData,
                success: function (data)
                {
                    data = JSON.parse(data)
                    message = data.message;
                    icon = data.status ? 'success' : 'warning';

                    Swal.fire({
						position: 'top-end',
						icon: icon,
						text: message,
						showConfirmButton: false,
						timer: 2000
					})
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error get data from ajax');
                }
            });
		}

		function shipping_status_change(val, prev_val, cargo_number = '', cargo_company = '', order_id)
		{
			let icon = '';
            let message = '';
			let formData = {OrderShippingStatus: val, OrderCargoNumber: cargo_number, OrderCargoCompany: cargo_company};

			if (cargo_number == '' && (val == 'shipped' || val == 'delivered')) 
			{
				Swal.fire({
					position: 'top-end',
					icon: 'warning',
					text: 'Kargo Numarası Giriniz!',
					showConfirmButton: false,
					timer: 2000
				});

				$('#shipping-status').val(prev_val);
			}
			else
			{
				$.ajax({
	                type: "POST",
	                url: base_url + 'order/main/shipping_status_update/' + order_id,
	                data: formData,
	                success: function (data)
	                {
	                    data = JSON.parse(data)
	                    message = data.message;
	                    icon = data.status ? 'success' : 'warning';

	                    Swal.fire({
							position: 'top-end',
							icon: icon,
							text: message,
							showConfirmButton: false,
							timer: 2000
						});

						reload_table();
	                },
	                error: function (jqXHR, textStatus, errorThrown)
	                {
	                    alert('Error get data from ajax');
	                }
	            });
			}
		}

		function reload_table()
		{
		    table.ajax.reload(null,false);
		}
	</script>

	<div class="modal fade" id="view-modal" role="dialog">
	    <div class="modal-dialog modal-xl">
	        <div class="modal-content popup-content">

	            <div class="modal-header">
			        <h5 class="modal-title">Görüntüleme</h5>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>

	            <div class="modal-body">

	            	<div class="container mb-2">
	            		<div class="row">
	            			<div class="col-md-12">
	            				<a href="javascript:void(0)" onclick="print_by_id('order-print')" class="btn btn-secondary w-100">YAZDIR</a>		
	            			</div>
	            		</div>
	            	</div>

	                <div id="order-view" class="view-list">

					</div>

	            </div>

	            <div class="modal-footer">
	                <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>
	            </div>

	        </div>
	    </div>
	</div>

	<!-- The UserDistanceSalesContract Modal -->
    <div class="modal" id="distance-sales-contract-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Mesafeli Satış Sözleşmesi</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" style="font-size: 13px; font-weight: 400; color: #212529; letter-spacing: 0px;">

                	<a href="javascript:void(0)" onclick="print_by_id('contract-content')" class="btn btn-secondary w-100 mb-3">YAZDIR</a>

                	<div class="contract-content" id="contract-content">

                		<h6 class="modal-title">Mesafeli Satış Sözleşmesi</h6>
                    
	                    <p><b>1-</b> Bu s&ouml;zleşme ile yazılı, g&ouml;rsel ve elektronik ortamda veya diğer iletişim ara&ccedil;ları kullanılarak ve t&uuml;keticilerle karşı karşıya gelinmeksizin yapılan, malın veye hizmetin t&uuml;keticiye anında veya sonradan teslimi veya ifası şeklinde yapılan &quot;T&uuml;ketici&quot; (Bundan sonra M&uuml;şteri olarak anılacaktır.) ve &quot;Satıcı&quot; arasındaki alışverişe ilişkin koşullar d&uuml;zenlenir. &nbsp;</p>

	                    <p><b>2-</b> <b>SATICI</b>,&nbsp; Doa Satış Pazarlama ve Ltd. Şti. &#39; dir (Bundan sonra &quot;<b>PETSHOPEVİNDE</b>&quot; olarak anılacaktır. .</p>

	                    <p>Mersis Numarası: 030251828100011</p>

	                    <p>Adres:&nbsp; Yenidoğan Mah. Halit Cansever Sok. No:32/1 İzmit Kocaeli</p>

	                    <p>E posta adresi: <a href="mailto:info@petshopevinde.com">info@petshopevinde.com</a></p>

	                    <p>Tel no: 0262 331 73 34</p>

	                    <p><b>3- ALICI</b> , <span class="delivery-fullname"></span> &#39;dir. (Bundan sonra&nbsp; &quot;<b>ALICI</b>&quot; olarak anılacaktır.)</p>

	                    <p>TC No : <span class="delivery-identity-number"></span></p>

	                    <p>Adres: <span class="delivery-address"></span></p>

	                    <p>E posta adresi : <span class="delivery-email"></span></p>

	                    <p>Teslim edilecek kişi: <span class="delivery-fullname"></span></p>

	                    <p>Teslimat Adresi: <span class="delivery-address"></span><br />
	                    Telefon: <span class="delivery-phone"></span><br />
	                    Eposta/kullanıcı adı: <span class="delivery-email"></span></p>

	                    <p><b>4- SİPARİŞ VEREN KİŞİ BİLGİLERİ</b></p>

	                    <p>Ad/Soyad/Unvan: <span class="delivery-fullname"></span></p>

	                    <p>Adres: <span class="delivery-address"></span><br />
	                    Telefon: <span class="delivery-phone"></span><br />
	                    Eposta/kullanıcı adı: <span class="delivery-email"></span></p>

	                    <p><b>5- S&Ouml;ZLEŞME KONUSU &Uuml;R&Uuml;N/&Uuml;R&Uuml;NLER BİLGİLERİ</b></p>

	                    <p><b>5.1. </b>Malın /&Uuml;r&uuml;n/&Uuml;r&uuml;nlerin/ Hizmetin temel &ouml;zelliklerini (t&uuml;r&uuml;, miktarı, marka/modeli, rengi, adedi) PETSHOPEVİNDE&rsquo;ye ait internet sitesinde yayınlanmaktadır. PETSHOPEVİNDE tarafından kampanya d&uuml;zenlenmiş ise ilgili &uuml;r&uuml;n&uuml;n temel &ouml;zelliklerini kampanya s&uuml;resince inceleyebilirsiniz. Kampanyaya dahil edilmiş &uuml;r&uuml;nler kampanya tarihi dışında kampanyayla aynı koşullarda satışa sunulamamaktadır.</p>

	                    <p><b>5.2. </b>Listelenen ve sitede ilan edilen fiyatlar satış fiyatıdır. İlan edilen fiyatlar ve vaatler g&uuml;ncelleme yapılana ve değiştirilene kadar ge&ccedil;erlidir. S&uuml;reli olarak ilan edilen fiyatlar ise belirtilen s&uuml;re sonuna kadar ge&ccedil;erlidir.</p>

	                    <p><b>5.3. </b>S&ouml;zleşme konusu mal ya da hizmetin t&uuml;m vergiler d&acirc;hil satış fiyatı aşağıda g&ouml;sterilmiştir.</p>

	                    <table class="table table-bordered" border="1" id="contract-product-detail">
	                    </table>

	                    <p>&Ouml;deme Şekli ve Planı: <span class="payment-type"></span></p>

	                    <p><b>5.4. </b>&Uuml;r&uuml;n sevkiyat masrafı olan kargo &uuml;creti ALICI tarafından &ouml;denecektir.</p>

	                    <p><b>6-&nbsp; FATURA BİLGİLERİ</b></p>

	                    <p>Ad/Soyad/Unvan: <span class="billing-fullname"></span></p>

	                    <p>Adres: <span class="billing-address"></span><br />
	                    Telefon: <span class="billing-phone"></span><br />
	                    Eposta/kullanıcı adı: <span class="billing-email"></span><br />
	                    Fatura teslim: Fatura sipariş teslimatı sırasında fatura adresine sipariş ile birlikte<br />
	                    teslim edilecektir.</p>

	                    <p><b>7- GENEL H&Uuml;K&Uuml;MLER</b></p>

	                    <p><b>7.1.</b>ALICI, PETSHOPEVİNDE&lsquo;ye ait internet sitesinde s&ouml;zleşme konusu &uuml;r&uuml;n&uuml;n temel nitelikleri, satış fiyatı ve &ouml;deme şekli ile teslimata ilişkin &ouml;n bilgileri okuyup, bilgi sahibi olduğunu, elektronik ortamda gerekli teyidi verdiğini kabul, beyan ve taahh&uuml;t eder. ALICI&rsquo;nın; &Ouml;n Bilgilendirmeyi elektronik ortamda teyit etmesi, mesafeli satış s&ouml;zleşmesinin kurulmasından evvel, PETSHOPEVİNDE tarafından ALICI&#39; ya verilmesi gereken adresi, siparişi verilen &uuml;r&uuml;nlere ait temel &ouml;zellikleri, &uuml;r&uuml;nlerin vergiler dahil fiyatını, &ouml;deme ve teslimat bilgilerini de doğru ve eksiksiz olarak edindiğini kabul, beyan ve taahh&uuml;t eder.</p>

	                    <p><b>7.2.</b>S&ouml;zleşme konusu her bir &uuml;r&uuml;n, 30 g&uuml;nl&uuml;k yasal s&uuml;reyi aşmamak kaydı ile ALICI&#39; nın yerleşim yeri uzaklığına bağlı olarak internet sitesindeki &ouml;n bilgiler kısmında belirtilen s&uuml;re zarfında ALICI veya ALICI&rsquo;nın g&ouml;sterdiği adresteki kişi ve/veya kuruluşa teslim edilir. Bu s&uuml;re i&ccedil;inde &uuml;r&uuml;n&uuml;n ALICI&rsquo;ya teslim edilememesi durumunda, ALICI&rsquo;nın s&ouml;zleşmeyi feshetme hakkı saklıdır.</p>

	                    <p><b>7.3.</b>PETSHOPEVİNDE, S&ouml;zleşme konusu &uuml;r&uuml;n&uuml; eksiksiz, siparişte belirtilen niteliklere uygun ve varsa garanti belgeleri, kullanım kılavuzları işin gereği olan bilgi ve belgeler ile teslim etmeyi, her t&uuml;rl&uuml; ayıptan ari olarak yasal mevzuat gereklerine g&ouml;re sağlam, standartlara uygun bir şekilde işi doğruluk ve d&uuml;r&uuml;stl&uuml;k esasları dahilinde ifa etmeyi, hizmet kalitesini koruyup y&uuml;kseltmeyi, işin ifası sırasında gerekli dikkat ve &ouml;zeni g&ouml;stermeyi, ihtiyat ve &ouml;ng&ouml;r&uuml; ile hareket etmeyi kabul, beyan ve taahh&uuml;t eder.</p>

	                    <p><b>7.4.</b>PETSHOPEVİNDE, s&ouml;zleşmeden doğan ifa y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml;n&uuml;n s&uuml;resi dolmadan ALICI&rsquo;yı bilgilendirmek ve a&ccedil;ık&ccedil;a onayını almak suretiyle eşit kalite ve fiyatta farklı bir &uuml;r&uuml;n tedarik edebilir.</p>

	                    <p><b>7.5.</b>PETSHOPEVİNDE, sipariş konusu &uuml;r&uuml;n veya hizmetin yerine getirilmesinin imkansızlaşması halinde s&ouml;zleşme konusu y&uuml;k&uuml;ml&uuml;l&uuml;klerini yerine getiremezse, bu durumu, &ouml;ğrendiği tarihten itibaren 3 g&uuml;n i&ccedil;inde yazılı olarak t&uuml;keticiye bildireceğini, 14 g&uuml;nl&uuml;k s&uuml;re i&ccedil;inde toplam bedeli ALICI&rsquo;ya iade edeceğini kabul, beyan ve taahh&uuml;t eder.</p>

	                    <p><b>7.6.</b>ALICI, S&ouml;zleşme konusu &uuml;r&uuml;n&uuml;n teslimatı i&ccedil;in işbu S&ouml;zleşme&rsquo;yi elektronik ortamda teyit edeceğini, herhangi bir nedenle s&ouml;zleşme konusu &uuml;r&uuml;n bedelinin &ouml;denmemesi ve/veya banka kayıtlarında iptal edilmesi halinde, PETSHOPEVİNDE&lsquo; s&ouml;zleşme konusu &uuml;r&uuml;n&uuml; teslim y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml;n&uuml;n sona ereceğini kabul, beyan ve taahh&uuml;t eder.</p>

	                    <p><b>7.7.</b>SATICI, tarafların iradesi dışında gelişen, &ouml;nceden &ouml;ng&ouml;r&uuml;lemeyen ve tarafların bor&ccedil;larını yerine getirmesini engelleyici ve/veya geciktirici hallerin oluşması gibi m&uuml;cbir sebepler nedeni ile s&ouml;zleşme konusu &uuml;r&uuml;n&uuml; s&uuml;resi i&ccedil;inde teslim edemez ise, durumu ALICI&#39;ya bildireceğini kabul, beyan ve taahh&uuml;t eder. ALICI da siparişin iptal edilmesini, s&ouml;zleşme konusu &uuml;r&uuml;n&uuml;n varsa emsali ile değiştirilmesini ve/veya teslimat s&uuml;resinin engelleyici durumun ortadan kalkmasına kadar ertelenmesini SATICI&rsquo;dan talep etme hakkını haizdir. ALICI tarafından siparişin iptal edilmesi halinde ALICI&rsquo;nın nakit ile yaptığı &ouml;demelerde, &uuml;r&uuml;n tutarı 14 g&uuml;n i&ccedil;inde kendisine nakden ve defaten &ouml;denir. ALICI&rsquo;nın kredi kartı ile yaptığı &ouml;demelerde ise, &uuml;r&uuml;n tutarı, siparişin ALICI tarafından iptal edilmesinden sonra 14 g&uuml;n i&ccedil;erisinde ilgili bankaya iade edilir. ALICI, SATICI tarafından kredi kartına iade edilen tutarın banka tarafından ALICI hesabına yansıtılmasına ilişkin ortalama s&uuml;recin 2 ile 3 haftayı bulabileceğini, bu tutarın bankaya iadesinden sonra ALICI&rsquo;nın hesaplarına yansıması halinin tamamen banka işlem s&uuml;reci ile ilgili olduğundan, ALICI, olası gecikmeler i&ccedil;in SATICI&rsquo;yı sorumlu tutamayacağını kabul, beyan ve taahh&uuml;t eder.</p>

	                    <p><b>7.8.</b>SATICININ, ALICI tarafından siteye kayıt (&uuml;yelik) formunda belirtilen veya daha sonra kendisi tarafından g&uuml;ncellenen adresi, e-posta adresi, sabit ve mobil telefon hatları ve diğer iletişim bilgileri &uuml;zerinden mektup, e-posta, SMS, telefon g&ouml;r&uuml;şmesi ve diğer yollarla iletişim, pazarlama, bildirim ve diğer ama&ccedil;larla ALICI&rsquo;ya ulaşma hakkı bulunmaktadır. ALICI, işbu s&ouml;zleşmeyi kabul etmekle SATICI&rsquo;nın kendisine y&ouml;nelik yukarıda belirtilen iletişim faaliyetlerinde bulunabileceğini kabul ve beyan etmektedir.</p>

	                    <p><b>7.9.</b>ALICI, s&ouml;zleşme konusu mal/hizmeti teslim almadan &ouml;nce muayene edecek; ezik, kırık, ambalajı yırtılmış vb. hasarlı ve ayıplı mal/hizmeti kargo şirketinden teslim almayacaktır. ALICI tarafından teslim alınan mal/hizmetin hasarsız ve sağlam olduğu kabul edilecektir. Teslimden sonra mal/hizmetin &ouml;zenle korunması borcu, ALICI&rsquo;ya aittir. Cayma hakkı kullanılacaksa mal/hizmet kullanılmamalıdır. Fatura iade edilmelidir.</p>

	                    <p><b>7.10.</b>ALICI ile sipariş esnasında kullanılan kredi kartı hamilinin aynı kişi olmaması veya &uuml;r&uuml;n&uuml;n ALICI&rsquo;ya tesliminden evvel, siparişte kullanılan kredi kartına ilişkin g&uuml;venlik a&ccedil;ığı tespit edilmesi halinde, SATICI, kredi kartı hamiline ilişkin kimlik ve iletişim bilgilerini, siparişte kullanılan kredi kartının bir &ouml;nceki aya ait ekstresini yahut kart hamilinin bankasından kredi kartının kendisine ait olduğuna ilişkin yazıyı ibraz etmesini ALICI&rsquo;dan talep edebilir. ALICI&rsquo;nın talebe konu bilgi/belgeleri temin etmesine kadar ge&ccedil;ecek s&uuml;rede sipariş dondurulacak olup, mezkur taleplerin 24 saat i&ccedil;erisinde karşılanmaması halinde ise SATICI, siparişi iptal etme hakkını haizdir.</p>

	                    <p><b>7.11.</b>ALICI, SATICI&rsquo;ya ait internet sitesine &uuml;ye olurken verdiği kişisel ve diğer sair bilgilerin ger&ccedil;eğe uygun olduğunu, SATICI&rsquo;nın bu bilgilerin ger&ccedil;eğe aykırılığı nedeniyle uğrayacağı t&uuml;m zararları, SATICI&rsquo;nın ilk bildirimi &uuml;zerine derhal, nakden ve defaten tazmin edeceğini beyan ve taahh&uuml;t eder.</p>

	                    <p><b>7.12.</b>ALICI, SATICI&rsquo;ya ait internet sitesini kullanırken yasal mevzuat h&uuml;k&uuml;mlerine riayet etmeyi ve bunları ihlal etmemeyi baştan kabul ve taahh&uuml;t eder. Aksi takdirde, doğacak t&uuml;m hukuki ve cezai y&uuml;k&uuml;ml&uuml;l&uuml;kler tamamen ve m&uuml;nhasıran ALICI&rsquo;yı bağlayacaktır.</p>

	                    <p><b>7.13.</b>ALICI, SATICI&rsquo;ya ait internet sitesini hi&ccedil;bir şekilde kamu d&uuml;zenini bozucu, genel ahlaka aykırı, başkalarını rahatsız ve taciz edici şekilde, yasalara aykırı bir ama&ccedil; i&ccedil;in, başkalarının maddi ve manevi haklarına tecav&uuml;z edecek şekilde kullanamaz. Ayrıca, &uuml;ye başkalarının hizmetleri kullanmasını &ouml;nleyici veya zorlaştırıcı faaliyet (spam, virus, truva atı, vb.) işlemlerde bulunamaz.</p>

	                    <p><b>7.14.</b>SATICI&rsquo;ya ait internet sitesinin &uuml;zerinden, SATICI&rsquo;nın kendi kontrol&uuml;nde olmayan ve/veya başkaca &uuml;&ccedil;&uuml;nc&uuml; kişilerin sahip olduğu ve/veya işlettiği başka web sitelerine ve/veya başka i&ccedil;eriklere link verilmiş olabilir. Bu linkler ALICI&rsquo;ya y&ouml;nlenme kolaylığı sağlamak amacıyla konmuş olup herhangi bir web sitesini veya o siteyi işleten kişiyi desteklememekte ve Link verilen web sitesinin i&ccedil;erdiği bilgilere y&ouml;nelik herhangi bir garanti niteliği taşımamaktadır.</p>

	                    <p><b>7.15.</b>İşbu s&ouml;zleşme i&ccedil;erisinde sayılan maddelerden bir ya da birka&ccedil;ını ihlal eden &uuml;ye işbu ihlal nedeniyle cezai ve hukuki olarak şahsen sorumlu olup, SATICI&rsquo;yı bu ihlallerin hukuki ve cezai sonu&ccedil;larından ari tutacaktır. Ayrıca; işbu ihlal nedeniyle, olayın hukuk alanına intikal ettirilmesi halinde, SATICI&rsquo;nın &uuml;yeye karşı &uuml;yelik s&ouml;zleşmesine uyulmamasından dolayı tazminat talebinde bulunma hakkı saklıdır.</p>

	                    <p><b>8. CAYMA HAKKI</b></p>

	                    <p><b>8.1.</b>ALICI; mesafeli s&ouml;zleşmenin mal satışına ilişkin olması durumunda, &uuml;r&uuml;n&uuml;n kendisine veya g&ouml;sterdiği adresteki kişi/kuruluşa teslim tarihinden itibaren 14 (ond&ouml;rt) g&uuml;n i&ccedil;erisinde, SATICI&rsquo;ya bildirmek şartıyla hi&ccedil;bir hukuki ve cezai sorumluluk &uuml;stlenmeksizin ve hi&ccedil;bir gerek&ccedil;e g&ouml;stermeksizin malı reddederek s&ouml;zleşmeden cayma hakkını kullanabilir. Hizmet sunumuna ilişkin mesafeli s&ouml;zleşmelerde ise, bu s&uuml;re s&ouml;zleşmenin imzalandığı tarihten itibaren başlar. Cayma hakkı s&uuml;resi sona ermeden &ouml;nce, t&uuml;keticinin onayı ile hizmetin ifasına başlanan hizmet s&ouml;zleşmelerinde cayma hakkı kullanılamaz. Cayma hakkının kullanımından kaynaklanan masraflar SATICI&rsquo; ya aittir. ALICI, iş bu s&ouml;zleşmeyi kabul etmekle, cayma hakkı konusunda bilgilendirildiğini kabul eder.</p>

	                    <p><b>8.2.</b>Cayma hakkının kullanılması i&ccedil;in 14 (ond&ouml;rt) g&uuml;nl&uuml;k s&uuml;re i&ccedil;inde SATICI&#39; ya iadeli taahh&uuml;tl&uuml; posta, faks veya eposta ile yazılı bildirimde bulunulması ve &uuml;r&uuml;n&uuml;n işbu s&ouml;zleşmede d&uuml;zenlenen &quot;Cayma Hakkı Kullanılamayacak &Uuml;r&uuml;nler&quot; h&uuml;k&uuml;mleri &ccedil;er&ccedil;evesinde kullanılmamış olması şarttır. Bu hakkın kullanılması halinde,</p>

	                    <p><b>a)</b>3. kişiye veya ALICI&rsquo; ya teslim edilen &uuml;r&uuml;n&uuml;n faturası, (İade edilmek istenen &uuml;r&uuml;n&uuml;n faturası kurumsal ise, iade ederken kurumun d&uuml;zenlemiş olduğu iade faturası ile birlikte g&ouml;nderilmesi gerekmektedir. Faturası kurumlar adına d&uuml;zenlenen sipariş iadeleri İADE FATURASI kesilmediği takdirde tamamlanamayacaktır.)</p>

	                    <p><b>b)</b>İade formu,</p>

	                    <p><b>c)</b>İade edilecek &uuml;r&uuml;nlerin kutusu, ambalajı, varsa standart aksesuarları ile birlikte eksiksiz ve hasarsız olarak teslim edilmesi gerekmektedir.</p>

	                    <p><b>d)</b>SATICI, cayma bildiriminin kendisine ulaşmasından itibaren en ge&ccedil; 10 g&uuml;nl&uuml;k s&uuml;re i&ccedil;erisinde toplam bedeli ve ALICI&rsquo;yı bor&ccedil; altına sokan belgeleri ALICI&rsquo; ya iade etmekle ve ALICI 20 g&uuml;nl&uuml;k s&uuml;re i&ccedil;erisinde malı satıcıya teslim etmekle y&uuml;k&uuml;ml&uuml;d&uuml;r.</p>

	                    <p><b>e)</b>ALICI&rsquo; nın kusurundan kaynaklanan bir nedenle malın değerinde bir azalma olursa veya iade imkansızlaşırsa ALICI kusuru oranında SATICI&rsquo; nın zararlarını tazmin etmekle y&uuml;k&uuml;ml&uuml;d&uuml;r. Ancak cayma hakkı s&uuml;resi i&ccedil;inde malın veya &uuml;r&uuml;n&uuml;n usul&uuml;ne uygun kullanılması sebebiyle meydana gelen değişiklik ve bozulmalardan ALICI sorumlu değildir.</p>

	                    <p><b>f)</b>Cayma hakkının kullanılması nedeniyle SATICI tarafından d&uuml;zenlenen kampanya limit tutarının altına d&uuml;ş&uuml;lmesi halinde kampanya kapsamında faydalanılan indirim miktarı iptal edilir.</p>

	                    <p><b>9. CAYMA HAKKI KULLANILAMAYACAK &Uuml;R&Uuml;NLER</b></p>

	                    <p>ALICI&rsquo;nın isteği veya a&ccedil;ık&ccedil;a kişisel ihtiya&ccedil;ları doğrultusunda hazırlanan ve geri g&ouml;nderilmeye m&uuml;sait olmayan &uuml;r&uuml;n ve/veya malzemeleri, tek kullanımlık &uuml;r&uuml;nler, &ccedil;abuk bozulma tehlikesi olan veya son kullanma tarihi ge&ccedil;me ihtimali olan mallar, ALICI&rsquo;ya teslim edilmesinin ardından ALICI tarafından ambalajı a&ccedil;ıldığı takdirde iade edilmesi sağlık ve hijyen a&ccedil;ısından uygun olmayan &uuml;r&uuml;nler, teslim edildikten sonra başka &uuml;r&uuml;nlerle karışan ve doğası gereği ayrıştırılması m&uuml;mk&uuml;n olmayan &uuml;r&uuml;nler, elektronik ortamda anında ifa edilen hizmetler veya t&uuml;keticiye anında teslim edilen gayrimaddi mallar ile ses veya g&ouml;r&uuml;nt&uuml; kayıtlarının, kitap, dijital i&ccedil;erik, yazılım programlarının, veri kaydedebilme ve veri depolama cihazlarının ambalajının ALICI tarafından a&ccedil;ılmış olması halinde iadesi Y&ouml;netmelik gereği m&uuml;mk&uuml;n değildir.</p>

	                    <p>a) Hijyenik &uuml;r&uuml;nlerin hi&ccedil;bir sebeple iadesi yoktur.(ambalajı a&ccedil;ılmış kedi yada k&ouml;peğinizin bedenine temas etmiş olması)<br />
	                    b) Kedi tuvalet &uuml;r&uuml;nleri.<br />
	                    c) Tırnak makası, fır&ccedil;a, tarak gibi kolay kontaminasyona uygun &uuml;r&uuml;nler( Ambalajı a&ccedil;ılmasa dahi!)<br />
	                    d) Tek kullanımlık &uuml;r&uuml;nler.<br />
	                    e) G&ouml;z, kulak damlası gibi ağızı a&ccedil;ıldıktan sonra kullanım s&uuml;resi olan &uuml;r&uuml;nler.<br />
	                    f) K&uuml;lotlar ve pedler.</p>

	                    <p>Kozmetik ve pet bakım &uuml;r&uuml;nleri, giyim &uuml;r&uuml;nleri, kitap, kopyalanabilir yazılım ve programlar, DVD, VCD, CD ve kasetlerin iade edilebilmesi i&ccedil;in ambalajlarının a&ccedil;ılmamış, denenmemiş, bozulmamış ve kullanılmamış olmaları gerekir.</p>

	                    <p><b>10. TEMERR&Uuml;T HALİ VE HUKUKİ SONU&Ccedil;LARI</b></p>

	                    <p>ALICI, &ouml;deme işlemlerini kredi kartı ile yaptığı durumda temerr&uuml;de d&uuml;şt&uuml;ğ&uuml; takdirde, kart sahibi banka ile arasındaki kredi kartı s&ouml;zleşmesi &ccedil;er&ccedil;evesinde faiz &ouml;deyeceğini ve bankaya karşı sorumlu olacağını kabul, beyan ve taahh&uuml;t eder. Bu durumda ilgili banka hukuki yollara başvurabilir; doğacak masrafları ve vekalet &uuml;cretini ALICI&rsquo;dan talep edebilir ve her koşulda ALICI&rsquo;nın borcundan dolayı temerr&uuml;de d&uuml;şmesi halinde, ALICI, borcun gecikmeli ifasından dolayı SATICI&rsquo;nın uğradığı zarar ve ziyanını &ouml;deyeceğini kabul, beyan ve taahh&uuml;t eder.</p>

	                    <p><b>11. YETKİLİ MAHKEME</b></p>

	                    <p><b>İşbu s&ouml;zleşmeden doğan uyuşmazlıklarda şikayet ve itirazlar </b>ilgili yasalarda belirtilen parasal sınırlar dahilinde t&uuml;ketici sorunları hakem heyetine veya t&uuml;ketici mahkemesine yapılabilir.<br />
	                    İşbu S&ouml;zleşme ticari ama&ccedil;larla yapılmaktadır. İşbu s&ouml;zleşme &ccedil;er&ccedil;evesinde meydana gelebilecek uyuşmazlıklarda kanunlarda &ouml;zel bir d&uuml;zenleme yoksa Kocaeli Mahkemeleri ve İcra Daireleri yetkilidir.</p>

	                    <p><b>12. Y&Uuml;R&Uuml;RL&Uuml;K</b></p>

	                    <p>ALICI, Site &uuml;zerinden verdiği siparişe ait &ouml;demeyi ger&ccedil;ekleştirdiğinde işbu s&ouml;zleşmenin t&uuml;m şartlarını kabul etmiş sayılır.&nbsp;</p>

                    </div>

                </div>

            </div>
        </div>
    </div>

</body>
	
</html>