<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata('AdminToken')) 
		{
			$this->session->set_userdata('which_page', (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			redirect(base_url() . 'auth/login');
			exit();
		}
		
		$this->autorun();
	}

	public function index()
	{
		$this->load->model('user/auth_model', 'auth');
		$data = $this->auth->getTotalRecords();

		$this->load->view('frontend/home_view', $data);
	}

}


?>