<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata('AdminToken')) 
		{
			$this->session->set_userdata('which_page', (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			redirect(base_url() . 'auth/login');
			exit();
		}
		
		$this->autorun();
		$this->load->model('user/user_model', 'user');
	}

	public function index()
	{
		$this->load->view('frontend/user/home_view');
	}
	
	public function list_all()
	{
		$post = $this->input->post(null, true);
		$post['search']['value'] = str_replace(' ', '%20', $post['search']['value']);
		$this->user->post = json_encode($post);

		$list = $this->user->getDatatables();

		$data = array();
		
		if (!empty($list))
		{
			foreach ($list as $user) 
			{
				$row = array();
				$row['UserFirstName'][] = $user['UserFirstName'] . ' ' . $user['UserLastName'];
				$row['UserEmail'][] = $user['UserEmail'];
				$row['UserPhone'][] = $user['UserPhone'];
				$row['UserGender'][] = $user['UserGender'];
				$row['Transactions'][] = '
					<a class="btn-green" href="javascript:void(0)" onclick="view('.$user['UserID'].')">
						<i class="la la-eye"></i> Görüntüle
					</a>
				';
				
				$data[] = $row;
			}
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->user->getCountAll(),
			"recordsFiltered" => $this->user->getCountFiltered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}

	public function view($id)
	{	
		$this->load->model('user/address_model', 'address');

		$data = $this->user->getByID($id);
		$data['Addresses'] = $this->address->getAllByUserID($id);
		
		echo json_encode($data);
	}

	public function search($name_and_surname)
	{	
		$data = $this->user->getAllByNameAndSurname($name_and_surname);
		
		echo json_encode($data);
	}

}

?>