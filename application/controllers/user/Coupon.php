<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coupon extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata('AdminToken')) 
		{
			$this->session->set_userdata('which_page', (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			redirect(base_url() . 'auth/login');
			exit();
		}
		
		$this->autorun();
		$this->load->model('user/coupon_model', 'user_coupon');
	}

	public function add()
	{
		$post = $this->input->post(null,true);

		$encode_data = array(
			'data' => json_encode($post),
		);

		$result = $this->user_coupon->save($encode_data, $post['UserCouponCouponContentID']);
		
		echo json_encode($result);
	}

	public function destroy($id, $coupon_content_id)
	{
		$result = $this->user_coupon->delete($id, $coupon_content_id);

		echo json_encode($result);
	}

	public function list_by_coupon_content_id($coupon_content_id)
	{	
		$data['users'] = $this->user_coupon->getAllByCouponContentID($coupon_content_id);
		
		echo json_encode($data);
	}

}

?>