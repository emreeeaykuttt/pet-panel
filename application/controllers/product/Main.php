<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata('AdminToken')) 
		{
			$this->session->set_userdata('which_page', (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			redirect(base_url() . 'auth/login');
			exit();
		}

		$this->autorun();
		$this->load->model('product/product_model', 'product');
		$this->load->model('language/language_model', 'language');
	}

	public function index()
	{
		$this->load->view('frontend/product/home_view');
	}
	
	public function list_all($lang_id = '')
	{
		if ($lang_id == '') 
		{
			$lang = $this->language->getByDefault();
			$lang_id = $lang['LanguageID'];
		}

		$post = $this->input->post(null, true);
		$post['search']['value'] = str_replace(' ', '%20', $post['search']['value']);
		$this->product->post = json_encode($post);

		$list = $this->product->getDatatables($lang_id);
		$data = array();
		
		if (!empty($list))
		{
			foreach ($list as $product) 
			{
				$cleared_name = str_replace('\'', '', $product['ProductName']);
				$group = json_decode($product['ProductOptionGroups'], true);
				$group_total = 0;
				if ($group) 
				{
					$group_total = count($group);
					$group_detail = implode('-', $group);
				}
				else
				{
					$group_detail = '';
				}
				
				$row = array();
				$row['ProductName'][] = $product['ProductName'];
				$row['ProductCode'][] = $product['ProductCode'];
				$row['CategoryName'][] = $product['CategoryName'];
				$row['Transactions'][] = '
					<a class="btn-green" href="javascript:void(0)" onclick="view('. $product['ProductContentID'] .')">
						<i class="la la-eye"></i> Görüntüle
					</a>
					<a class="btn-yellow" href="javascript:void(0)" onclick="edit('. $product['ProductContentID'] .')">
						<i class="la la-pen-nib"></i> Düzenle
					</a>
					<a class="btn-red" href="javascript:void(0)" onclick="destroy('. $product['ProductContentID'] .')">
						<i class="la la-trash-alt"></i> Sil
					</a>
					<a class="btn-blue" href="javascript:void(0)" onclick="option_add('. $product['ProductID'] . ', '. $product['ProductContentID'] .', \''. $cleared_name .'\', '. $group_total . ', \'' . $group_detail .'\')">
						<i class="la la-code-branch"></i> Opsiyonlar <span>('. $product['ProductOptionCount'] .')</span>
					</a>
				';
				
				$data[] = $row;
			}
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->product->getCountAll($lang_id),
			"recordsFiltered" => $this->product->getCountFiltered($lang_id),
			"data" => $data,
		);
		
		echo json_encode($output);
	}

	public function view($content_id)
	{	
		$this->load->model('product/category_model', 'category');

		$results = $this->product->getByContentID($content_id);
		$i = 0;

		foreach ($results as $key => $value) 
		{
			$i++;
			$data[$i] = $value;
			$this->category->lang_id = $value['ProductLangID'];
			$category = $this->category->getByContentID($value['ProductCategoryContentID']);
			$data[$i]['ProductMainCategoryID'] = $category['CategoryParentID'];
			$data[$i]['ProductCategoryName'] = $category['CategoryName'];
		}

		$data['total_record'] = $i;
		
		echo json_encode($data);
	}

	public function view_all($content_id)
	{	
		$this->load->model('product/category_model', 'category');
		$this->load->model('product/product_option_model', 'product_option');
		$this->load->model('product/option_group_model', 'option_group');
		$this->load->model('product/option_model', 'option');

		$results = $this->product->getByContentID($content_id);
		$i = 0;

		foreach ($results as $key => $value) 
		{
			$i++;
			$data[$i] = $value;
			$this->category->lang_id = $value['ProductLangID'];
			$category = $this->category->getByContentID($value['ProductCategoryContentID']);
			$data[$i]['ProductMainCategoryID'] = $category['CategoryParentID'];
			$data[$i]['ProductCategoryName'] = $category['CategoryName'];
		}

		$data['total_record'] = $i;
		
		echo json_encode($data);
	}

	public function add()
	{
		$this->load->model('language/Language_model', 'language');

		$post = $this->input->post();
		$languages = $this->language->getCount();
		$files = array();

		for ($i=1; $i <= $languages; $i++)
		{ 
			$files[$i]['image'] = '';
			$files[$i]['name'] = '';

			if (!empty($_FILES['ProductPhoto']['tmp_name'][$i])) 
			{
				$check = getimagesize($_FILES['ProductPhoto']['tmp_name'][$i]);
				if($check !== false) 
				{
					$data = base64_encode(file_get_contents($_FILES['ProductPhoto']['tmp_name'][$i]));
					$files[$i]['image'] = 'data:' . $check['mime'] . ';base64,' . $data;
					$files[$i]['name'] = $_FILES['ProductPhoto']['name'][$i];
				}
			}
		}

		$encode_data = array(
			'data' => json_encode($post),
			'files' => json_encode($files),
		);

		$result = $this->product->save($encode_data);
		
		echo json_encode($result);
	}

	public function update()
	{
		$this->load->model('language/Language_model', 'language');
		
		$post = $this->input->post();
		$languages = $this->language->getCount();
		$files = array();

		for ($i=1; $i <= $languages; $i++)
		{ 
			$files[$i]['image'] = '';
			$files[$i]['name'] = '';

			if (!empty($_FILES['ProductPhoto']['tmp_name'][$i])) 
			{
				$check = getimagesize($_FILES['ProductPhoto']['tmp_name'][$i]);
				if($check !== false) 
				{
					$data = base64_encode(file_get_contents($_FILES['ProductPhoto']['tmp_name'][$i]));
					$files[$i]['image'] = 'data:' . $check['mime'] . ';base64,' . $data;
					$files[$i]['name'] = $_FILES['ProductPhoto']['name'][$i];
				}
			}
		}

		$encode_data = array(
			'data' => json_encode($post),
			'files' => json_encode($files),
		);

		$result = $this->product->update($encode_data);
		
		echo json_encode($result);
	}

	public function destroy($content_id)
	{
		$result = $this->product->delete($content_id);

		echo json_encode($result);
	}

}


?>