<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Option extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata('AdminToken')) 
		{
			$this->session->set_userdata('which_page', (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			redirect(base_url() . 'auth/login');
			exit();
		}
		
		$this->autorun();
		$this->load->model('product/option_model', 'option');
	}

	public function view($content_id)
	{	
		$results = $this->option->getByContentID($content_id);
		$i = 0;

		foreach ($results as $key => $value) 
		{
			$i++;
			$data[$i] = $value;
		}

		$data['total_record'] = $i;
		
		echo json_encode($data);
	}

	public function add()
	{
		$post = $this->input->post(null,true);

		$encode_data = array(
			'data' => json_encode($post),
		);

		$result = $this->option->save($encode_data);
		
		echo json_encode($result);
	}

	public function update()
	{
		$post = $this->input->post(null,true);

		$encode_data = array(
			'data' => json_encode($post),
		);
		
		$result = $this->option->update($encode_data);
		
		echo json_encode($result);
	}

	public function destroy($content_id)
	{
		$result = $this->option->delete($content_id);

		echo json_encode($result);
	}

	public function list_by_group_content_id($group_content_id)
	{	
		$data['options'] = $this->option->getAllByGroupContentID($group_content_id);
		
		echo json_encode($data);
	}

}

?>