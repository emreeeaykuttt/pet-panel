<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Option_group extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata('AdminToken')) 
		{
			$this->session->set_userdata('which_page', (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			redirect(base_url() . 'auth/login');
			exit();
		}
		
		$this->autorun();
		$this->load->model('product/option_group_model', 'option_group');
		$this->load->model('product/option_model', 'option');
		$this->load->model('language/language_model', 'language');
	}

	public function index()
	{
		$this->load->view('frontend/product/option_group_view');
	}

	public function list_all($lang_id = '')
	{
		if ($lang_id == '') 
		{
			$lang = $this->language->getByDefault();
			$lang_id = $lang['LanguageID'];
		}

		$post = $this->input->post(null, true);
		$post['search']['value'] = str_replace(' ', '%20', $post['search']['value']);
		$this->option_group->post = json_encode($post);

		$list = $this->option_group->getDatatables($lang_id);
		$data = array();
		
		if (!empty($list))
		{
			foreach ($list as $option_group) 
			{
				$row = array();
				$row['GroupName'][] = $option_group['GroupName'];
				$row['GroupSort'][] = $option_group['GroupSort'];
				$row['Transactions'][] = '
					<a class="btn-green" href="javascript:void(0)" onclick="view('.$option_group['GroupContentID'].')">
						<i class="la la-eye"></i> Görüntüle
					</a>
					<a class="btn-yellow" href="javascript:void(0)" onclick="edit('.$option_group['GroupContentID'].')">
						<i class="la la-pen-nib"></i> Düzenle
					</a>
					<a class="btn-red" href="javascript:void(0)" onclick="destroy('.$option_group['GroupContentID'].')">
						<i class="la la-trash-alt"></i> Sil
					</a>
					<a class="btn-blue" href="javascript:void(0)" onclick="option_add('.$option_group['GroupID'].', '.$option_group['GroupContentID'].', \''.$option_group['GroupName'].'\')">
						<i class="la la-code-branch"></i> Opsiyonlar <span>('. $option_group['OptionCount'] .')</span>
					</a>
				';
				
				$data[] = $row;
			}
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->option_group->getCountAll($lang_id),
			"recordsFiltered" => $this->option_group->getCountFiltered($lang_id),
			"data" => $data,
		);
		
		echo json_encode($output);
	}

	public function view($content_id)
	{	
		$results = $this->option_group->getByContentID($content_id);
		$i = 0;

		foreach ($results as $key => $value) 
		{
			$i++;
			$data[$i] = $value;

			$this->option->lang_id = $value['GroupLangID'];
			$options = $this->option->getAllByGroupContentID($value['GroupContentID']);
			$data[$i]['Options'] = $options;
		}

		$data['total_record'] = $i;
		
		echo json_encode($data);
	}

	public function add()
	{
		$post = $this->input->post(null,true);

		$encode_data = array(
			'data' => json_encode($post),
		);

		$result = $this->option_group->save($encode_data);
		
		echo json_encode($result);
	}

	public function update()
	{
		$post = $this->input->post(null,true);

		$encode_data = array(
			'data' => json_encode($post),
		);
		
		$result = $this->option_group->update($encode_data);
		
		echo json_encode($result);
	}

	public function destroy($content_id)
	{
		$result = $this->option_group->delete($content_id);

		echo json_encode($result);
	}

	public function groups()
	{
		$data['option_groups'] = $this->option_group->getAll();

		echo json_encode($data);
	}
}

?>