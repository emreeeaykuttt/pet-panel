<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coupon extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata('AdminToken')) 
		{
			$this->session->set_userdata('which_page', (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			redirect(base_url() . 'auth/login');
			exit();
		}
		
		$this->autorun();
		$this->load->model('product/coupon_model', 'coupon');
		$this->load->model('language/language_model', 'language');
	}

	public function index()
	{
		$this->load->view('frontend/product/coupon_view');
	}

	public function list_all($lang_id = '')
	{
		if ($lang_id == '') 
		{
			$lang = $this->language->getByDefault();
			$lang_id = $lang['LanguageID'];
		}

		$post = $this->input->post(null, true);
		$post['search']['value'] = str_replace(' ', '%20', $post['search']['value']);
		$this->coupon->post = json_encode($post);

		$list = $this->coupon->getDatatables($lang_id);
		$data = array();
		
		if (!empty($list))
		{
			foreach ($list as $coupon) 
			{
				$row = array();
				$row['CouponName'][] = $coupon['CouponName'];
				$row['Transactions'][] = '
					<a class="btn-green" href="javascript:void(0)" onclick="view('.$coupon['CouponContentID'].')">
						<i class="la la-eye"></i> Görüntüle
					</a>
					<a class="btn-yellow" href="javascript:void(0)" onclick="edit('.$coupon['CouponContentID'].')">
						<i class="la la-pen-nib"></i> Düzenle
					</a>
					<a class="btn-red" href="javascript:void(0)" onclick="destroy('.$coupon['CouponContentID'].')">
						<i class="la la-trash-alt"></i> Sil
					</a>
					<a class="btn-blue" href="javascript:void(0)" onclick="user_add('. $coupon['CouponContentID'] .', ' . $coupon['CouponContentID'] .', \''. $coupon['CouponName'] .'\''.')">
						<i class="la la-code-branch"></i> Kullanıcılar <span>('. $coupon['UserCouponCount'] .')</span>
					</a>
				';
				
				$data[] = $row;
			}
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->coupon->getCountAll($lang_id),
			"recordsFiltered" => $this->coupon->getCountFiltered($lang_id),
			"data" => $data,
		);
		
		echo json_encode($output);
	}

	public function view($content_id)
	{	
		$results = $this->coupon->getByContentID($content_id);
		$i = 0;

		foreach ($results as $key => $value) 
		{
			$i++;
			$data[$i] = $value;
		}

		$data['total_record'] = $i;
		
		echo json_encode($data);
	}

	public function add()
	{
		$post = $this->input->post(null,true);

		$encode_data = array(
			'data' => json_encode($post),
		);

		$result = $this->coupon->save($encode_data);
		
		echo json_encode($result);
	}

	public function update()
	{
		$post = $this->input->post(null,true);
		
		$encode_data = array(
			'data' => json_encode($post),
		);
		
		$result = $this->coupon->update($encode_data);
		
		echo json_encode($result);
	}

	public function destroy($content_id)
	{
		$result = $this->coupon->delete($content_id);

		echo json_encode($result);
	}
}

?>