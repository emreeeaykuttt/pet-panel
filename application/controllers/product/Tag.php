<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tag extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata('AdminToken')) 
		{
			$this->session->set_userdata('which_page', (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			redirect(base_url() . 'auth/login');
			exit();
		}
		
		$this->autorun();
		$this->load->model('product/tag_model', 'tag');
		$this->load->model('language/language_model', 'language');
	}

	public function index()
	{
		$this->load->view('frontend/product/tag_view');
	}

	public function list_all($lang_id = '')
	{
		if ($lang_id == '') 
		{
			$lang = $this->language->getByDefault();
			$lang_id = $lang['LanguageID'];
		}

		$post = $this->input->post(null, true);
		$post['search']['value'] = str_replace(' ', '%20', $post['search']['value']);
		$this->tag->post = json_encode($post);

		$list = $this->tag->getDatatables($lang_id);
		$data = array();
		
		if (!empty($list))
		{
			foreach ($list as $tag) 
			{
				$row = array();
				$row['TagTitle'][] = $tag['TagTitle'];
				$row['Transactions'][] = '
					<a class="btn-green" href="javascript:void(0)" onclick="view('.$tag['TagContentID'].')">
						<i class="la la-eye"></i> Görüntüle
					</a>
					<a class="btn-yellow" href="javascript:void(0)" onclick="edit('.$tag['TagContentID'].')">
						<i class="la la-pen-nib"></i> Düzenle
					</a>
					<a class="btn-red" href="javascript:void(0)" onclick="destroy('.$tag['TagContentID'].')">
						<i class="la la-trash-alt"></i> Sil
					</a>
				';
				
				$data[] = $row;
			}
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->tag->getCountAll($lang_id),
			"recordsFiltered" => $this->tag->getCountFiltered($lang_id),
			"data" => $data,
		);
		
		echo json_encode($output);
	}

	public function view($content_id)
	{	
		$results = $this->tag->getByContentID($content_id);
		$i = 0;

		foreach ($results as $key => $value) 
		{
			$i++;
			$data[$i] = $value;
		}

		$data['total_record'] = $i;
		
		echo json_encode($data);
	}

	public function add()
	{
		$post = $this->input->post(null,true);

		$encode_data = array(
			'data' => json_encode($post),
		);

		$result = $this->tag->save($encode_data);
		
		echo json_encode($result);
	}

	public function update()
	{
		$post = $this->input->post(null,true);

		$encode_data = array(
			'data' => json_encode($post),
		);
		
		$result = $this->tag->update($encode_data);
		
		echo json_encode($result);
	}

	public function destroy($content_id)
	{
		$result = $this->tag->delete($content_id);

		echo json_encode($result);
	}

	public function search()
	{	
		if (!empty($this->input->get('title'))) 
		{
			$data = $this->tag->getAllByTitle($this->input->get('title'));
		}
		elseif (!empty($this->input->get('content_ids'))) 
		{
			$data = $this->tag->getAllByContentIDs($this->input->get('content_ids'));
		}
		else
		{
			$data = null;
		}
		
		echo json_encode($data);
	}
}

?>