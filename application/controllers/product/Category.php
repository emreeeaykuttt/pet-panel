<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata('AdminToken')) 
		{
			$this->session->set_userdata('which_page', (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			redirect(base_url() . 'auth/login');
			exit();
		}
		
		$this->autorun();
		$this->load->model('product/category_model', 'category');
		$this->load->model('language/language_model', 'language');
	}

	public function index()
	{
		$this->load->view('frontend/product/category_view');
	}

	public function list_all($lang_id = '')
	{
		if ($lang_id == '') 
		{
			$lang = $this->language->getByDefault();
			$lang_id = $lang['LanguageID'];
		}

		$post = $this->input->post(null, true);
		$post['search']['value'] = str_replace(' ', '%20', $post['search']['value']);
		$this->category->post = json_encode($post);

		$list = $this->category->getDatatables($lang_id);
		$data = array();
		
		if (!empty($list))
		{
			foreach ($list as $category) 
			{
				$row = array();
				$row['CategoryName'][] = $category['CategoryName'];
				$row['CategoryHeadName'][] = $category['CategoryHeadName'];
				$row['Transactions'][] = '
					<a class="btn-green" href="javascript:void(0)" onclick="view('.$category['CategoryContentID'].')">
						<i class="la la-eye"></i> Görüntüle
					</a>
					<a class="btn-yellow" href="javascript:void(0)" onclick="edit('.$category['CategoryContentID'].')">
						<i class="la la-pen-nib"></i> Düzenle
					</a>
					<a class="btn-red" href="javascript:void(0)" onclick="destroy('.$category['CategoryContentID'].')">
						<i class="la la-trash-alt"></i> Sil
					</a>
				';
				
				$data[] = $row;
			}
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->category->getCountAll($lang_id),
			"recordsFiltered" => $this->category->getCountFiltered($lang_id),
			"data" => $data,
		);
		
		echo json_encode($output);
	}

	public function view($content_id)
	{	
		$results = $this->category->getByContentID($content_id);
		$i = 0;

		foreach ($results as $key => $value) 
		{
			$i++;
			$data[$i] = $value;

			$this->category->lang_id = $value['CategoryLangID'];
			$category = $this->category->getByContentID($value['CategoryParentID']);
			$data[$i]['CategoryHeadContentID'] = $category['CategoryContentID'];
			$data[$i]['CategoryHeadName'] = $category['CategoryName'];
		}

		$data['total_record'] = $i;
		
		echo json_encode($data);
	}

	public function add()
	{
		$post = $this->input->post(null,true);


		$encode_data = array(
			'data' => json_encode($post),
		);

		$result = $this->category->save($encode_data);
		
		echo json_encode($result);
	}

	public function update()
	{
		$post = $this->input->post(null,true);

		$encode_data = array(
			'data' => json_encode($post),
		);
		
		$result = $this->category->update($encode_data);
		
		echo json_encode($result);
	}

	public function destroy($content_id)
	{
		$result = $this->category->delete($content_id);

		echo json_encode($result);
	}

	public function list_by_parent_id($parent_id)
	{
		$data['categories'] = $this->category->getAllByParentID($parent_id);

		echo json_encode($data);
	}

	public function tree_list($lang_id)
	{
		$this->category->lang_id = $lang_id;
		$categories = $this->category->getAll();
		$data = isset($categories['status']) ? NULL : $categories;

		echo json_encode($data);
	}

}

?>