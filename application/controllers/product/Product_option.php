<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_option extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata('AdminToken')) 
		{
			$this->session->set_userdata('which_page', (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			redirect(base_url() . 'auth/login');
			exit();
		}
		
		$this->autorun();
		$this->load->model('product/product_option_model', 'product_option');
	}

	public function view($content_id)
	{	
		$results = $this->product_option->getByContentID($content_id);

		$i = 0;
		foreach ($results as $key => $value) 
		{
			$i++;
			$data[$i] = $value;
		}

		$data['total_record'] = $i;
		
		echo json_encode($data);
	}

	public function add()
	{
		$post = $this->input->post(null,true);

		$encode_data = array(
			'data' => json_encode($post),
		);

		$result = $this->product_option->save($encode_data);
		
		echo json_encode($result);
	}

	public function update()
	{
		$post = $this->input->post(null,true);

		$encode_data = array(
			'data' => json_encode($post),
		);
		
		$result = $this->product_option->update($encode_data);
		
		echo json_encode($result);
	}

	public function destroy($content_id)
	{
		$result = $this->product_option->delete($content_id);

		echo json_encode($result);
	}

	public function list_by_product_content_id($product_content_id)
	{	
		$this->load->model('product/option_group_model', 'option_group');
		$this->load->model('product/option_model', 'option');

		$result = $this->product_option->getAllByProductContentID($product_content_id);
		$product_option_arr = array();

		foreach ($result as $key => $value) 
		{
			$product_option_arr[$key] = $value;

			$this->option->lang_id = $value['ProductOptionLangID'];
			$option = '';
			foreach (json_decode($value['ProductOptionOptionContentIDs'], true) as $option_val) 
			{
				$val = $this->option->getByContentID($option_val);
				$option .= $val['OptionName'] . ' - ';
			}

			$product_option_arr[$key]['ProductOptionOptionNames'] = rtrim($option, ' - ');
		}

		$data['options'] = $product_option_arr;

		echo json_encode($data);
	}

}

?>