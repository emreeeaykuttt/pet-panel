<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_option_photo extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata('AdminToken')) 
		{
			$this->session->set_userdata('which_page', (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			redirect(base_url() . 'auth/login');
			exit();
		}

		$this->autorun();
		$this->load->model('product/product_option_photo_model','product_option_photo');
	}

	public function list_by_product_option_id($product_option_id)
	{
		$data = $this->product_option_photo->getAllByProductOptionID($product_option_id);
		
		echo json_encode($data);
	}

	public function gallery_upload($gallery_id)
	{
		$files = array();

		if (!empty($_FILES['ProductOptionGallery']['tmp_name'][$gallery_id])) 
		{
			$tmp_name = $_FILES['ProductOptionGallery']['tmp_name'][$gallery_id];
			$image_name = $_FILES['ProductOptionGallery']['name'][$gallery_id];
			$file_total = count($tmp_name);
			for ($i = 0; $i < $file_total; $i++)
			{ 
				$check = getimagesize($tmp_name[$i]);
				if($check !== false) 
				{
					$data = base64_encode(file_get_contents($tmp_name[$i]));
					$files[$i]['image'] = 'data:' . $check['mime'] . ';base64,' . $data;
					$files[$i]['name'] = $image_name[$i];
				}
			}
		}

		$encode_data = array(
			'files' => json_encode($files),
			'total' => $file_total,
		);

		$result = $this->product_option_photo->upload($encode_data);
		
		echo json_encode($result);
	}

}

?>