<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata('AdminToken')) 
		{
			$this->session->set_userdata('which_page', (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			redirect(base_url() . 'auth/login');
			exit();
		}
		
		$this->autorun();
		$this->load->model('language/Language_model', 'language');
	}

	public function languages_and_default_language()
	{
		$data['languages'] = $this->language->getAllByActive();
		$data['default_language'] = $this->language->getByDefault();

		echo json_encode($data);
	}

}


?>