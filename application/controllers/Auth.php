<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->autorun();
		$this->load->model('user/auth_model', 'auth');
	}

	public function index()
	{
		redirect(base_url());
	}

	public function login()
	{
		$post = $this->input->post(null, true);
		$data = '';

		if ($post) 
		{
			$result = $this->auth->validate($post);

			if ($result['status'] == TRUE) 
			{
				$user = $result['user'];

				$this->session->unset_userdata(array('AdminUserID', 'AdminUserFirstName', 'AdminToken'));
				
				$this->session->set_userdata('AdminUserID', $user['UserID']);
				$this->session->set_userdata('AdminUserFirstName', $user['UserFirstName']);
				$this->session->set_userdata('AdminToken', $result['token']);

				$remember = empty($post['UserRemember']) ? 0 : 1;
				if($remember == 1)
				{
					$this->session->set_tempdata('RememberAdminUserEmail', $post['UserEmail'], 604800); // 1 week
					$this->session->set_tempdata('RememberAdminUserPassword', $post['UserPassword'], 604800);
				}
				else
				{
					$this->session->unset_tempdata(array('RememberAdminUserEmail', 'RememberAdminUserPassword'));
				}

				if ($this->session->userdata('which_page'))
				{
					$which_page = $this->session->userdata('which_page');
					$this->session->unset_userdata(array('which_page'));
					
					redirect($which_page);
				}
				else
				{
					redirect(base_url());
				}
			}
			else
			{
				$this->session->set_flashdata('login_status', $result);

				$this->load->view('frontend/auth/login_view', $result);
			}
		}
		else
		{
			if ($this->session->userdata('AdminToken'))
			{
				redirect(base_url());
			}
			else
			{
				$this->load->view('frontend/auth/login_view', $data);
			}
		}
	}

	public function logout()
	{
		$this->session->unset_userdata(array('AdminUserID', 'AdminUserFirstName', 'AdminToken'));

		redirect(base_url().'auth/login');
	}
}

?>