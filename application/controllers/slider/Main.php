<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata('AdminToken')) 
		{
			$this->session->set_userdata('which_page', (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			redirect(base_url() . 'auth/login');
			exit();
		}

		$this->autorun();
		$this->load->model('slider/slider_model', 'slider');
		$this->load->model('language/language_model', 'language');
	}

	public function index()
	{
		$this->load->view('frontend/slider/home_view');
	}
	
	public function list_all($lang_id = '')
	{
		if ($lang_id == '') 
		{
			$lang = $this->language->getByDefault();
			$lang_id = $lang['LanguageID'];
		}

		$post = $this->input->post(null, true);
		$post['search']['value'] = str_replace(' ', '%20', $post['search']['value']);
		$this->slider->post = json_encode($post);

		$list = $this->slider->getDatatables($lang_id);
		$data = array();
		
		if (!empty($list))
		{
			foreach ($list as $slider) 
			{
				$row = array();
				$row['SliderTitle'][] = $slider['SliderTitle'];
				$row['SliderSort'][] = $slider['SliderSort'];
				if($slider['SliderPhoto']){
					$row['SliderPhoto'][] = '<img src="'. SERVER_URL . $slider['SliderPhoto'] .'" class="img-fluid" style="height:100px" />';
				} else{
					$row['SliderPhoto'][] = '(Fotoğraf Yok)';
				}
				$row['Transactions'][] = '
					<a class="btn-green" href="javascript:void(0)" onclick="view('. $slider['SliderContentID'] .')">
						<i class="la la-eye"></i> Görüntüle
					</a>
					<a class="btn-yellow" href="javascript:void(0)" onclick="edit('. $slider['SliderContentID'] .')">
						<i class="la la-pen-nib"></i> Düzenle
					</a>
					<a class="btn-red" href="javascript:void(0)" onclick="destroy('. $slider['SliderContentID'] .')">
						<i class="la la-trash-alt"></i> Sil
					</a>
				';
				
				$data[] = $row;
			}
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->slider->getCountAll($lang_id),
			"recordsFiltered" => $this->slider->getCountFiltered($lang_id),
			"data" => $data,
		);
		
		echo json_encode($output);
	}

	public function view($content_id)
	{	
		$this->load->model('slider/slider_model', 'slider');

		$results = $this->slider->getByContentID($content_id);
		$i = 0;

		foreach ($results as $key => $value) 
		{
			$i++;
			$data[$i] = $value;
		}

		$data['total_record'] = $i;
		
		echo json_encode($data);
	}

	public function view_all($content_id)
	{	
		$results = $this->slider->getByContentID($content_id);
		$i = 0;

		foreach ($results as $key => $value) 
		{
			$i++;
			$data[$i] = $value;
		}

		$data['total_record'] = $i;
		
		echo json_encode($data);
	}

	public function add()
	{
		$this->load->model('language/Language_model', 'language');

		$post = $this->input->post();
		$languages = $this->language->getCount();
		$files = array();

		for ($i=1; $i <= $languages; $i++)
		{ 
			$files[$i]['image'] = '';
			$files[$i]['name'] = '';

			if (!empty($_FILES['SliderPhoto']['tmp_name'][$i])) 
			{
				$check = getimagesize($_FILES['SliderPhoto']['tmp_name'][$i]);
				if($check !== false) 
				{
					$data = base64_encode(file_get_contents($_FILES['SliderPhoto']['tmp_name'][$i]));
					$files[$i]['image'] = 'data:' . $check['mime'] . ';base64,' . $data;
					$files[$i]['name'] = $_FILES['SliderPhoto']['name'][$i];
				}
			}
		}

		$encode_data = array(
			'data' => json_encode($post),
			'files' => json_encode($files),
		);

		$result = $this->slider->save($encode_data);
		
		echo json_encode($result);
	}

	public function update()
	{
		$this->load->model('language/Language_model', 'language');
		
		$post = $this->input->post();
		$languages = $this->language->getCount();
		$files = array();

		for ($i=1; $i <= $languages; $i++)
		{ 
			$files[$i]['image'] = '';
			$files[$i]['name'] = '';

			if (!empty($_FILES['SliderPhoto']['tmp_name'][$i])) 
			{
				$check = getimagesize($_FILES['SliderPhoto']['tmp_name'][$i]);
				if($check !== false) 
				{
					$data = base64_encode(file_get_contents($_FILES['SliderPhoto']['tmp_name'][$i]));
					$files[$i]['image'] = 'data:' . $check['mime'] . ';base64,' . $data;
					$files[$i]['name'] = $_FILES['SliderPhoto']['name'][$i];
				}
			}
		}

		$encode_data = array(
			'data' => json_encode($post),
			'files' => json_encode($files),
		);

		$result = $this->slider->update($encode_data);
		
		echo json_encode($result);
	}

	public function destroy($content_id)
	{
		$result = $this->slider->delete($content_id);

		echo json_encode($result);
	}

}


?>