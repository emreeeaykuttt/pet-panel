<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata('AdminToken')) 
		{
			$this->session->set_userdata('which_page', (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			redirect(base_url() . 'auth/login');
			exit();
		}
		
		$this->autorun();
		$this->load->model('order/order_model', 'order');
	}

	public function index()
	{
		$this->load->view('frontend/order/home_view');
	}
	
	public function list_all()
	{
		$post = $this->input->post(null, true);
		$post['search']['value'] = str_replace(' ', '%20', $post['search']['value']);
		$this->order->post = json_encode($post);

		$list = $this->order->getDatatables();

		$data = array();
		
		if (!empty($list))
		{
			foreach ($list as $order) 
			{
				$row = array();
				$row['OrderTrackingNumber'][] = $order['OrderTrackingNumber'];
				$row['OrderDeliveryFirstName'][] = $order['OrderDeliveryFirstName'] . ' ' . $order['OrderDeliveryLastName'];
				$row['OrderLatestAmount'][] = $order['OrderLatestAmount'] . $order['OrderCurrencyCode'];
				$row['OrderPaymentType'][] = $order['OrderPaymentType'] == 'card' ? 'Kredi Kartı / Banka Kartı' : 'EFT/havale';
				$row['OrderStatusText'][] = $order['OrderStatusText'];
				$row['OrderShippingStatusText'][] = $order['OrderShippingStatusText'];
				$row['Transactions'][] = '
					<a class="btn-green" href="javascript:void(0)" onclick="view('.$order['OrderID'].')">
						<i class="la la-eye"></i> Görüntüle
					</a>
					<a class="btn-yellow" href="javascript:void(0)" onclick="contract_view('.$order['OrderID'].')">
						<i class="la la-eye"></i> Sözleşme
					</a>
				';
				
				$data[] = $row;
			}
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->order->getCountAll(),
			"recordsFiltered" => $this->order->getCountFiltered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}

	public function view($id)
	{	
		$this->load->model('order/detail_model', 'detail');

		$data = $this->order->getByID($id);
		$data['OrderDetails'] = $this->detail->getAllByOrderID($id);
		
		echo json_encode($data);
	}

	public function status_update($id)
	{
		$post = $this->input->post(null, true);

		$data = array(
			'update_type' => 'status',
			'OrderStatus' => $post['OrderStatus'],
		);

		$result = $this->order->update($data, $id);

		echo json_encode($result);
	}
	
	public function shipping_status_update($id)
	{
		$post = $this->input->post(null, true);

		$data = array(
			'update_type' => 'shipping_status',
			'OrderShippingStatus' => $post['OrderShippingStatus'],
			'OrderCargoCompany' => $post['OrderCargoCompany'],
			'OrderCargoNumber' => $post['OrderCargoNumber'],
		);

		$result = $this->order->update($data, $id);

		echo json_encode($result);
	}

}


?>