$(document).ready(function(){
    var pageActiveUrl = window.location.pathname;
    pageActiveUrl = pageActiveUrl.replace(/\//g,'-');

    $('#menu' + pageActiveUrl).addClass('active');
});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

function module_export_name(text)
{
    let new_date = new Date();
    let month = new_date.getMonth()+1;
    let day = new_date.getDate();
    let full_date = new_date.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
    let module_export_name = text + '_' + full_date;

    return module_export_name;
}

function replace_special_chars(str) 
{
    var specialChars = [['ş', 's'], ['ğ', 'g'], ['ü', 'u'], ['ı', 'i'],['_', '-'],
        ['ö', 'o'], ['Ş', 'S'], ['Ğ', 'G'], ['Ç', 'C'], ['ç', 'c'],
        ['Ü', 'U'], ['İ', 'I'], ['Ö', 'O'], ['ş', 's']];

    for (var i = 0; i < specialChars.length; i++) {
        str = str.replace(eval('/' + specialChars[i][0] + '/ig'), specialChars[i][1]);
    }
    return str;
}

function ajax_languages()
{
    $.ajax({
        url : base_url + 'setting/language/main/languages_and_default_language',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            languages = data.languages;
            default_language = data.default_language.LanguageID;
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        },
        async: false,
    });
}

function round_number(val, digit = 2)
{
    return Number(Math.round(val + 'e' + digit) + 'e-' + digit);
}